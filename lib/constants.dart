import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF4DA794);
const kPrimaryLightColor = Color(0xFF6FEDD1);
const kSecondaryColor = Color(0xFF999999);
const kTextAccentColor = Color(0xFF093C5B);
const kSystemColor = Color(0xFFEEEEEE);
const kRowColor = Color(0xFF65CCB7);

final themeColor = Color(0xFF4DA794);
final primaryColor = Color(0xff203152);
final greyColor = Color(0xffaeaeae);
final greyColor2 = Color(0xffE8E8E8);
final greyColor3 = Color(0xffeaeaea);
final blueColor = Color(0xff0080ff);

const kCCubeAppId = "5402";
const kCCUbeAuthKey =  "6ew38YSvUEXu-5N";
const kCCUbeSecret = "4-pjBrX6b5vpDOM";

final String SORT_ASC = "asc";
final String SORT_DESC = "desc";

final String USER_ARG_NAME = "user";
final String DIALOG_ARG_NAME = "dialog";