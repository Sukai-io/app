class NFTItem {
  final String id;
  final String creator;
  final String title;
  final String image;
  final String description;
  final double price;
  final String currency;
  final String network;

  NFTItem(this.id, this.creator, this.title, this.image, this.description, this.price, this.currency, this.network);

  factory NFTItem.fromJson(Map<String, dynamic> json) {
    return NFTItem(
      json['id'] ?? '',
      json['owner_name'] ?? '',
      json['title'] ?? '',
      json['thumbnail'] ?? '',
      json['description'] ?? '',
      double.tryParse(json['price']) ?? 0.0,
      json['currency'] ?? '',
      json['network'] ?? '',
    );
  }
}

class NFTRecord {
  final String title;
  final String description;
  final String image;

  NFTRecord(this.title, this.description, this.image);
}