enum AdsStatus { active, finished, draft }

class SmartAds {
  final String title;
  final AdsStatus status;
  final String thumbnail;

  SmartAds(this.title, this.status, this.thumbnail);
}

class AdsStat {
  final double females;
  final double males;
  final double clicks;
  final double likes;
  final double comments;
  final double blocked;

  AdsStat(this.females, this.males, this.clicks, this.likes, this.comments, this.blocked);
}