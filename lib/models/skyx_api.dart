import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:decimal/decimal.dart';
import 'package:http/http.dart' as http;
import 'package:http/io_client.dart';

class TrustAllCertificates {
  static http.Client sslClient() {
    var ioClient = new HttpClient()
      ..badCertificateCallback = (X509Certificate cert, String host, int port) {
        return (host.compareTo("staging.skyxtoken.io") == 0);
      };
    http.Client _client = IOClient(ioClient); return _client;
  }
}

class SkyXApi {
  static final SkyXApi _skyXApi = SkyXApi._internal();
  static const String BASE_URL = 'https://staging.skyxtoken.io/api';

  factory SkyXApi() {
    return _skyXApi;
  }

  SkyXApi._internal();

  final Map<String, String> headers = {
    'Cache-Control': 'no-cache',
    'Content-Type': 'application/json; charset=UTF-8',
  };

  final Map<String, String> formHeaders = {
    'Cache-Control': 'no-cache',
    'Content-Type': 'multipart/form-data; charset=UTF-8',
  };

  Uri _requestUri(String endpoint) {
    return Uri.parse('$BASE_URL/$endpoint');
  }

  Future<dynamic> _get(String endpoint) async {
    final response = await TrustAllCertificates.sslClient().get(
      _requestUri(endpoint),
      headers: headers,
    );
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    }
    throw Exception('Request failed');
  }

  Future<dynamic> _post(String endpoint, Map<String, String> fields, {List<http.MultipartFile>? multipartFiles}) async {
    final request = http.MultipartRequest('post', _requestUri(endpoint));
    if (multipartFiles != null) {
      request.files.addAll(multipartFiles);
    }
    request.fields.addAll(fields);
    final response = await request.send();

    if (response.statusCode == 200) {
      final responseStr = await response.stream.bytesToString();
      return jsonDecode(responseStr);
    }
    throw Exception('Request failed');
  }

  Future<List<SkyXItem>> getCollections(String address) async {
    final responseJson = await _get(
      "token/collection/$address",
    );
    if (responseJson['docs'] != null) {
      List<dynamic> items = responseJson['docs'];
      final list = items.map((json) => SkyXItem.fromJson(json)).toList();
      return list;
    }
    return List.empty();
  }

  Future<List<SkyXItem>> getTokenSale() async {
    final responseJson = await _get(
      "token/sale",
    );
    if (responseJson['docs'] != null) {
      List<dynamic> items = responseJson['docs'];
      final list = items.map((json) => SkyXItem.fromJson(json)).toList();
      return list;
    }
    return List.empty();
  }

  Future<Map<String, dynamic>> getTxHistory(String address) async {
    final responseJson = await _get(
      "tx/address/$address",
    );
    Map<String, dynamic> dataMap = responseJson;
    return dataMap;
  }

  Future<Map<String, dynamic>> getTokenHistory(int tokenId) async {
    final responseJson = await _get(
      "tx/token/$tokenId",
    );
    Map<String, dynamic> dataMap = responseJson;
    return dataMap;
  }

  Future<Map<String, dynamic>> getItemsList() async {
    final responseJson = await _get(
      "items",
    );
    Map<String, dynamic> dataMap = responseJson;
    return dataMap;
  }

  Future<Map<String, dynamic>> mintToken() async {
    final responseJson = await _post(
      "new-item",
        <String, String>{}
    );
    Map<String, dynamic> dataMap = responseJson;
    return dataMap;
  }

}

class SkyXItem {
  final String id;
  final String address;
  final String tokenID;
  final int amount;
  final bool isForSale;
  final String priceID;
  final Decimal tokenPriceEther;
  final String tokenPriceWei;
  final SkyXMetaData metadata;

  SkyXItem(this.id, this.address, this.tokenID, this.amount, this.isForSale, this.priceID, this.tokenPriceEther, this.tokenPriceWei, this.metadata);

  factory SkyXItem.fromJson(Map<String, dynamic> json) {
    final double price = json['tokenPriceEther'] ?? 0.0;
    return SkyXItem(
      json['_id'] ?? '',
      json['address'] ?? '',
      json['tokenID'] ?? '',
      json['amount'] ?? 0,
      json['isForSale'] ?? false,
      json['priceID'] ?? '',
      Decimal.parse(price.toString()),
      json['tokenPriceWei'] ?? '',
      SkyXMetaData.fromJson(json['metadata'])
    );
  }
}

class SkyXMetaData {
  final String name;
  final String description;
  final String network;
  final String txHash;
  final String tokenIdHex;
  final String file;
  final bool status;

  SkyXMetaData(this.name, this.description, this.network, this.txHash, this.tokenIdHex, this.file, this.status);

  factory SkyXMetaData.fromJson(Map<String, dynamic> json) {
    return SkyXMetaData(
      json['name'] ?? '',
      json['description'] ?? '',
      json['network'] ?? '',
      json['txHash'] ?? '',
      json['tokenIdHex'] ?? '',
      json['file'] ?? '',
      json['status'] ?? false,
    );
  }
}