import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sukai/models/user.dart';

import 'giveaway.dart';
import 'nft.dart';

class SukaiApi {
  static final SukaiApi _sukaiApi = SukaiApi._internal();
  static const String PUB_ASSET = 'https://sukai.io';

  factory SukaiApi() {
    return _sukaiApi;
  }

  SukaiApi._internal();

  Future<bool> isLogin() async {
    if (_loginUser == null) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      final userLoginJson = prefs.getString('user_login') ?? null;
      if (userLoginJson != null) {
        final userLoginMap = jsonDecode(userLoginJson);
        _loginUser = LoginUser.fromJson(userLoginMap);
        _member = await getMember();
      }
    }
    return _loginUser != null;
  }

  LoginUser? _loginUser;
  Member? _member;

  Member? get currentUser {
    return _member;
  }

  String getToken(){
    if (_loginUser == null) return "";
    return _loginUser!.token;
  }

  final String baseUrl = 'https://sukai.io/api';

  final Map<String, String> headers = {
    'Cache-Control': 'no-cache',
    'Content-Type': 'application/json; charset=UTF-8',
  };

  final Map<String, String> formHeaders = {
    'Cache-Control': 'no-cache',
    'Content-Type': 'multipart/form-data; charset=UTF-8',
  };

  Uri _requestUri(String endpoint) {
    return Uri.parse('$baseUrl/$endpoint');
  }

  Future<dynamic> _get(String endpoint, {bool authenticated = false}) async {
    if (authenticated && _loginUser != null) {
      headers.addAll({
        'X-Auth-Token' : _loginUser!.token
      });
    }
    final response = await http.get(
        _requestUri(endpoint),
        headers: headers,
    );
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    }
    throw Exception('Request failed');
  }

  Future<dynamic> _post(String endpoint, Map<String, String> fields, {bool authenticated = false, List<http.MultipartFile>? multipartFiles}) async {
    final request = http.MultipartRequest('post', _requestUri(endpoint));
    if (multipartFiles != null) {
      request.files.addAll(multipartFiles);
    }
    request.fields.addAll(fields);
    if (authenticated && _loginUser != null) {
      request.headers.addAll({
        'X-Auth-Token' : _loginUser!.token
      });
    }
    final response = await request.send();

    if (response.statusCode == 200) {
      final responseStr = await response.stream.bytesToString();
      return jsonDecode(responseStr);
    }
    throw Exception('Request failed');
  }

  Future<LoginUser?> authenticate(String username, String password, bool rememberPassword) async {
    final responseJson = await _post(
        'authenticate',
        <String, String> {
            'username' : username,
            'password' : password
        }
    );
    if (responseJson['success'] == true) {
      final token = responseJson['data'];
      _loginUser = LoginUser(username, token);
      _member = await getMember();
      if (rememberPassword) {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('user_login', jsonEncode(_loginUser));
      }
      return _loginUser;
    }
    return null;
  }

  void logout() async {
    _loginUser = null;
    _member = null;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('user_login');
  }

  Future<Member?> getMember() async {
    final responseJson = await _get(
      'member/get',
      authenticated: true
    );
    if (responseJson['success'] == true) {
      return Member.fromJson(responseJson['data']);
    }
    return null;
  }

  Future<bool> buyTicket(String quantity, String screenshot) async {
    final responseJson = await _post(
        'buyticket',
        <String, String> {
          'quantity' : quantity
        },
        multipartFiles: [
          await http.MultipartFile.fromPath(
              'payment',
              screenshot)
        ],
        authenticated: true
    );
    if (responseJson['success'] == true) {
      return true;
    }
    return false;
  }

  Future<List<Participant>> giveawayParticipants() async {
    final responseJson = await _get(
      'giveparticipants',
      authenticated: true
    );
    if (responseJson['success'] == true && responseJson['data'] != null) {
      List<dynamic> participantsList = responseJson['data'];
      final list = participantsList.map((json) => Participant.fromJson(json)).toList();
      return list;
    }
    return List.empty();
  }

  Future<APIResult<Sponsor>> searchsponsordata(String username) async {
    final responseJson = await _post(
      'searchsponsordata',
      <String, String>{
        'sponsor_username' : username
      }
    );
    Sponsor? sponsor;
    if (responseJson['success']) {
      final sponsorJson = responseJson['data']['data']['data'];
      sponsor = Sponsor(sponsorJson['sponsor_id'], sponsorJson['sponsor_name']);
    }
    return APIResult(responseJson['success'], message: responseJson['data']['error'], data: sponsor);
  }

  Future<APIResult> checkusername(String username) async {
    final responseJson = await _post(
      'checkusername',
      <String, String>{
        'username' : username
      }
    );
    return APIResult(responseJson['success'], message: responseJson['data']['error']);
  }

  Future<bool> _checkExist(String field, String value) async {
    final responseJson = await _post(
      'checkexist',
      <String, String>{
        'field' : field,
        'value' : value
      }
    );
    return responseJson['success'] == true;
  }

  Future<bool> checkEmailExist(String email) async {
    return await _checkExist('email', email);
  }

  Future<bool> checkIdNumberExist(String idcard) async {
    return await _checkExist('idcard', idcard);
  }

  Future<bool> checkPhoneExist(String phone) async {
    return await _checkExist('phone', phone);
  }

  Future<APIResult> registerMember(RegisterMember form) async {
    final sponsor = await searchsponsordata(form.sponsor);
    final responseJson = await _post(
      'register',
      <String, String>{
        'sponsored' : 'other_sponsor',
        'reg_member_sponsor_id' : sponsor.data?.id??'',
        'reg_member_sponsor' : form.sponsor,
        'reg_member_username' : form.username,
        'reg_member_password' : form.password,
        'reg_member_name' : form.name,
        'reg_member_email' : form.email,
        'reg_member_address' : form.address,
        'reg_member_city_string' : form.city,
        'reg_member_gender' : form.gender,
        'reg_member_phone' : form.phone,
        'reg_member_idcard' : form.idnumber,
        'reg_member_country_code' : form.country,
        'reg_member_smoker' : form.smoker,
        'reg_member_married' : form.married,
        'reg_member_hobby' : form.hobby,
        'reg_member_profession' : form.profession,
        'reg_member_birthday' : form.dateofbirth
      },
      multipartFiles: [
        await http.MultipartFile.fromPath(
            'reg_member_idcardfile',
            form.idfilepath)
      ],
    );
    return APIResult(responseJson['success'], message: responseJson['data']['error']);
  }

  Future<APIResult> simpleRegister(RegisterMember form) async {
    final sponsor = await searchsponsordata(form.sponsor);
    final responseJson = await _post(
      'simpleregister',
      <String, String>{
        'sponsored' : 'other_sponsor',
        'reg_member_sponsor_id' : sponsor.data?.id??'',
        'reg_member_sponsor' : form.sponsor,
        'reg_member_username' : form.username,
        'reg_member_password' : form.password,
        'reg_member_name' : form.name,
        'reg_member_email' : form.email,
      },
    );
    final isSuccess = responseJson['success'];
    return APIResult(isSuccess, data:isSuccess?Member.fromJson(responseJson['data']):null , message: isSuccess?null:responseJson['data']['error']);
  }

  Future<APIResult> registerclient(RegisterClient form) async {
    final sponsor = await searchsponsordata(form.sponsor);
    final responseJson = await _post(
        'registerclient',
        {
          'sponsored' : 'other_sponsor',
          'reg_member_sponsor_id' : sponsor.data?.id??'',
          'reg_member_sponsor' : form.sponsor,
          'reg_member_username' : form.username,
          'reg_member_password' : form.password,
          'reg_member_name' : form.name,
          'reg_member_email' : form.email,
          'reg_member_city_string' : form.city,
          'reg_member_gender' : form.gender,
          'reg_member_phone' : form.phone,
          'reg_member_idcard' : form.idnumber,
          'reg_member_country_code' : form.country,
          'reg_member_company_name' : form.companyName,
          'reg_member_company_address' : form.companyAddress,
          'reg_member_brand_name' : form.brandName,
          'reg_member_product_type' : form.productType
        },
        multipartFiles: [
          await http.MultipartFile.fromPath(
              'reg_member_idcardfile',
              form.idfilepath)
        ],
    );
    return APIResult(responseJson['success'], message: responseJson['data']['error']);
  }

  Future<Dashboard> dashboard() async {
    final responseJson = await _get(
        'dashboard',
        authenticated: true
    );
    if (responseJson['success'] == true && responseJson['data'] != null) {
      final dataJson = responseJson['data'];
      return Dashboard(dataJson['total_poin'], dataJson['total_usdt'], dataJson['total_token'], dataJson['total_sponsored']);
    }else{
      return Dashboard("0", "0", "0", "0");
    }
  }

  Future<Map<String, dynamic>> getOptions() async {
    final responseJson = await _get(
      'getoptions',
    );
    if (responseJson['success'] == true && responseJson['data'] != null) {
      Map<String, dynamic> dataMap = responseJson['data'];
      return dataMap;
    }
    return Map<String, dynamic>();
  }

  Future<APIResult> forgotpassword(String email) async {
    final responseJson = await _post(
        'forgotpassword',
      {
        'email' : email
      }
    );
    return APIResult(responseJson['success'], message: responseJson['data']);
  }

  Future<APIResult> newpassword(String email, String token, String newPassword, String cNewPassword) async {
    final responseJson = await _post(
        'newpassword',
        {
          'email' : email,
          'token' : token,
          'new_pass' : newPassword,
          'cnew_pass': cNewPassword,
        }
    );
    return APIResult(responseJson['success'], message: responseJson['data']);
  }

  Future<List<Member>> getSponsored() async {
    final responseJson = await _get(
        'getsponsored',
        authenticated: true
    );
    if (responseJson['success'] == true && responseJson['data'] != null) {
      List<dynamic> participantsList = responseJson['data'];
      final list = participantsList.map((json) => Member.fromJson(json)).toList();
      return list;
    }
    return List.empty();
  }

  Future<MemberProfile?> getProfile() async {
    final responseJson = await _get(
        'getprofile',
        authenticated: true
    );
    if (responseJson['success'] == true && responseJson['data'] != null) {
      final profile = MemberProfile.fromJson(responseJson['data']);
      return profile;
    }
  }

  Future<APIResult> updateProfile(MemberProfile profile, String? profilePicture) async {
    List<http.MultipartFile>? avatarFile;
    if (profilePicture != null) {
      avatarFile = [
        await http.MultipartFile.fromPath(
            'member_photo',
            profilePicture)
      ];
    }
    final responseJson = await _post(
        'updatesimpleprofile',
        {
          'member_name' : profile.name,
          'member_email' : profile.email,
          'member_country' : profile.country,
          'member_phone': profile.phone,
          'member_city' : profile.city,
          'member_address' : profile.address,
          'member_hobby' : profile.hobby,
          'member_profession' : profile.profession,
          'member_description' : profile.description,
          'member_wallet' : profile.wallet
        },
        authenticated: true,
        multipartFiles: avatarFile,
    );
    return APIResult(responseJson['success'], message: responseJson['data']);
  }

  Future<List<NFTItem>> getNFTList(int offset, int limit) async {
    final responseJson = await _get(
        "getnftlist?offset=$offset&limit=$limit",
        authenticated: true
    );
    if (responseJson['success'] == true && responseJson['data'] != null) {
      List<dynamic> participantsList = responseJson['data'];
      final list = participantsList.map((json) => NFTItem.fromJson(json)).toList();
      return list;
    }
    return List.empty();
  }

  Future<APIResult> buyNFT(String nftId, String transfer, String network, String payment) async {
    final responseJson = await _post(
        "buynft",
      {
        'nft_id' : nftId,
        'nft_transfer' : transfer,
        'nft_network' : network,
      },
      authenticated: true,
      multipartFiles:[
        await http.MultipartFile.fromPath(
            'nft_payment',
            payment)
      ]
    );
    return APIResult(responseJson['success'], message: responseJson['data']);
  }

  Future<List<String>> getPublicChat() async {
    final responseJson = await _get(
        'chat/public',
        authenticated: true
    );
    if (responseJson['success'] == true && responseJson['data'] != null) {
      List<dynamic> roomList = responseJson['data'];
      final list = roomList.map((id) => id as String).toList();
      return list;
    }
    return List.empty();
  }

}

class APIResult<T> {
  final bool success;
  final T? data;
  final String? message;

  APIResult(this.success, {this.data, this.message});
}

class Sponsor {
  final String id;
  final String name;
  Sponsor(this.id, this.name);
}

class Dashboard {
  final String point;
  final String usdt;
  final String token;
  final String affiliate;
  Dashboard(this.point, this.usdt, this.token, this.affiliate);
}