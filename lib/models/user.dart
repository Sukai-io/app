import 'package:sukai/models/sukai_api.dart';

class LoginUser {
  final String username;
  final String token;

  LoginUser(this.username, this.token);

  factory LoginUser.fromJson(Map<String, dynamic> json) {
    return LoginUser(
      json['username'] ?? '',
      json['token'] ?? ''
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'username': this.username,
      'token': this.token
    };
  }
}

class Member {
  final String id;
  final String username;
  final String name;
  final String avatarUrl;
  final String package;

  Member(this.id, this.username, this.name, this.avatarUrl, this.package);

  factory Member.fromJson(Map<String, dynamic> json) {
    final String avatarUrl = json['avatar_url'] ?? '';
    return Member(
        json['id'] ?? '',
        json['username'] ?? '',
        json['name'] ?? '',
        avatarUrl.isNotEmpty?'${SukaiApi.PUB_ASSET}/$avatarUrl':'',
        json['package'] ?? '',
    );
  }
}

class RegisterMember {
  final String sponsor;
  final String username;
  final String name;
  final String email;
  final String password;
  final String idnumber;
  final String idfilepath;
  final String dateofbirth;
  final String gender;
  final String country;
  final String phone;
  final String city;
  final String address;
  final String smoker;
  final String married;
  final String hobby;
  final String profession;

  RegisterMember(
      this.sponsor,
      this.username,
      this.name,
      this.email,
      this.password,
      this.idnumber,
      this.idfilepath,
      this.dateofbirth,
      this.gender,
      this.country,
      this.phone,
      this.city,
      this.address,
      this.smoker,
      this.married,
      this.hobby,
      this.profession
      );
}

class RegisterClient {
  final String sponsor;
  final String username;
  final String name;
  final String email;
  final String password;
  final String idnumber;
  final String idfilepath;
  final String gender;
  final String country;
  final String phone;
  final String city;
  final String companyName;
  final String companyAddress;
  final String brandName;
  final String productType;

  RegisterClient(
      this.sponsor,
      this.username,
      this.name,
      this.email,
      this.password,
      this.idnumber,
      this.idfilepath,
      this.gender,
      this.country,
      this.phone,
      this.city,
      this.companyName,
      this.companyAddress,
      this.brandName,
      this.productType
      );
}

class MemberProfile {
  final String id;
  final String referral;
  final String username;
  final String email;
  final String name;
  final String country;
  final String phone;
  final String avatarUrl;
  final String city;
  final String address;
  final String hobby;
  final String profession;
  String description;
  String wallet;

  MemberProfile(this.id, this.referral, this.username, this.email, this.name, this.country, this.phone, this.avatarUrl, this.city, this.address, this.hobby, this.profession, this.description, this.wallet);

  factory MemberProfile.fromJson(Map<String, dynamic> json) {
    final String avatarUrl = json['avatar_url'] ?? '';
    return MemberProfile(
      json['id'] ?? '',
      json['sponsor_username'] ?? '',
      json['username'] ?? '',
      json['email'] ?? '',
      json['name'] ?? '',
      json['country'] ?? '',
      json['phone'] ?? '',
      avatarUrl.isNotEmpty?'${SukaiApi.PUB_ASSET}/$avatarUrl':'',
      json['city'] ?? '',
      json['address'] ?? '',
      json['hobby'] ?? '',
      json['profession'] ?? '',
      json['description'] ?? '',
      json['wallet'] ?? '',
    );
  }
}