class Participant {
  final DateTime entryDate;
  final String username;
  final String ticket;

  Participant(this.entryDate, this.username, this.ticket);

  factory Participant.fromJson(Map<String, dynamic> json) {
    return Participant(
        DateTime.parse(json['datemodified']??''),
        json['username'] ?? '',
        json['ticket'] ?? ''
    );
  }

}