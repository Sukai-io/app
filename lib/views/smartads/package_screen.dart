import 'package:flutter/material.dart';
import 'package:sukai/views/components/client_appbar.dart';
import 'package:sukai/views/components/client_menu.dart';
import 'package:sukai/views/smartads/topup_screen.dart';
import '../../constants.dart';
import 'components/topup.dart';

class PackageScreen extends StatefulWidget {
  PackageScreen({Key? key}) : super(key: key);

  @override
  _PackageScreenState createState() => _PackageScreenState();
}

class _PackageScreenState extends State<PackageScreen> {
  static double? _radioSize = 30.0;
  int? _package = null;

  @override
  Widget build(BuildContext context) {
    final TextStyle style = TextStyle(
        color: kPrimaryColor,
        fontSize: 15,
        fontWeight: FontWeight.bold
    );

    return Scaffold(
        appBar: ClientAppbar(showDetail: false),
        drawer: ClientMenuDrawer(),
        body: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('CHOOSE PACKAGE', style: style,),
                      Text('Balance: 350 USDT', style: style,),
                    ],
                  ),
                  ElevatedButton(
                      onPressed: (){
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => TopUpScreen())
                        );
                      },
                      child: Text('TOP UP')
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(top:16, bottom: 16),
                alignment: Alignment.centerLeft,
                child: Text(
                  'PACKAGE FOR : SUPER KIDS VITAMIN',
                  style: style,
                ),
              ),
              Row(
                children: [
                  Container(
                    width: _radioSize,
                    height: _radioSize,
                    child: Radio(
                      value: 1,
                      groupValue: _package,
                      onChanged: (int? value) {
                        setState(() {
                          _package = value;
                        });
                      },
                    ),
                  ),
                  Text(' USDT 100 - 1500 Clicks', style: style),
                ],
              ),
              Row(
                children: [
                  Container(
                    width: _radioSize,
                    height: _radioSize,
                    child: Radio(
                      value: 2,
                      groupValue: _package,
                      onChanged: (int? value) {
                        setState(() {
                          _package = value;
                        });
                      },
                    ),
                  ),
                  Text(' USDT 350 - 4500 Clicks + Front Page', style: style),
                ],
              ),
              Row(
                children: [
                  Container(
                    width: _radioSize,
                    height: _radioSize,
                    child: Radio(
                      value: 3,
                      groupValue: _package,
                      onChanged: (int? value) {
                        setState(() {
                          _package = value;
                        });
                      },
                    ),
                  ),
                  Text(' USDT 600 - 7500 Clicks + Front Page & Banner', style: style),
                ],
              ),
              Row(
                children: [
                  Container(
                    width: _radioSize,
                    height: _radioSize,
                    child: Radio(
                      value: 4,
                      groupValue: _package,
                      onChanged: (int? value) {
                        setState(() {
                          _package = value;
                        });
                      },
                    ),
                  ),
                  Text(' Custom Package (Min 1000 USDT)', style: style)
                ],
              ),
              Container(
                alignment: Alignment.centerLeft,
                width: 200,
                padding: EdgeInsets.only(left: _radioSize!.toDouble() + 5.0),
                child: TextField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    suffixIcon: Text('USDT'),
                    suffixIconConstraints: BoxConstraints(minWidth: 0, minHeight: 0),
                  ),
                )
              ),
              SizedBox(height: 16,),
              Expanded(child: TopUp()),
            ],
          ),
        )
    );
  }
}