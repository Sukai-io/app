import 'package:flutter/material.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:sukai/constants.dart';
import 'package:sukai/views/components/client_appbar.dart';
import 'package:sukai/views/components/client_menu.dart';
import 'package:sukai/views/components/imagepicker.dart';
import 'package:sukai/views/smartads/package_screen.dart';

class CreateAdsScreen extends StatelessWidget {
  const CreateAdsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: ClientAppbar(showDetail: false),
        drawer: ClientMenuDrawer(),
        body: CreateForm()
    );
  }
}

class CreateForm extends StatefulWidget {
  const CreateForm({Key? key}) : super(key: key);

  @override
  _CreateFormState createState() => _CreateFormState();
}

class _CreateFormState extends State<CreateForm> {
  RoundedLoadingButtonController _buttonController = RoundedLoadingButtonController();

  @override
  Widget build(BuildContext context) {
    final TextStyle headerStyle = TextStyle(
      color: kPrimaryColor,
      fontSize: 14,
      fontWeight: FontWeight.bold
    );
    return Container(
      padding: EdgeInsets.all(18),
      child: Form(
        child: ListView(
          children: [
            Container(
              alignment: Alignment.center,
              child: Text('CREATE ADS', style: headerStyle,)
            ),
            TextFormField(
              decoration: InputDecoration(
                  hintText: 'Input ads subject',
                  labelText: 'Subject'
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 32),
                alignment: Alignment.centerLeft,
                child: Text('SMART ADVERTISING FILE ADS', style: headerStyle,)
            ),
            TextFormField(
              decoration: InputDecoration(
                  hintText: 'Input ads description',
                  labelText: 'Description'
              ),
              maxLines: null,
              keyboardType: TextInputType.multiline,
            ),
            ImageFormField(
              label: 'Advertisement File',
              onImagePicked: (file){},
            ),
            Container(
                margin: EdgeInsets.only(top: 32),
                alignment: Alignment.centerLeft,
                child: Text('BIG FRONT BANNER FILE ADS', style: headerStyle,)
            ),
            TextFormField(
              decoration: InputDecoration(
                  hintText: 'Input ads description',
                  labelText: 'Description'
              ),
              maxLines: null,
              keyboardType: TextInputType.multiline,
            ),
            ImageFormField(
              label: 'Advertisement File',
              onImagePicked: (file){},
            ),
            Container(
                margin: EdgeInsets.only(top: 32),
                alignment: Alignment.centerLeft,
                child: Text('TARGET AUDIENCE', style: headerStyle,)
            ),
            TextFormField(
              decoration: InputDecoration(
                  hintText: 'Input target country',
                  labelText: 'Country'
              ),
            ),
            TextFormField(
              decoration: InputDecoration(
                  hintText: 'Input target city',
                  labelText: 'City'
              ),
            ),
            TextFormField(
              decoration: InputDecoration(
                  hintText: 'Input target age',
                  labelText: 'Age'
              ),
            ),
            TextFormField(
              decoration: InputDecoration(
                  hintText: 'Input target hobbies',
                  labelText: 'Hobbies'
              ),
            ),
            TextFormField(
              decoration: InputDecoration(
                  hintText: 'Input target gender',
                  labelText: 'Gender'
              ),
            ),
            TextFormField(
              decoration: InputDecoration(
                  hintText: 'Input target smoker',
                  labelText: 'Smoke'
              ),
            ),
            SizedBox(height: 32,),
            RoundedLoadingButton(
              controller: _buttonController,
              color: kPrimaryColor,
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => PackageScreen())
                );
              },
              child: Text('NEXT'),
            ),
            SizedBox(height: 32,),
          ],
        )
      ),
    );
  }
}

