import 'package:flutter/material.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:sukai/views/components/client_appbar.dart';
import 'package:sukai/views/components/client_menu.dart';
import 'package:sukai/views/home/client_screen.dart';

import '../../constants.dart';
import 'components/topup.dart';

class TopUpScreen extends StatefulWidget {
  const TopUpScreen({Key? key}) : super(key: key);

  @override
  _TopUpScreenState createState() => _TopUpScreenState();
}

class _TopUpScreenState extends State<TopUpScreen> {
  RoundedLoadingButtonController _buttonController = RoundedLoadingButtonController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: ClientAppbar(showDetail: false),
        drawer: ClientMenuDrawer(),
        body: Container(
          padding: EdgeInsets.all(5),
          child: TopUp(),
        )
    );
  }
}