import 'package:flutter/material.dart';
import 'package:sukai/views/components/client_appbar.dart';
import 'package:sukai/views/components/client_menu.dart';
import 'package:sukai/views/smartads/components/reseller_card.dart';
import '../../constants.dart';

class ResellerReport extends StatefulWidget {
  ResellerReport({Key? key}) : super(key: key);

  @override
  _ResellerReportState createState() => _ResellerReportState();
}

class _ResellerReportState extends State<ResellerReport> {

  @override
  Widget build(BuildContext context) {
    final TextStyle style = TextStyle(
        color: kPrimaryColor,
        fontSize: 15,
        fontWeight: FontWeight.bold
    );

    return Scaffold(
        appBar: ClientAppbar(showDetail: false),
        drawer: ClientMenuDrawer(),
        body: Container(
          padding: EdgeInsets.all(16),
          color: kSystemColor,
          child: Column(
            children: [
              Center(child: Text('RESELLER REPORT', style: style,)),
              ResellerReportCard(),
              ResellerReportCard(),
            ],
          ),
        )
    );
  }
}