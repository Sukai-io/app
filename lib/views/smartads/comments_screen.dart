import 'package:flutter/material.dart';
import 'package:sukai/views/components/client_appbar.dart';
import 'package:sukai/views/components/client_menu.dart';
import '../../constants.dart';

class CommentsScreen extends StatefulWidget {
  CommentsScreen({Key? key}) : super(key: key);

  @override
  _CommentsScreenState createState() => _CommentsScreenState();
}

class _CommentsScreenState extends State<CommentsScreen> {

  @override
  Widget build(BuildContext context) {
    final TextStyle style = TextStyle(
        color: kPrimaryColor,
        fontSize: 15,
        fontWeight: FontWeight.bold
    );

    return Scaffold(
        appBar: ClientAppbar(showDetail: false),
        drawer: ClientMenuDrawer(),
        body: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            children: [
              Center(child: Text('COMMENTS', style: style,)),
              Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                        color: kSystemColor,
                        borderRadius: BorderRadius.circular(5),
                        border: Border.all(
                            width: 1,
                            color: kPrimaryLightColor,
                            style: BorderStyle.solid
                        )
                    ),
                  )
              ),
            ],
          ),
        )
    );
  }
}