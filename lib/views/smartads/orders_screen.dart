import 'package:flutter/material.dart';
import 'package:sukai/views/components/client_appbar.dart';
import 'package:sukai/views/components/client_menu.dart';
import 'package:sukai/views/smartads/components/reseller_card.dart';
import 'package:sukai/views/smartads/reseller_report.dart';
import '../../constants.dart';

class OrdersScreen extends StatefulWidget {
  OrdersScreen({Key? key}) : super(key: key);

  @override
  _OrdersScreenState createState() => _OrdersScreenState();
}

class _OrdersScreenState extends State<OrdersScreen> {
  static double? _radioSize = 30.0;
  int? _package = null;

  @override
  Widget build(BuildContext context) {
    final TextStyle style = TextStyle(
        color: kPrimaryColor,
        fontSize: 15,
        fontWeight: FontWeight.bold
    );

    return Scaffold(
        appBar: ClientAppbar(showDetail: false),
        drawer: ClientMenuDrawer(),
        body: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(child: Text('RESELLER ORDERS', style: style,)),
              Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                        color: kSystemColor,
                        borderRadius: BorderRadius.circular(5),
                        border: Border.all(
                            width: 1,
                            color: kPrimaryLightColor,
                            style: BorderStyle.solid
                        )
                    ),
                    child: ListView(
                      children: [
                        ResellerOrderCard(),
                        ResellerOrderCard(),
                        ResellerOrderCard(),
                        ResellerOrderCard(),
                        ResellerOrderCard(),
                      ],
                    ),
                  )
              ),
            ],
          ),
        )
    );
  }
}