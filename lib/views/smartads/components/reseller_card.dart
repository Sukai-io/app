import 'package:flutter/material.dart';

import '../../../constants.dart';

class ResellerReportCard extends StatelessWidget {
  const ResellerReportCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextStyle style = TextStyle(
        color: kPrimaryColor,
        fontSize: 14,
        fontWeight: FontWeight.bold
    );
    final TextStyle title = TextStyle(
        color: kTextAccentColor,
        fontSize: 15,
        fontWeight: FontWeight.bold
    );
    return Card(
      child: InkWell(
        onTap: (){},
        child: Container(
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
          ),
          child: Column(
              children: [
                Text('SUPER KIDS VITAMIN', style: title,),
                Container(
                    alignment: Alignment.centerLeft,
                    child: Text('MASTER', style: style,)
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('GOLD PACKAGE'),
                    Text('CHAT: 10 USERS')
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('PLATINUM PACKAGE'),
                    Text('CHAT: 10 USERS')
                  ],
                ),
                SizedBox(height: 16,),
                Container(
                    alignment: Alignment.centerLeft,
                    child: Text('REGULER', style: style,)
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('BRONZE PACKAGE'),
                    Text('CHAT: 10 USERS')
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('SILVER PACKAGE'),
                    Text('CHAT: 10 USERS')
                  ],
                ),
              ]
          ),
        ),
      ),
    );
  }
}

class ResellerOrderCard extends StatelessWidget {
  const ResellerOrderCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        onTap: (){},
        child: Container(
          padding: EdgeInsets.all(5),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('John Doe'),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Waiting Payment', style: TextStyle(
                        fontStyle: FontStyle.italic
                      ),),
                    ],
                  ),
                  ElevatedButton(
                      onPressed: (){
                      },
                      child: Text('CHAT')
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
