import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:line_icons/line_icons.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:sukai/views/components/imagepicker.dart';
import 'package:sukai/views/home/client_screen.dart';

import '../../../constants.dart';

class TopUp extends StatefulWidget {
  const TopUp({Key? key}) : super(key: key);

  @override
  _TopUpState createState() => _TopUpState();
}

class _TopUpState extends State<TopUp> {
  TextEditingController _ethAddrController = TextEditingController(text: '0x4A23D85Cef3B7Aae0284B54FC18b4267B73aDf52');
  TextEditingController _tronAddrController = TextEditingController(text: 'TTFNutwDruuHyqNLXwWx5E617bU91NZ7Bi');
  File? _paymentFile;

  RoundedLoadingButtonController _buttonController = RoundedLoadingButtonController();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      child: ListView(
        children: [
          Text(
            'Payment transfer via:',
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold
            ),
          ),
          TextField(
            controller: _ethAddrController,
            readOnly: true,
            decoration: InputDecoration(
                labelText: 'USDT (ERC20) ETH NETWORK:',
                suffixIcon: IconButton(
                  onPressed: (){
                    Clipboard.setData(ClipboardData(text: _ethAddrController.text));
                    final snackBar = SnackBar(content: Text('Address copied!'));
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  },
                  icon: Icon(LineIcons.copy),
                )
            ),
          ),
          SizedBox(height: 16,),
          Center(
            child: Image.asset('assets/images/usdt-erc20.jpg', width: size.width * 0.5,),
          ),
          SizedBox(height: 16,),
          Center(
            child: Text(
              'OR',
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold
              ),
            ),
          ),
          TextField(
            controller: _tronAddrController,
            readOnly: true,
            decoration: InputDecoration(
                labelText: 'USDT (TRC20) TRON NETWORK:',
                suffixIcon: IconButton(
                  onPressed: (){
                    Clipboard.setData(ClipboardData(text: _tronAddrController.text));
                    final snackBar = SnackBar(content: Text('Address copied!'));
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  },
                  icon: Icon(LineIcons.copy),
                )
            ),
          ),
          SizedBox(height: 16,),
          Center(
            child: Image.asset('assets/images/usdt-trc20.jpg', width: size.width * 0.55,),
          ),
          SizedBox(height: 16,),
          ImageFormField(
            label: 'Send Payment Confirmation',
            onImagePicked: (file){
              setState(() {
                _paymentFile = file;
              });
            },
          ),
          SizedBox(height: 16,),
          RoundedLoadingButton(
            controller: _buttonController,
            color: kPrimaryColor,
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ClientScreen())
              );
            },
            child: Text('CONFIRM'),
          ),
          SizedBox(height: 16,),
        ],
      ),
    );
  }
}
