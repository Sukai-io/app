import 'package:flutter/material.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:sukai/views/components/client_appbar.dart';
import 'package:sukai/views/components/client_menu.dart';
import 'package:sukai/views/home/client_screen.dart';
import 'package:sukai/views/smartads/reseller_report.dart';
import '../../constants.dart';

class ResellerScreen extends StatefulWidget {
  ResellerScreen({Key? key}) : super(key: key);

  @override
  _ResellerScreenState createState() => _ResellerScreenState();
}

class _ResellerScreenState extends State<ResellerScreen> {
  static double? _radioSize = 30.0;
  int? _type = null;

  RoundedLoadingButtonController _buttonController = RoundedLoadingButtonController();

  @override
  Widget build(BuildContext context) {
    final TextStyle style = TextStyle(
        color: kPrimaryColor,
        fontSize: 15,
        fontWeight: FontWeight.bold
    );

    return Scaffold(
        appBar: ClientAppbar(showDetail: false),
        drawer: ClientMenuDrawer(),
        body: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('CREATE RESELLER PRICE', style: style,),
                    ],
                  ),
                  ElevatedButton(
                      onPressed: (){
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => ResellerReport())
                        );
                      },
                      child: Text('VIEW REPORT')
                  ),
                ],
              ),
              Row(
                children: [
                  Container(
                    width: 75,
                    child: Text('Reseller :', style: style),
                  ),
                  Expanded(
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Container(
                                width: _radioSize,
                                height: _radioSize,
                                child: Radio(
                                  value: 1,
                                  groupValue: _type,
                                  onChanged: (int? value) {
                                    setState(() {
                                      _type = value;
                                    });
                                  },
                                ),
                              ),
                              Text('Master', style: style),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                width: _radioSize,
                                height: _radioSize,
                                child: Radio(
                                  value: 2,
                                  groupValue: _type,
                                  onChanged: (int? value) {
                                    setState(() {
                                      _type = value;
                                    });
                                  },
                                ),
                              ),
                              Text('Regular', style: style),
                            ],
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
              TextFormField(
                decoration: InputDecoration(
                  labelText: 'Title',
                  hintText: 'Input reseller title',
                ),
              ),
              TextFormField(
                decoration: InputDecoration(
                  labelText: 'Quantity',
                  hintText: 'Input minimum purchase',
                ),
              ),
              TextFormField(
                decoration: InputDecoration(
                  labelText: 'USDT',
                  hintText: 'Input reseller price in USDT',
                ),
              ),
              SizedBox(height: 16,),
              RoundedLoadingButton(
                controller: _buttonController,
                color: kPrimaryColor,
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ClientScreen())
                  );
                },
                child: Text('SAVE'),
              ),
              SizedBox(height: 32,),
              Container(
                  alignment: Alignment.centerLeft,
                  child: Text('LIST RESELLER PRICE', style: style,)
              ),
              Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(
                        width: 1,
                        color: kPrimaryLightColor,
                        style: BorderStyle.solid
                      )
                    ),
                  )
              ),
            ],
          ),
        )
    );
  }
}