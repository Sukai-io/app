import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:sukai/models/nft.dart';
import 'package:sukai/models/skyx_api.dart';
import 'nft_item.dart';

typedef NFTFetch = Future<List<SkyXItem>> Function(int offset, int size);

class NFTGrid extends StatefulWidget {
  final NFTFetch nftFetch;
  const NFTGrid({required this.nftFetch, Key? key}) : super(key: key);

  @override
  _NFTGridState createState() => _NFTGridState();
}

class _NFTGridState extends State<NFTGrid> {
  static const _pageSize = 4;

  final PagingController<int, SkyXItem> _pagingController =
  PagingController(firstPageKey: 0);

  Future<void> _fetchPage(int pageKey) async {
    try {
      final newItems = await widget.nftFetch(pageKey, _pageSize);
      final isLastPage = newItems.length < _pageSize;
      if (isLastPage) {
        _pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageKey + newItems.length;
        _pagingController.appendPage(newItems, nextPageKey);
      }
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  void initState() {
    super.initState();
    _pagingController.addPageRequestListener( (pageKey) {
      _fetchPage(pageKey);
    });
  }

  @override
  Widget build(BuildContext context) =>
      RefreshIndicator(
        onRefresh: () => Future.sync(() => _pagingController.refresh()),
        child: PagedGridView(
            pagingController: _pagingController,
            builderDelegate: PagedChildBuilderDelegate<SkyXItem>(
              itemBuilder: (context, item, index) => NFTItemView(item: item,)
            ),
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(childAspectRatio: 100 / 120,
              crossAxisSpacing: 15,
              mainAxisSpacing: 15,
              crossAxisCount: 2,),
        ),
      );
}


