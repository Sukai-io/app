import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:sukai/models/nft.dart';
import 'package:sukai/models/skyx_api.dart';
import 'package:sukai/models/sukai_api.dart';

import 'nft_item.dart';

class NFTList extends StatefulWidget {
  const NFTList({Key? key}) : super(key: key);

  @override
  _NFTListState createState() => _NFTListState();
}

class _NFTListState extends State<NFTList> {
  static const _pageSize = 5;

  final PagingController<int, NFTItem> _pagingController =
  PagingController(firstPageKey: 0);

  Future<List<NFTItem>> _getNFTList(int offset){
    return SukaiApi().getNFTList(offset, _pageSize);
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      final newItems = await _getNFTList(pageKey);
      final isLastPage = newItems.length < _pageSize;
      if (isLastPage) {
        _pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageKey + newItems.length;
        _pagingController.appendPage(newItems, nextPageKey);
      }
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  void initState() {
    super.initState();
    _pagingController.addPageRequestListener( (pageKey) {
      _fetchPage(pageKey);
    });
  }

  @override
  Widget build(BuildContext context) =>
      RefreshIndicator(
        onRefresh: () => Future.sync(() => _pagingController.refresh()),
        child: PagedListView(
          pagingController: _pagingController,
          builderDelegate: PagedChildBuilderDelegate<SkyXItem>(
              itemBuilder: (context, item, index) => NFTItemView(item: item,)
          ),
        ),
      );
}


