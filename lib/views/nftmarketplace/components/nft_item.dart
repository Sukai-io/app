import 'package:flutter/material.dart';
import 'package:sukai/models/skyx_api.dart';
import 'package:sukai/views/nftmarketplace/nft_detail_screen.dart';

import '../../../constants.dart';

class NFTItemView extends StatelessWidget {
  final SkyXItem item;
  const NFTItemView({Key? key, required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(4),
            child: Text(
              item.metadata.name,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.bold
              ),
            ),
          ),
          Flexible(
              flex: 1,
              child: Container(
                padding: EdgeInsets.all(4),
                child: GestureDetector(
                  onTap: (){
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => NFTDetailScreen(nftItem: item))
                    );
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                          image: Image.network(item.metadata.file).image,
                          fit: BoxFit.cover,
                        ),
                        borderRadius: BorderRadius.circular(5),
                        border: Border.all(
                            color: kPrimaryColor,
                            width: 1
                        )
                    ),
                  ),
                ),
              )
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(4),
            child: Text(
              item.metadata.description,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  color: kSecondaryColor
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerRight,
            padding: EdgeInsets.all(4),
            child: Text(
              item.tokenPriceEther.toString() + " BNB",
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.left,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: kPrimaryColor
              ),
            ),
          ),
        ],
      ),
    );
  }
}