import 'package:flutter/material.dart';
import 'package:sukai/constants.dart';
import 'package:sukai/models/skyx_api.dart';
import 'package:sukai/models/sukai_api.dart';
import 'package:sukai/views/components/menu.dart';
import 'package:sukai/views/nftmarketplace/components/nft_grid.dart';
import 'package:sukai/views/nftmarketplace/nft_create_screen.dart';
import 'package:sukai/views/nftmarketplace/nft_gallery_screen.dart';

class NFTMyScreen extends StatefulWidget {
  const NFTMyScreen({Key? key}) : super(key: key);

  @override
  _NFTMyScreenState createState() => _NFTMyScreenState();
}

class _NFTMyScreenState extends State<NFTMyScreen> with SingleTickerProviderStateMixin {
  String _userName = 'username';
  String _wallet = '';
  String _description = '';
  bool _hasWallet = false;
  ImageProvider _avatar = Image.asset('assets/icons/ic_profile.png').image;
  late TabController _tabController;

  @override
  void initState() {
    final loginUser = SukaiApi().currentUser;
    if (loginUser != null) {
      _userName = loginUser.name;
      if (loginUser.avatarUrl.isNotEmpty) {
        _avatar = Image.network(loginUser.avatarUrl).image;
      }
      _loadProfile();
    }
    _tabController = new TabController(length: 4, vsync: this);
    super.initState();
  }

  void _loadProfile() async {
    final profile = await SukaiApi().getProfile();
    if (profile != null) {
      setState(() {
        _wallet = profile.wallet;
        _description = profile.description;
        if (_wallet.isNotEmpty) {
          _hasWallet = true;
        }
      });
      final collections = await SkyXApi().getCollections(_wallet);
      debugPrint(collections.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("MY NFT"),
        ),
        drawer: MenuDrawer(),
        body: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.all(8),
                  child: Column(
                    children: [
                      Container(
                        width: 90,
                        height: 90,
                        padding: EdgeInsets.all(8),
                        child: CircleAvatar(
                          backgroundColor: Colors.white,
                          backgroundImage: _avatar,
                        ),
                      ),
                      Text(_userName),
                      Text(_wallet),
                      Text(_description),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Spacer(),
                          ElevatedButton(
                            onPressed: (){
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) => NFTGalleryScreen())
                              );
                            },
                            child: Text("NFT Gallery"),
                            style: ElevatedButton.styleFrom(
                              primary: kPrimaryColor,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                              padding: EdgeInsets.symmetric(horizontal: 4),
                            ),
                          ),
                          SizedBox(width: 8,),
                          ElevatedButton(
                            onPressed: (){
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) => NFTCreateScreen())
                              );
                            },
                            child: Text("Create NFT"),
                            style: ElevatedButton.styleFrom(
                              primary: kPrimaryColor,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                              padding: EdgeInsets.symmetric(horizontal: 4),
                            ),
                          ),
                          Spacer(),
                        ],
                      )
                    ],
                  ),
                ),
                TabBar(
                  unselectedLabelColor: kPrimaryColor,
                  labelColor: kTextAccentColor,
                  controller: _tabController,
                  indicatorColor: kPrimaryColor,
                  indicatorSize: TabBarIndicatorSize.tab,
                  labelPadding: EdgeInsets.all(8),
                  tabs: [
                    Tab(
                      text: 'COLLECTION',
                    ),
                    Tab(
                      text: 'SOLD',
                    ),
                    Tab(
                      text: 'CREATED',
                    ),
                    Tab(
                      text: 'BOUGHT',
                    )
                  ],
                ),
                Expanded(
                  child: TabBarView(
                    children: [
                      NFTGrid(nftFetch: (offset, size) => SkyXApi().getCollections(_wallet)),
                      NFTGrid(nftFetch: (offset, size) => SkyXApi().getCollections(_wallet)),
                      NFTGrid(nftFetch: (offset, size) => SkyXApi().getCollections(_wallet)),
                      NFTGrid(nftFetch: (offset, size) => SkyXApi().getCollections(_wallet))
                    ],
                    controller: _tabController,
                  ),
                ),
              ],
            )
        )
    );
  }
}


