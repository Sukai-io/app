import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:sukai/wallet_connect.dart';
import 'dart:io';
import '../../constants.dart';
import 'components/nft_imagepicker.dart';

class NFTCreateScreen extends StatefulWidget {
  const NFTCreateScreen({Key? key}) : super(key: key);

  @override
  _NFTCreateScreenState createState() => _NFTCreateScreenState();
}

class _NFTCreateScreenState extends State<NFTCreateScreen> {
  final _formKey = GlobalKey<FormState>();
  File? _idFile;
  final TextEditingController _nftNameController = TextEditingController();
  final TextEditingController _nftDescController = TextEditingController();
  final TextEditingController _nftPriceController = TextEditingController();
  final TextEditingController _serviceFeeController = TextEditingController();
  final TextEditingController _receiveController = TextEditingController();

  RoundedLoadingButtonController _buttonController = RoundedLoadingButtonController();

  @override
  void initState() {
    _serviceFeeController.text = "0";
    _receiveController.text = "0";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("CREATE NFT")),
        body: Container(
          padding: EdgeInsets.all(16),
          child: Form(
            key: _formKey,
            child: ListView(
              children: [
                Container(
                  child: Column(
                    children: [
                      Text("PNG, GIF, JPG (max 2MB)"),
                      NFTImageFormField(
                        label: 'Choose File',
                        onImagePicked: (file) {
                          setState(() {
                            _idFile = file;
                          });
                        },
                      ),
                    ],
                  ),
                ),
                TextFormField(
                  controller: _nftNameController,
                  decoration: InputDecoration(
                    hintText: 'Input NFT name',
                    labelText: 'NFT Name',
                  ),
                  validator: (String? value){
                    if (value == null || value.isEmpty) return 'NFT name cannot be empty';
                    return null;
                  },
                ),
                TextFormField(
                  controller: _nftDescController,
                  minLines: 4,
                  maxLines: 6,
                  maxLength: 300,
                  maxLengthEnforcement: MaxLengthEnforcement.enforced,
                  decoration: InputDecoration(
                    labelText: 'NFT Description (max: 300 characters)',
                    floatingLabelBehavior: FloatingLabelBehavior.always
                  ),
                  validator: (String? value){
                    if (value == null || value.isEmpty) return 'NFT description cannot be empty';
                    return null;
                  },
                ),
                TextFormField(
                  controller: _nftPriceController,
                  decoration: InputDecoration(
                    hintText: '',
                    labelText: 'NFT Price (BNB)',
                  ),
                  validator: (String? value){
                    if (value == null || value.isEmpty) return 'NFT price cannot be empty';
                    return null;
                  },
                ),
                TextFormField(
                  enabled: false,
                  controller: _serviceFeeController,
                  textAlign: TextAlign.end,
                  decoration: InputDecoration(
                    labelText: 'Service Fee',
                    border: InputBorder.none,
                    suffixText: "BNB"
                  )
                ),
                TextFormField(
                  enabled: false,
                  controller: _receiveController,
                  textAlign: TextAlign.end,
                  decoration: InputDecoration(
                    labelText: 'You will receive',
                    border: InputBorder.none,
                    suffixText: "BNB"
                  )
                ),
                SizedBox(height: 32,),
                RoundedLoadingButton(
                  controller: _buttonController,
                  color: kPrimaryColor,
                  child: Text('SUBMIT', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                  onPressed: (){
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => WalletConnectApp())
                    );
                    _buttonController.reset();
                  },
                )
              ],
            ),
          ),
        ),
    );
  }
}
