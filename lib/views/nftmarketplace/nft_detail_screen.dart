import 'package:flutter/material.dart';
import 'package:sukai/models/skyx_api.dart';
import '../../constants.dart';
import 'nft_buy_screen.dart';

class NFTDetailScreen extends StatefulWidget {
  final SkyXItem nftItem;

  const NFTDetailScreen({Key? key, required this.nftItem}) : super(key: key);

  @override
  _NFTDetailScreenState createState() => _NFTDetailScreenState();
}

class _NFTDetailScreenState extends State<NFTDetailScreen> with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    _tabController = new TabController(length: 3, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("DETAIL NFT"),),
        body: Column(
          children: [
            Flexible(
              flex: 2,
              child: Container(
                padding: EdgeInsets.fromLTRB(16, 16, 16, 4),
                child: ListView(
                  children: [
                    Center(
                      child: Text(
                        widget.nftItem.metadata.name,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Container(
                      height: 200,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                            image: Image.network(widget.nftItem.metadata.file).image,
                            fit: BoxFit.cover,
                          ),
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                              color: kPrimaryColor,
                              width: 1
                          )
                      ),
                    ),
                    TextFormField(
                        enabled: false,
                        initialValue: widget.nftItem.tokenPriceEther.toString() + " BNB",
                        decoration: InputDecoration(
                          labelText: 'Price',
                          border: InputBorder.none,
                        )
                    ),
                    TextFormField(
                      enabled: false,
                      initialValue: widget.nftItem.metadata.description,
                      maxLines: null,
                      decoration: InputDecoration(
                        labelText: 'Description',
                        border: InputBorder.none,
                      ),
                      textAlign: TextAlign.justify,
                    ),
                    SizedBox(height: 16,),
                    ElevatedButton(
                      onPressed: (){
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => NFTBuyScreen(nftItem: widget.nftItem))
                        );
                      },
                      child: Text("BUY NFT"),
                      style: ElevatedButton.styleFrom(
                        primary: kPrimaryColor,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                        padding: EdgeInsets.symmetric(horizontal: 4),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Flexible(
              flex: 1,
              child: Container(
                padding: EdgeInsets.fromLTRB(16, 4, 16, 4),
                child: Column(
                  children: [
                    TabBar(
                      unselectedLabelColor: kPrimaryColor,
                      labelColor: kTextAccentColor,
                      controller: _tabController,
                      indicatorColor: kPrimaryColor,
                      indicatorSize: TabBarIndicatorSize.tab,
                      labelPadding: EdgeInsets.all(8),
                      tabs: [
                        Tab(
                          text: 'OWNER',
                        ),
                        Tab(
                          text: 'HISTORY',
                        ),
                        Tab(
                          text: 'INFO',
                        ),
                      ],
                    ),
                    Expanded(
                      child: TabBarView(
                        children: [
                          Text('OWNER'),
                          Text('HISTORY'),
                          Text('INFO'),
                        ],
                        controller: _tabController,
                      ),
                    ),
                  ],
                )
              ),
            )
          ],
        )
    );
  }
}