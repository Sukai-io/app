import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:line_icons/line_icons.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:sukai/models/skyx_api.dart';
import 'package:sukai/models/sukai_api.dart';
import 'package:sukai/views/components/alerts.dart';
import 'package:sukai/views/components/imagepicker.dart';
import 'dart:io';

import '../../constants.dart';

class NFTBuyScreen extends StatefulWidget {
  final SkyXItem nftItem;

  const NFTBuyScreen({Key? key, required this.nftItem}) : super(key: key);

  @override
  _NFTBuyScreenState createState() => _NFTBuyScreenState();
}

class _NFTBuyScreenState extends State<NFTBuyScreen> {
  TextEditingController _busdAddrController = TextEditingController(text: '0xbfec1a4a07d0078fc249ed8eabc87439d36c5b97');
  TextEditingController _transferController = TextEditingController();
  RoundedLoadingButtonController _buttonController = RoundedLoadingButtonController();
  File? _paymentFile;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
        appBar: AppBar(title: Text(widget.nftItem.metadata.name),),
        body: Container(
            padding: EdgeInsets.all(16),
            child: ListView(
              children: [
                Center(
                  child: Text(
                    widget.nftItem.address,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
                Container(
                  height: 250,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                        image: Image.network(widget.nftItem.metadata.file).image,
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(
                          color: kPrimaryColor,
                          width: 1
                      )
                  ),
                ),
                TextFormField(
                    enabled: false,
                    initialValue: widget.nftItem.metadata.name,
                    decoration: InputDecoration(
                      labelText: 'Title',
                      border: InputBorder.none,
                    )
                ),
                TextFormField(
                  enabled: false,
                  initialValue: widget.nftItem.metadata.description,
                  maxLines: null,
                  decoration: InputDecoration(
                    labelText: 'Description',
                    border: InputBorder.none,
                  ),
                  textAlign: TextAlign.justify,
                ),
                TextFormField(
                    enabled: false,
                    initialValue: widget.nftItem.tokenPriceEther.toString() + " BNB",
                    decoration: InputDecoration(
                      labelText: 'Price',
                      border: InputBorder.none,
                    )
                ),
                TextFormField(
                    enabled: false,
                    initialValue: widget.nftItem.metadata.network.toUpperCase(),
                    decoration: InputDecoration(
                      labelText: 'NFT Network',
                      border: InputBorder.none,
                    )
                ),
                Text(
                  'Transfer BUSD Payment via:',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold
                  ),
                ),
                TextField(
                  controller: _busdAddrController,
                  readOnly: true,
                  decoration: InputDecoration(
                      labelText: 'SUKAI BUSD Address:',
                      suffixIcon: IconButton(
                        onPressed: (){
                          Clipboard.setData(ClipboardData(text: _busdAddrController.text));
                          final snackBar = SnackBar(content: Text('Address copied!'));
                          ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        },
                        icon: Icon(LineIcons.copy),
                      )
                  ),
                ),
                SizedBox(height: 16,),
                Center(
                  child: Image.asset('assets/images/sukai-busd.jpg', width: size.width * 0.5,),
                ),
                TextFormField(
                  controller: _transferController,
                  decoration: InputDecoration(
                      hintText: 'Your ' + widget.nftItem.metadata.network.toUpperCase() + ' address',
                      labelText: 'Input Transfer Address Ownership'
                  ),
                  validator: (String? value){
                    if (value == null || value.isEmpty) return 'Transfer address cannot be empty';
                    return null;
                  },
                ),
                ImageFormField(
                  label: 'Send Payment Confirmation',
                  onImagePicked: (file){
                    setState(() {
                      _paymentFile = file;
                    });
                  },
                ),
                SizedBox(height: 16,),
                RoundedLoadingButton(
                  controller: _buttonController,
                  color: kPrimaryColor,
                  onPressed: () async {

                    try {
                      if (_transferController.text.isEmpty) {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) => ErrorAlert(errorMessage: 'Transfer address is required.')
                        );
                        _buttonController.reset();
                        return;
                      }

                      if (_paymentFile == null) {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) => ErrorAlert(errorMessage: 'Payment confirmation is required.')
                        );
                        _buttonController.reset();
                        return;
                      }

                      final result = await SukaiApi().buyNFT(
                          widget.nftItem.id,
                          _transferController.text,
                          widget.nftItem.metadata.network.toUpperCase(),
                          _paymentFile!.path);
                      if (result.success) {
                        _buttonController.success();
                        showDialog(
                            context: context,
                            builder: (BuildContext context) => AlertDialog(
                              title: Text('Success'),
                              content: Text(result.message??"Buy request submitted."),
                              actions: [
                                TextButton(
                                    onPressed: () {
                                      Navigator.pop(context, 'OK');
                                      Navigator.pop(context, 'OK');
                                    },
                                    child: Text('OK'))
                              ],
                            )
                        );
                      }else{
                        _buttonController.reset();
                        showDialog(
                            context: context,
                            builder: (BuildContext context) => ErrorAlert(errorMessage: result.message??'Request Failed')
                        );
                      }
                    } catch (e) {
                      _buttonController.reset();
                      showDialog(
                          context: context,
                          builder: (BuildContext context) => ErrorAlert(errorMessage: e.toString())
                      );
                    }


                  },
                  child: Text('CONFIRM', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                )
              ],
            )
        )
    );
  }
}