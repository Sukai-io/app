import 'package:flutter/material.dart';
import 'package:sukai/models/skyx_api.dart';
import 'package:sukai/views/nftmarketplace/components/nft_grid.dart';

class NFTGalleryScreen extends StatelessWidget {
  const NFTGalleryScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('NFT GALLERY'),
        ),
        body: NFTGrid(nftFetch: (offset, size) => SkyXApi().getTokenSale())
    );
  }
}