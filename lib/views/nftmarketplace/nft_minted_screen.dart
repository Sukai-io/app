import 'package:flutter/material.dart';
import 'package:sukai/models/nft.dart';
import 'package:sukai/views/nftmarketplace/nft_my_screen.dart';

import '../../constants.dart';

class NFTMintedScreen extends StatefulWidget {
  final NFTItem nftItem;

  const NFTMintedScreen({Key? key, required this.nftItem}) : super(key: key);

  @override
  _NFTMintedScreenState createState() => _NFTMintedScreenState();
}

class _NFTMintedScreenState extends State<NFTMintedScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("NEW MINTED NFT"),),
        body: Container(
            padding: EdgeInsets.all(16),
            child: ListView(
              children: [
                Center(
                  child: Text(
                    widget.nftItem.title,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ),
                SizedBox(height: 16,),
                Container(
                  height: 250,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                        image: Image.network(widget.nftItem.image).image,
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(
                          color: kPrimaryColor,
                          width: 1
                      )
                  ),
                ),
                TextFormField(
                    enabled: false,
                    initialValue: widget.nftItem.price.toString() + " " +  widget.nftItem.currency,
                    decoration: InputDecoration(
                      labelText: 'Price',
                      border: InputBorder.none,
                    )
                ),
                TextFormField(
                  enabled: false,
                  initialValue: widget.nftItem.description,
                  maxLines: null,
                  decoration: InputDecoration(
                    labelText: 'Description',
                    border: InputBorder.none,
                  ),
                  textAlign: TextAlign.justify,
                ),
                SizedBox(height: 16,),
                ElevatedButton(
                  onPressed: (){
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => NFTMyScreen())
                    );
                  },
                  child: Text("MY NFT"),
                  style: ElevatedButton.styleFrom(
                    primary: kPrimaryColor,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                    padding: EdgeInsets.symmetric(horizontal: 4),
                  ),
                ),
              ],
            )
        )
    );
  }
}