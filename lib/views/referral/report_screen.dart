import 'package:flutter/material.dart';
import 'package:sukai/constants.dart';
import 'package:sukai/models/sukai_api.dart';
import 'package:sukai/views/components/menu.dart';
import 'package:sukai/views/referral/components/downline_list.dart';

class ReportScreen extends StatefulWidget {
  const ReportScreen({Key? key}) : super(key: key);

  @override
  _ReportScreenState createState() => _ReportScreenState();
}

class _ReportScreenState extends State<ReportScreen> {
  Dashboard _dashboard = Dashboard("0", "0", "0", "0");

  void _getDashboard() async {
    final dashboard = await SukaiApi().dashboard();
    setState(() {
      _dashboard = dashboard;
    });
  }

  @override
  void initState() {
    super.initState();
    _getDashboard();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Referral'),
      ),
      drawer: MenuDrawer(),
      body: Container(
        padding: EdgeInsets.all(16),
        child: Column(
          children: [
            Row(
              children: [
                Text('You have ' + _dashboard.affiliate + ' Referrals'),
              ],
            ),
            Container(
              margin: EdgeInsets.only(top:16, bottom: 4),
              padding: EdgeInsets.all(8),
              color: kSystemColor,
              child: Row(
                children: [
                  Spacer(),
                  Text('Name'),
                  Spacer(),
                  Text('Username'),
                  Spacer(),
                ],
              ),
            ),
            Expanded(child: DownLineList())
          ],
        ),
      )
    );
  }
}
