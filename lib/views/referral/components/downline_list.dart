import 'package:flutter/material.dart';
import 'package:sukai/models/sukai_api.dart';
import 'package:sukai/models/user.dart';
import '../../../constants.dart';

class DownLineList extends StatefulWidget {
  const DownLineList({Key? key}) : super(key: key);

  @override
  _DownLineListState createState() => _DownLineListState();
}

class _DownLineListState extends State<DownLineList> {
  Future<List<Member>> _getSponsored(){
    return SukaiApi().getSponsored();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Member>>(
        future: _getSponsored(),
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.connectionState == ConnectionState.done) {
            if ( snapshot.data!.length <= 0 ) {
              return Center(child: Text('No referral data'),);
            }
            return ListView.builder(
                itemCount: snapshot.data?.length,
                itemBuilder: (context, index) {
                  Member downline = snapshot.data![index];
                  return Card(
                    color: index%2==1?kRowColor:Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Text((index+1).toString() + ". " + downline.name),
                              Spacer(),
                              Text(downline.username),
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                });
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        }
    );
  }
}