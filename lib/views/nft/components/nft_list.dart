import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:sukai/models/nft.dart';
import 'package:sukai/models/sukai_api.dart';
import 'package:sukai/views/nft/nft_buy_screen.dart';

import '../../../constants.dart';

class NFTList extends StatefulWidget {
  const NFTList({Key? key}) : super(key: key);

  @override
  _NFTListState createState() => _NFTListState();
}

class _NFTListState extends State<NFTList> {
  static const _pageSize = 4;

  final PagingController<int, NFTItem> _pagingController =
  PagingController(firstPageKey: 0);

  Future<List<NFTItem>> _getNFTList(int offset){
    return SukaiApi().getNFTList(offset, _pageSize);
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      final newItems = await _getNFTList(pageKey);
      final isLastPage = newItems.length < _pageSize;
      if (isLastPage) {
        _pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageKey + newItems.length;
        _pagingController.appendPage(newItems, nextPageKey);
      }
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  void initState() {
    super.initState();
    _pagingController.addPageRequestListener( (pageKey) {
      _fetchPage(pageKey);
    });
  }

  @override
  Widget build(BuildContext context) =>
      RefreshIndicator(
        onRefresh: () => Future.sync(() => _pagingController.refresh()),
        child: PagedGridView(
            pagingController: _pagingController,
            builderDelegate: PagedChildBuilderDelegate<NFTItem>(
              itemBuilder: (context, item, index) => NFTItemView(item: item,)
            ),
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(childAspectRatio: 100 / 120,
              crossAxisSpacing: 15,
              mainAxisSpacing: 15,
              crossAxisCount: 2,),
        ),
      );
}

class NFTItemView extends StatelessWidget {
  final NFTItem item;
  const NFTItemView({Key? key, required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(4),
          child: Text(
            item.title,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.bold
            ),
          ),
        ),
        Flexible(
            flex: 1,
            child: Container(
              padding: EdgeInsets.all(4),
              child: GestureDetector(
                onTap: (){
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => NFTBuyScreen(nftItem: item))
                  );
                },
                child: Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                        image: Image.network(item.image).image,
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(
                          color: kPrimaryColor,
                          width: 1
                      )
                  ),
                ),
              ),
            )
        ),
        Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.all(4),
          child: Text(
            item.description,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: kSecondaryColor
            ),
          ),
        ),
        Container(
          alignment: Alignment.centerRight,
          padding: EdgeInsets.all(4),
          child: Text(
            item.price.toString() + " " + item.currency,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.left,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: kPrimaryColor
            ),
          ),
        ),
      ],
    );
  }
}
