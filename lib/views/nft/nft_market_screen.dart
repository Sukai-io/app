import 'package:flutter/material.dart';
import 'package:sukai/constants.dart';
import 'package:sukai/views/components/menu.dart';
import 'package:url_launcher/url_launcher.dart';

import 'components/nft_list.dart';

class NFTMarketScreen extends StatelessWidget {
  const NFTMarketScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
          appBar: AppBar(
            title: Text('NFT MARKETPLACE'),
            bottom: TabBar(
              tabs: [
                Tab(text: "GALLERY",),
                Tab(text: "ADD NFT",),
                Tab(text: "CREATE NFT",),
              ],
            ),
          ),
          drawer: MenuDrawer(),
          body: TabBarView(
            children: [
              Container(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Text("Gallery is the place where you can see all uploaded NFT by NFT Creator/Owner and you can buy Creator/Owner NFT here."),
                    ),
                    Container(height: 1, width: double.infinity, color: kPrimaryLightColor,),
                    Expanded(child: NFTList())
                  ],
                )
              ),
              Container(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Text("If you already uploaded/created NFTs in other NFT Marketplace then you want also to publish those NFTs in Sukai NFT Marketplace. We can help you. Just Press below button to proceed."),
                    ),
                    Container(height: 1, width: double.infinity, color: kPrimaryLightColor,),
                    ElevatedButton(
                        onPressed: () async {
                          final url = "https://forms.gle/7FJB3NrcMXZhx2Y58";
                          await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';
                        },
                        child: Text('ADD MY NFT', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
                    ),
                  ],
                ),
              ),
              Container(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Text("If you have Digital Artwork and do not know how to create NFT and do not know how to upload it in NFT Marketplace. We can help you to Create the NFT for your Digital Artwork. Just press below button to proceed."),
                    ),
                    Container(height: 1, width: double.infinity, color: kPrimaryLightColor,),
                    ElevatedButton(
                      onPressed: () async {
                        final url = "https://forms.gle/3MAmcnsFJbBrrYBx6";
                        await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';
                      },
                      child: Text('CREATE NFT FOR ME', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
                    ),
                  ],
                ),
              )
            ],
          )
      ),
    );
  }
}