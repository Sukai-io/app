import 'package:sukai/models/sukai_api.dart';

class LoginViewModel  {
  final String username;
  final String password;
  final bool rememberPassword;

  LoginViewModel(this.username, this.password, this.rememberPassword);

  Future<bool> login() async {
    final loginUser = await SukaiApi().authenticate(username, password, rememberPassword);
    return loginUser != null;
  }
}