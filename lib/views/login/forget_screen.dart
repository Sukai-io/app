import 'package:flutter/material.dart';
import 'package:sukai/models/sukai_api.dart';
import 'package:sukai/views/components/alerts.dart';
import 'package:sukai/views/login/reset_screen.dart';

import '../../constants.dart';

class ForgetScreen extends StatefulWidget {
  @override
  _ForgetScreenState createState() => _ForgetScreenState();
}

class _ForgetScreenState extends State<ForgetScreen> {
  final _emailController = TextEditingController();
  bool _emailValid = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Forgot Password'),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
        child: Column(
          children: [
            Text('To reset your password, input your email address below.', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
            SizedBox(height: 32,),
            TextFormField(
              controller: _emailController,
              validator: (String? value) {
                _emailValid = false;
                if (value == null || value.isEmpty) return 'Email cannot be empty';
                bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value);
                if (!emailValid) return 'Email format not valid';
                _emailValid = true;
              },
              autovalidateMode: AutovalidateMode.onUserInteraction,
            ),
            SizedBox(height: 32,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  flex: 10,
                  child: ElevatedButton(
                      onPressed: () async {
                        Navigator.pop(context);
                      },
                      child: Text('CANCEL', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
                      style: ElevatedButton.styleFrom(
                        primary: kSecondaryColor,
                        shape: StadiumBorder(),
                        padding: EdgeInsets.symmetric(vertical: 16),
                      )
                  ),
                ),
                Spacer(flex: 1,),
                Expanded(
                  flex: 10,
                  child: ElevatedButton(
                      onPressed: () async {
                        if (!_emailValid) {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) => ErrorAlert(errorMessage: 'Email invalid.')
                          );
                          return;
                        }
                        final result = await SukaiApi().forgotpassword(_emailController.text);
                        if (result.success) {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) => SuccessAlert(
                                successMessage: result.message??'Password reset email sent.',
                                onDismiss: (){
                                  Navigator.push(context,
                                      MaterialPageRoute(builder: (context) => ResetScreen(email: _emailController.text,))
                                  );
                                },
                              )
                          );
                        }else{
                          showDialog(
                              context: context,
                              builder: (BuildContext context) => ErrorAlert(errorMessage: result.message??'Reset password failed.')
                          );
                        }
                      },
                      child: Text('RESET', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
                      style: ElevatedButton.styleFrom(
                        shape: StadiumBorder(),
                        padding: EdgeInsets.symmetric(vertical: 16),
                      )
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}