import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:sukai/constants.dart';
import 'package:sukai/views/components/alerts.dart';
import 'package:sukai/views/components/dialogs.dart';
import 'package:sukai/views/login/forget_screen.dart';
import 'package:sukai/views/login/login_view_model.dart';
import 'package:sukai/views/register/simple_register_screen.dart';
import '../../../main.dart';
import 'background.dart';

class Body extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  bool _rememberPass = true;
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  String _appVersion = '-';

  dispose(){
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _getVersion();
  }

  void _getVersion() async {
    try {
      PackageInfo packageInfo = await PackageInfo.fromPlatform();

      // String appName = packageInfo.appName;
      String version = packageInfo.version;
      String buildNumber = packageInfo.buildNumber;

      setState(() {
        _appVersion = 'v$version($buildNumber)';
      });
    } catch(e) {
      print(e);
    }
  }

  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: Container(
        padding: EdgeInsets.all(16),
        height: size.height,
        width: double.infinity,
        child: Wrap(
          runAlignment: WrapAlignment.end,
          // mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "Hello !",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 56, color: kTextAccentColor),
                textAlign: TextAlign.left,
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "Sukai is an application that combines video advertising, NFT, and marketplace.",
                style: TextStyle(fontSize: 14, color: kTextAccentColor),
                textAlign: TextAlign.left,
              ),
            ),
            SizedBox(height: 10),
            Align(
              alignment: Alignment.centerLeft,
              child: Text('Username', style: TextStyle(color: kPrimaryColor, fontSize: 16),)
            ),
            TextField(
              controller: _usernameController,
              decoration: InputDecoration(
                hintText: 'Type your username here',
                enabledBorder: UnderlineInputBorder()
              ),
            ),
            SizedBox(height: 10),
            Align(
                alignment: Alignment.centerLeft,
                child: Text('Password', style: TextStyle(color: kPrimaryColor, fontSize: 16),)
            ),
            TextField(
              controller: _passwordController,
              obscureText: true,
              enableSuggestions: false,
              autocorrect: false,
              decoration: InputDecoration(
                  hintText: 'Type your password here',
                  enabledBorder: UnderlineInputBorder()
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                GestureDetector(
                  onTap: (){
                    Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ForgetScreen())
                    );
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric( vertical: 8.0),
                    child: Text('Forgot Password?', style: TextStyle(fontSize: 12, fontStyle: FontStyle.italic)),
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 10,
                  child: ElevatedButton(
                      onPressed: () async {
                        try {
                          bool isLoggedIn = await LoginViewModel(_usernameController.text, _passwordController.text, _rememberPass).login();
                          if (isLoggedIn) {
                            Navigator.pushAndRemoveUntil(context,
                              MaterialPageRoute(builder: (context) => MyApp()),
                                  (Route<dynamic> route) => false,
                            );
                          } else {
                            showDialog(context: context, builder: (context) => ErrorAlert(errorMessage: 'Login Failed. Please check your username or password.'));
                          }
                        } on Exception catch (e) {
                          showDialog(context: context, builder: (context) => ErrorAlert(errorMessage: e.toString()));
                        }
                      },
                      child: Text('Login', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
                      style: ElevatedButton.styleFrom(
                          shape: StadiumBorder(),
                      )
                  ),
                ),
                Spacer(flex: 1,),
                Expanded(
                  flex: 10,
                  child: ElevatedButton(
                      onPressed: (){
                        Navigator.push(context,
                          MaterialPageRoute(builder: (context) => SimpleRegisterScreen())
                        );
                      },
                      child: Text('Signup', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
                      style: ElevatedButton.styleFrom(
                          primary: kSecondaryColor,
                          shape: StadiumBorder(),
                      )
                  ),
                ),
              ],
            ),
            Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Checkbox(
                    value: _rememberPass,
                    onChanged: (bool? newValue) {
                      setState(() {
                        _rememberPass = newValue??false;
                      });
                    },
                  ),
                  Text('Remember your password', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold))
                ]
            ),
            RichText(
              text: TextSpan(
                  style: TextStyle(color: Colors.black),
                  children: [
                    TextSpan(
                      text: 'By clicking signup, you are agree to our ',
                    ),
                    TextSpan(
                      style: TextStyle(decoration: TextDecoration.underline),
                      text: 'Terms',
                      recognizer: TapGestureRecognizer()..onTap = () {
                        showDialog(
                          context: context,
                          builder: (context) {
                            return PolicyDialog(
                              mdFileName: 'terms_conditions.md',
                            );
                          },
                        );
                      }
                    ),
                    TextSpan(
                      text: ' and ',
                    ),
                    TextSpan(
                      style: TextStyle(decoration: TextDecoration.underline),
                      text: 'Privacy',
                      recognizer: TapGestureRecognizer()..onTap = () {
                        showDialog(
                          context: context,
                          builder: (context) {
                            return PolicyDialog(
                              mdFileName: 'privacy_policy.md',
                            );
                          },
                        );
                      }
                    ),
                  ]),
            ),
            SizedBox(height: 32),
            Center(
              child: Text(
                _appVersion,
                style: TextStyle(
                  fontSize: 10,
                  fontWeight: FontWeight.bold
                ),
              ),
            )
          ],
        ),
      )
    );
  }
}



