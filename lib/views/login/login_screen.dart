import 'package:flutter/material.dart';
import 'package:sukai/models/sukai_api.dart';
import 'package:sukai/views/login/components/body.dart';

class LoginScreen extends StatelessWidget {

  initState(){
    SukaiApi().logout();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Body());
  }
}