import 'package:flutter/material.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:sukai/models/sukai_api.dart';
import 'package:sukai/views/login/reset_screen.dart';

import '../../constants.dart';

class VerifyScreen extends StatefulWidget {

  @override
  _VerifyScreenState createState() => _VerifyScreenState();
}

class _VerifyScreenState extends State<VerifyScreen> {
  final _emailController = TextEditingController();
  final _loadingController = RoundedLoadingButtonController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Email Verification'),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
        child: Column(
          children: [
            Text('Input your verification code.', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
            SizedBox(height: 32,),
            TextFormField(
              controller: _emailController,
            ),
            SizedBox(height: 32,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                RoundedLoadingButton(
                    color: kPrimaryColor,
                    controller: _loadingController,
                    onPressed: () async {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => ResetScreen(email: _emailController.text,))
                      );
                    },
                    child: Text('VERIFY', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                )
              ],
            ),
            SizedBox(height: 32,),
            Text('Note:', style: TextStyle(fontWeight: FontWeight.bold),),
            Text("Check your email's inbox for the verification code"),
          ],
        ),
      ),
    );
  }
}