import 'package:flutter/material.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:sukai/models/sukai_api.dart';
import 'package:sukai/views/components/alerts.dart';

import '../../constants.dart';
import '../../main.dart';

class ResetScreen extends StatefulWidget {
  final String email;

  ResetScreen( {Key? key, required this.email}) : super(key: key);

  @override
  _ResetScreenState createState() => _ResetScreenState();
}

class _ResetScreenState extends State<ResetScreen> {
  final _tokenController = TextEditingController();
  final _newPasswordController = TextEditingController();
  final _newPassword2Controller = TextEditingController();
  final _loadingController = RoundedLoadingButtonController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Update Password'),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
        child: Column(
          children: [
            Text('Update your password.', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
            SizedBox(height: 32,),
            TextFormField(
              controller: _tokenController,
              decoration: InputDecoration(
                labelText: 'Verification Code',
              ),
              validator: (String? value) {
                if (value == null || value.isEmpty) return 'Token cannot be empty';
              },
              autovalidateMode: AutovalidateMode.onUserInteraction,
            ),
            TextFormField(
              controller: _newPasswordController,
              obscureText: true,
              decoration: InputDecoration(
                labelText: 'New Password',
              ),
              validator: (String? value) {
                if (value == null || value.isEmpty) return 'Password cannot be empty';
              },
              autovalidateMode: AutovalidateMode.onUserInteraction,
            ),
            TextFormField(
              controller: _newPassword2Controller,
              obscureText: true,
              decoration: InputDecoration(
                labelText: 'Retype New Password',
              ),
              validator: (String? value) {
                if (value == null || value.isEmpty) return 'Retype your password';
                if (value != _newPasswordController.text) return "Password must match";
              },
              autovalidateMode: AutovalidateMode.onUserInteraction,
            ),
            SizedBox(height: 32,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                RoundedLoadingButton(
                    color: kPrimaryColor,
                    controller: _loadingController,
                    onPressed: () async {
                      final result = await SukaiApi().newpassword(widget.email, _tokenController.text, _newPasswordController.text, _newPassword2Controller.text);
                      if (result.success) {
                        _loadingController.success();
                        showDialog(
                            context: context,
                            builder: (BuildContext context) => SuccessAlert(
                              successMessage: result.message??'Password successfully changed.',
                              onDismiss: (){
                                Navigator.pushAndRemoveUntil(context,
                                  MaterialPageRoute(builder: (context) => MyApp()),
                                      (Route<dynamic> route) => false,
                                );
                              },
                            )
                        );
                      }else{
                        _loadingController.error();
                        showDialog(
                            context: context,
                            builder: (BuildContext context) => ErrorAlert(errorMessage: result.message??'Reset password failed.')
                        );
                      }
                      _loadingController.reset();
                    },
                    child: Text('UPDATE', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                )
              ],
            ),
            SizedBox(height: 32,),
            Text('Note:', style: TextStyle(fontWeight: FontWeight.bold),),
            Text("Check your email's inbox for the verification code"),
          ],
        ),
      ),
    );
  }
}