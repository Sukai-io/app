import 'package:flutter/material.dart';
import 'package:sukai/constants.dart';
import 'package:sukai/models/sukai_api.dart';
import 'package:sukai/views/smartads/create_screen.dart';
import 'package:sukai/views/smartads/topup_screen.dart';

class ClientAppbar extends StatefulWidget implements PreferredSizeWidget {
  final bool showDetail;

  const ClientAppbar({Key? key, this.showDetail = false}) : super(key: key);

  @override
  _ClientAppbarState createState() => _ClientAppbarState();

  @override
  Size get preferredSize => _getSize();

  Size _getSize() {
    if (showDetail) {
      return Size.fromHeight(150);
    }
    return Size.fromHeight(kToolbarHeight);
  }
}

class _ClientAppbarState extends State<ClientAppbar> with WidgetsBindingObserver {
  String _userName = 'Hi!';
  Dashboard _dashboard = Dashboard("0", "0", "0", "0");
  ImageProvider _avatar = Image.asset('assets/icons/ic_profile.png').image;

  @override
  void initState() {
    super.initState();

    final loginUser = SukaiApi().currentUser;
    if (loginUser != null) {
      _userName = 'Hi, ${loginUser.name}!';
      if (loginUser.avatarUrl.isNotEmpty && widget.showDetail) {
        _avatar = Image.network(loginUser.avatarUrl).image;
      }
    }

    if (widget.showDetail) {
      WidgetsBinding.instance!.addObserver(this);
      _getDashboard();
    }
  }

  void _getDashboard() async {
    final dashboard = await SukaiApi().dashboard();
    setState(() {
      _dashboard = dashboard;
    });
  }

  @override
  Widget build(BuildContext context) {

    final textStyle = TextStyle(
      color: kPrimaryColor,
      fontSize: 14
    );

    if (widget.showDetail) {
      return Column(
        children: [
          AppBar(
            title: Text(_userName),
          ),
          Material(
            elevation: 3,
            child: Container(
              child: Row(
                children: [
                  Container(
                    width: 75,
                    height: 75,
                    padding: EdgeInsets.all(5),
                    child: Container(
                      child: CircleAvatar(
                        backgroundColor: Colors.white,
                        foregroundImage: _avatar,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(5),
                      child: Column(
                        verticalDirection: VerticalDirection.down,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text('Balance: ', style: textStyle,),
                              Text(_dashboard.usdt, style: textStyle,),
                              Text(' USDT', style: textStyle,),
                            ],
                          ),
                          Row(
                            children: [
                              Text('Active Ads: ', style: textStyle,),
                              Text('0', style: textStyle,),
                            ],
                          ),
                          Row(
                            children: [
                              ElevatedButton(
                                onPressed: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(builder: (context) => TopUpScreen())
                                  );
                                },
                                child: Text('TOP UP'),
                              ),
                              SizedBox(width: 5,),
                              ElevatedButton(
                                onPressed: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(builder: (context) => CreateAdsScreen())
                                  );
                                },
                                child: Text('CREATE ADS'),
                              )
                            ],
                          ),
                        ],
                      ),
                    )
                  )
                ],
              ),
            ),
          )
        ],
      );
    } else {
      return AppBar(
        title: Text(_userName),
      );
    }
  }
}
