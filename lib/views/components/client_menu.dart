import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:sukai/constants.dart';
import 'package:sukai/models/sukai_api.dart';
import 'package:sukai/views/giveaway/giveaway_screen.dart';
import 'package:sukai/views/home/client_screen.dart';
import 'package:sukai/views/login/login_screen.dart';

class ClientMenuDrawer extends StatelessWidget {
  const ClientMenuDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
                decoration: BoxDecoration(
                  color: kPrimaryColor,
                ),
                child: Image.asset('assets/images/logo-login-atas.png')
            ),
            ListTile(
              leading: Icon(LineIcons.home),
              title: Text('Home'),
              onTap: () {
                Navigator.pushAndRemoveUntil(context,
                  MaterialPageRoute(builder: (context) => ClientScreen()),
                      (Route<dynamic> route) => false,
                );
              },
            ),
            ListTile(
              leading: Icon(LineIcons.alternateTicket),
              title: Text('Giveaway'),
              onTap: () {
                Navigator.pushAndRemoveUntil(context,
                  MaterialPageRoute(builder: (context) => GiveawayScreen()),
                      (Route<dynamic> route) => false,
                );
              },
            ),
            ListTile(
              leading: Icon(LineIcons.powerOff),
              title: Text('Logout'),
              onTap: () {
                SukaiApi().logout();
                Navigator.pushAndRemoveUntil(context,
                  MaterialPageRoute(builder: (context) => LoginScreen()),
                      (Route<dynamic> route) => false,
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
