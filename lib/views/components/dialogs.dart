import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:sukai/constants.dart';
import 'package:sukai/models/sukai_api.dart';
import 'package:sukai/views/nftmarketplace/nft_my_screen.dart';

class PolicyDialog extends StatelessWidget {
  PolicyDialog({
    Key? key,
    this.radius = 8,
    required this.mdFileName,
  })  : assert(mdFileName.contains('.md'), 'The file must contain the .md extension'),
        super(key: key);

  final double radius;
  final String mdFileName;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(radius)),
      child: Column(
        children: [
          Expanded(
            child: FutureBuilder<String>(
              future: Future.delayed(Duration(milliseconds: 150)).then((value) {
                return rootBundle.loadString('assets/text/$mdFileName');
              }),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Markdown(
                    data: snapshot.data!,
                  );
                }
                return Center(
                  child: CircularProgressIndicator(),
                );
              },
            ),
          ),
          TextButton(
            onPressed: () => Navigator.of(context).pop(),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(radius),
                  bottomRight: Radius.circular(radius),
                ),
              ),
              alignment: Alignment.center,
              height: 30,
              width: double.infinity,
              child: Text(
                "CLOSE",
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  color: kPrimaryColor,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class NoWalletAlert extends StatelessWidget {

  const NoWalletAlert({Key? key,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _walletController = TextEditingController();
    final walletInput = TextFormField(
        controller: _walletController,
        maxLines: 2,
        decoration: InputDecoration(
            hintText: 'Input Wallet address',
            labelText: 'Wallet',
            floatingLabelBehavior: FloatingLabelBehavior.always
        )
    );
    return AlertDialog(
      title: Text('No Wallet Info'),
      content: Container(
        height: 160,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Please input your wallet address to start NFT transaction."),
            walletInput,
            Text("*you can edit wallet address from profile page.",
              style: TextStyle(fontSize: 12),),
          ],
        ),
      ),
      actions: [
        TextButton(
            onPressed: () async {
              final profile = await SukaiApi().getProfile();
              if (profile != null) {
                profile.wallet = _walletController.text;
                final result = await SukaiApi().updateProfile(profile, null);
                if (result.success) {
                  Navigator.pushAndRemoveUntil(context,
                    MaterialPageRoute(builder: (context) => NFTMyScreen()),
                        (Route<dynamic> route) => false,
                  );
                }
              }
            },
            child: Text('SAVE'))
      ],
    );
  }
}