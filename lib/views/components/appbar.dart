import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sukai/constants.dart';
import 'package:sukai/models/sukai_api.dart';
import 'package:sukai/views/profile/profile_screen.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfileAppBar extends StatefulWidget implements PreferredSizeWidget {
  final bool showDetail;

  const ProfileAppBar({Key? key, this.showDetail = false}) : super(key: key);

  @override
  _ProfileAppBarState createState() => _ProfileAppBarState();

  @override
  Size get preferredSize => _getSize();

  Size _getSize() {
    if (showDetail) {
      return Size.fromHeight(158);
    }
    return Size.fromHeight(kToolbarHeight);
  }

}

class _ProfileAppBarState extends State<ProfileAppBar> with WidgetsBindingObserver {
  String _userName = 'Hi!';
  Dashboard _dashboard = Dashboard("0", "0", "0", "0");
  ImageProvider _avatar = Image.asset('assets/icons/ic_profile.png').image;

  @override
  void initState() {
    super.initState();

    final loginUser = SukaiApi().currentUser;
    if (loginUser != null) {
      _userName = 'Hi, ${loginUser.name}!';
      if (loginUser.avatarUrl.isNotEmpty && widget.showDetail) {
        _avatar = Image.network(loginUser.avatarUrl).image;
      }
    }

    if (widget.showDetail) {
      WidgetsBinding.instance!.addObserver(this);
      _getDashboard();
    }
  }

  void _getDashboard() async {
    final dashboard = await SukaiApi().dashboard();
    setState(() {
      _dashboard = dashboard;
    });
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      _getDashboard();
    }
  }

  @override
  Widget build(BuildContext context) {
    if (widget.showDetail) {
      return Stack(
        children: [
          Column(
            children: [
              Container(
                child: AppBar(
                    title: Text(_userName)
                ),
              ),
              WalletWidget(_dashboard.point, _dashboard.usdt, _dashboard.token)
            ],
          ),
          Positioned(
              bottom: 0,
              right: 0,
              child: Container(
                height: 100 + kToolbarHeight,
                width: 120,
                padding: EdgeInsets.fromLTRB(4, 0, 16, 4),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  verticalDirection: VerticalDirection.up,
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 13),
                      child: AffiliateBox(value: _dashboard.affiliate, unit: 'AFFILIATE')
                    ),
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) => ProfileScreen())
                          );
                        },
                        child: Container(
                          margin: EdgeInsets.fromLTRB(0, 4, 0, 0),
                          child: CircleAvatar(
                            backgroundColor: Colors.white,
                            backgroundImage: _avatar,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
          ),
        ],
      );
    } else {
      return AppBar(
        title: Text(_userName),
      );
    }
  }
}

class WalletWidget extends StatelessWidget  {
  final String point;
  final String usdt;
  final String token;
  const WalletWidget(this.point, this.usdt, this.token, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 3,
      child: Container(
        height: 100,
        padding: EdgeInsets.fromLTRB(16, 8, 120, 0),
        color: kPrimaryLightColor,
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  'Your Wallet',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                    color: kPrimaryColor,
                  ),
                ),
            //     Spacer(),
            //     GestureDetector(
            //       onTap: (){
            //         showDialog(
            //           context: context,
            //           builder: (BuildContext context) => UnderConstruction(),
            //         );
            //       },
            //       child: Text('Topup'),
            //     ),
            //     Text(' | '),
            //     GestureDetector(
            //         onTap: (){
            //           showDialog(
            //             context: context,
            //             builder: (BuildContext context) => UnderConstruction(),
            //           );
            //         },
            //         child: Text('Withdraw'))
              ],
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  MetricBox(value: usdt, unit: 'BUSD', url: 'https://forms.gle/fPSVY2oZdrWVPfKt8',),
                  SizedBox(width: 8,),
                  MetricBox(value: token, unit: 'SKYX', url: 'https://forms.gle/nESCLxCyYDyi6x4KA'),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}


class MetricBox extends StatelessWidget {
  final String value;
  final String unit;
  final String? url;
  const MetricBox({Key? key, required this.value, required this.unit, this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ElevatedButton(
        onPressed: () async {
          if (url != null) {
            await canLaunch(url!) ? await launch(url!) : throw 'Could not launch $url';
          }
        },
        child: Column(
          children: [
            Text(
              value,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 12,
                color: kPrimaryColor,
              ),
            ),
            Text(
              unit,
              style: TextStyle(
                fontSize: 10,
                color: kPrimaryColor,
              ),
            )
          ],
        ),
        style: ElevatedButton.styleFrom(
            primary: Colors.white,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            padding: EdgeInsets.symmetric(horizontal: 4),
        )
      )
    );
  }
}

class AffiliateBox extends StatelessWidget {
  final String value;
  final String unit;
  const AffiliateBox({Key? key, required this.value, required this.unit,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 0, vertical: 8),
        child: Column(
          children: [
            Text(
              value,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 12,
              ),
            ),
            Text(
              unit,
              style: TextStyle(
                fontSize: 10,
              ),
            )
          ],
        )
    );
  }
}

class UnderConstruction extends StatelessWidget {
  const UnderConstruction({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Under construction'),
      content: Text('Feature still under development'),
      actions: [
        TextButton(
            onPressed: () => Navigator.pop(context, 'OK'),
            child: Text('OK'))
      ],
    );
  }
}