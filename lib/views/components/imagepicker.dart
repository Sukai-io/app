import 'package:flutter/material.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:sukai/constants.dart';

typedef PickerCallback = void Function(File file);

class ImageFormField extends StatefulWidget {
  ImageFormField({Key? key, required this.label, required this.onImagePicked}) : super(key: key);

  final String label;
  final PickerCallback onImagePicked;

  @override
  _ImageFormFieldState createState() => _ImageFormFieldState();
}

class _ImageFormFieldState extends State<ImageFormField> {
  PickedFile? _file;
  final ImagePicker _picker = ImagePicker();
  dynamic _pickImageError;

  @override
  Widget build(BuildContext context) {
    Column imageColumn = Column(
      children: [
        TextFormField(
          initialValue: _file?.path,
          readOnly: true,
          decoration: InputDecoration(
              hintText: widget.label,
              suffixIcon: TextButton(
                onPressed: chooseImage,
                child: Text('BROWSE'),
              )
          ),
          validator: (value) {
            if (_pickImageError != null) {
              return _pickImageError;
            }
            return null;
          },
        ),
      ],
    );

    if (_file != null) {
        imageColumn.children.add(Container(
          width: 240,
          height: 150,
          margin: EdgeInsets.symmetric(vertical: 8, horizontal: 2),
          padding: EdgeInsets.all(2),
          child: DecoratedBox(
            decoration: BoxDecoration(
                image: DecorationImage(
                  image: FileImage(File(_file!.path)),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(5),
                border: Border.all(
                    color: kPrimaryColor,
                    width: 1
                )
            ),
          ),
        ));
    }

    return Container(
      child: imageColumn
    );
  }

  chooseImage() async {
    try {
      final pickedFile = await _picker.getImage(source: ImageSource.gallery);
      setState(() {
        _file = pickedFile;
        if (_file != null) {
          widget.onImagePicked(File(_file!.path));
        }
      });
    } catch(e) {
      setState(() {
        _pickImageError = e;
      });
    }
  }

}

