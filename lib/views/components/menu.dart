import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:sukai/constants.dart';
import 'package:sukai/models/sukai_api.dart';
import 'package:sukai/views/chat/chat_screen.dart';
import 'package:sukai/views/giveaway/giveaway_screen.dart';
import 'package:sukai/views/home/home_screen.dart';
import 'package:sukai/views/login/login_screen.dart';
import 'package:sukai/views/nftmarketplace/nft_my_screen.dart';
import 'package:sukai/views/profile/profile_screen.dart';
import 'package:sukai/views/referral/report_screen.dart';

class MenuDrawer extends StatelessWidget {
  const MenuDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
                decoration: BoxDecoration(
                  color: kPrimaryColor,
                ),
                child: Image.asset('assets/images/logo-login-atas.png')
            ),
            ListTile(
              leading: Icon(LineIcons.home),
              title: Text('Home'),
              onTap: () {
                Navigator.pushAndRemoveUntil(context,
                  MaterialPageRoute(builder: (context) => HomeScreen()),
                      (Route<dynamic> route) => false,
                );
              },
            ),
            ListTile(
              leading: Icon(LineIcons.alternateTicket),
              title: Text('Giveaway'),
              onTap: () {
                Navigator.pushAndRemoveUntil(context,
                  MaterialPageRoute(builder: (context) => GiveawayScreen()),
                      (Route<dynamic> route) => false,
                );
              },
            ),
            ListTile(
              leading: Icon(LineIcons.shoppingCart),
              title: Text('My NFT'),
              onTap: () {
                Navigator.pushAndRemoveUntil(context,
                  MaterialPageRoute(builder: (context) => NFTMyScreen()),
                      (Route<dynamic> route) => false,
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.chat),
              title: Text('Chat Forum'),
              onTap: () {
                Navigator.pushAndRemoveUntil(context,
                  MaterialPageRoute(builder: (context) => ChatScreen()),
                      (Route<dynamic> route) => false,
                );
              },
            ),
            ListTile(
              leading: Icon(LineIcons.link),
              title: Text('Affiliate'),
              onTap: () {
                Navigator.pushAndRemoveUntil(context,
                  MaterialPageRoute(builder: (context) => ReportScreen()),
                      (Route<dynamic> route) => false,
                );
              },
            ),
            ListTile(
              leading: Icon(LineIcons.addressBook),
              title: Text('Profile'),
              onTap: () {
                Navigator.pushAndRemoveUntil(context,
                  MaterialPageRoute(builder: (context) => ProfileScreen()),
                      (Route<dynamic> route) => false,
                );
              },
            ),
            ListTile(
              leading: Icon(LineIcons.powerOff),
              title: Text('Logout'),
              onTap: () {
                SukaiApi().logout();
                Navigator.pushAndRemoveUntil(context,
                  MaterialPageRoute(builder: (context) => LoginScreen()),
                      (Route<dynamic> route) => false,
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
