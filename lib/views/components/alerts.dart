import 'package:flutter/material.dart';

class SuccessAlert extends StatelessWidget {
  final String successMessage;
  final Function onDismiss;

  const SuccessAlert({Key? key, required this.successMessage, required this.onDismiss}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Success'),
      content: Text(successMessage),
      actions: [
        TextButton(
            onPressed: () {
              Navigator.pop(context, 'OK');
              onDismiss();
            },
            child: Text('OK'))
      ],
    );
  }
}


class ErrorAlert extends StatelessWidget {
  final String errorMessage;

  const ErrorAlert({Key? key, required this.errorMessage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Error'),
      content: Text(errorMessage),
      actions: [
        TextButton(
            onPressed: () => Navigator.pop(context, 'OK'),
            child: Text('OK'))
      ],
    );
  }
}

class UnderConstruction extends StatelessWidget {
  const UnderConstruction({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Under construction'),
      content: Text('Feature still under development'),
      actions: [
        TextButton(
            onPressed: () => Navigator.pop(context, 'OK'),
            child: Text('OK'))
      ],
    );
  }
}