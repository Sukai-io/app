import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:sukai/constants.dart';
import 'package:sukai/main.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OnBoarding extends StatefulWidget {
  const OnBoarding({Key? key}) : super(key: key);

  @override
  _OnBoardingState createState() => _OnBoardingState();
}

class _OnBoardingState extends State<OnBoarding> {
  @override
  Widget build(BuildContext context) {
    return IntroductionScreen(
      globalBackgroundColor: kPrimaryLightColor,
      pages: [
        PageViewModel(
          title: '', bodyWidget: Container(child: Image.asset('assets/images/slide01.jpg', fit: BoxFit.fill, alignment: Alignment.center,))
        ),
        PageViewModel(
          title: '', bodyWidget: Container(child: Image.asset('assets/images/slide02.jpg', fit: BoxFit.fill, alignment: Alignment.center,))
        ),
        PageViewModel(
          title: '', bodyWidget: Container(child: Image.asset('assets/images/slide03.jpg', fit: BoxFit.fill, alignment: Alignment.center,))
        )
      ],
      done: const Text("LAUNCH", style: TextStyle(fontWeight: FontWeight.w600)),
      showNextButton: false,
      showSkipButton: false,
      onDone: () async {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        await prefs.setBool('seen_onboarding', true);
        Navigator.of(context).push(
          MaterialPageRoute(builder: (_) => LandingScreen())
        );
      },
    );
  }
}
