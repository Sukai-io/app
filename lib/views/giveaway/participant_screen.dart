import 'package:flutter/material.dart';
import 'components/participants.dart';

class ParticipantScreen extends StatefulWidget {
  const ParticipantScreen({Key? key}) : super(key: key);

  @override
  _ParticipantScreenState createState() => _ParticipantScreenState();
}

class _ParticipantScreenState extends State<ParticipantScreen> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(title: Text('List of Participants'),),
      body: Container(
          width: double.infinity,
          child: Column(
            children: [
              Image.asset('assets/images/participant-giveaway.png', width: size.width),
              Expanded(
                child: Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.all(16),
                  child: Participants()
                ),
              )
            ],
          )
      ),
    );
  }
}
