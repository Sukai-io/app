import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sukai/constants.dart';
import 'package:sukai/models/giveaway.dart';
import 'package:sukai/models/sukai_api.dart';

class Participants extends StatefulWidget {
  const Participants({Key? key}) : super(key: key);

  @override
  _ParticipantsState createState() => _ParticipantsState();
}

class _ParticipantsState extends State<Participants> {
  Future<List<Participant>> _getParticipants(){
    return SukaiApi().giveawayParticipants();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Participant>>(
        future: _getParticipants(),
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.connectionState == ConnectionState.done) {
            if ( snapshot.data!.length <= 0 ) {
              return Center(child: Text('No participants data'),);
            }
            return ListView.builder(
                itemCount: snapshot.data?.length,
                itemBuilder: (context, index) {
                Participant participant = snapshot.data![index];
                String formattedDate = DateFormat('MMMM dd yyy').format(participant.entryDate);
                return Card(
                  color: index%2==1?kRowColor:kPrimaryColor,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Align(
                              child: Text(
                                formattedDate,
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.white
                                ),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                  participant.username,
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white
                                ),
                              ),
                              Text(
                                  participant.ticket.toUpperCase(),
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                  color: Colors.yellowAccent
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                );
            });
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        }
    );
  }
}
