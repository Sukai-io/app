import 'package:sukai/models/sukai_api.dart';
import 'dart:io';

class RegisterViewModel {
  final String quantity;
  final File file;

  RegisterViewModel(this.quantity, this.file);

  static Future<double> getTicketPrice() async {
    final optionMap = await SukaiApi().getOptions();
    return double.tryParse(optionMap['ticket_usdt']??'10')??10.0;
  }

  Future<bool> buyTicket() async {
    return await SukaiApi().buyTicket(quantity, file.path);
  }
}