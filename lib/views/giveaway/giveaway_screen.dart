import 'package:flutter/material.dart';
import 'package:sukai/views/components/appbar.dart';
import 'package:sukai/views/components/menu.dart';
import 'package:sukai/views/giveaway/participant_screen.dart';
import 'package:sukai/views/giveaway/register_screen.dart';

class GiveawayScreen extends StatelessWidget {
  const GiveawayScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: ProfileAppBar(),
        drawer: MenuDrawer(),
        body: Container(
          child: ListView(
            children: [
              GestureDetector(
                onTap: (){
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => RegisterScreen())
                  );
                },
                child: Image.asset('assets/images/ticket-giveaway.png'),
              ),
              GestureDetector(
                onTap: (){
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ParticipantScreen())
                  );
                },
                child: Image.asset('assets/images/participant-giveaway.png'),
              ),
              GestureDetector(
                onTap: (){
                  showDialog(
                    context: context,
                    builder: (BuildContext context) => UnderConstruction(),
                  );
                },
                child: Image.asset('assets/images/winner-giveaway.png'),
              ),
            ],
          ),
        )
    );
  }
}

class UnderConstruction extends StatelessWidget {
  const UnderConstruction({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Under construction'),
      content: Text('Feature still under development'),
      actions: [
        TextButton(
            onPressed: () => Navigator.pop(context, 'OK'),
            child: Text('OK'))
      ],
    );
  }
}