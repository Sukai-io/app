import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:flutter/services.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:sukai/constants.dart';
import 'package:sukai/models/sukai_api.dart';
import 'package:sukai/views/components/alerts.dart';
import 'package:sukai/views/components/imagepicker.dart';
import 'package:sukai/views/giveaway/register_view_model.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  double _price = 0;
  TextEditingController _howManyController = TextEditingController(text: '1');
  TextEditingController _totalPayController = TextEditingController(text: '0');
  TextEditingController _busdAddrController = TextEditingController(text: '0xbfec1a4a07d0078fc249ed8eabc87439d36c5b97');
  RoundedLoadingButtonController _buttonController = RoundedLoadingButtonController();
  final _textStyle = TextStyle(fontSize: 16);
  final username = SukaiApi().currentUser?.username;
  File? _paymentFile;

  @override
  void initState() {
    super.initState();
    _getPrice();
  }

  _getPrice() async {
    final price = await RegisterViewModel.getTicketPrice();
    setState(() {
      _price = price;
    });
    _updateTotalPay(_howManyController.text);
  }

  _updateTotalPay(String quantity) {
    int qty = int.tryParse(quantity) ?? 1;
    if (qty <= 0) qty = 1;
    double total = _price * qty;
    setState(() {
      _totalPayController.text = total.toString();
    });
  }

  @override
  void dispose() {
    _howManyController.dispose();
    _totalPayController.dispose();
    _busdAddrController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(title: Text('Get Ticket Now'),),
      body: Container(
        width: double.infinity,
        child: ListView(
          children: [
            Image.asset('assets/images/get-ticket-now.png', width: size.width),
            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.all(18),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children : [
                    Text(
                      'Username: ${username??'-'}',
                      style: _textStyle,
                    ),
                    Text(
                      'Price per ticket: $_price BUSD',
                      style: _textStyle,
                    ),
                    TextFormField(
                      controller: _howManyController,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        hintText: 'How many tickets?',
                        labelText: 'How many ticket'
                      ),
                      onChanged: (newText) {
                        _updateTotalPay(newText);
                      },
                    ),
                    TextFormField(
                      controller: _totalPayController,
                      readOnly: true,
                      decoration: InputDecoration(
                        hintText: '[Total payment in BUSD]',
                        labelText: 'Total payment in BUSD'
                      ),
                    ),
                    SizedBox(height: 16,),
                    Text(
                      'Transfer BUSD Payment via:',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                    TextField(
                      controller: _busdAddrController,
                      readOnly: true,
                      decoration: InputDecoration(
                        labelText: 'SUKAI BUSD Address:',
                        suffixIcon: IconButton(
                          onPressed: (){
                            Clipboard.setData(ClipboardData(text: _busdAddrController.text));
                            final snackBar = SnackBar(content: Text('Address copied!'));
                            ScaffoldMessenger.of(context).showSnackBar(snackBar);
                          },
                          icon: Icon(LineIcons.copy),
                        )
                      ),
                    ),
                    SizedBox(height: 16,),
                    Center(
                      child: Image.asset('assets/images/sukai-busd.jpg', width: size.width * 0.5,),
                    ),
                    SizedBox(height: 16,),
                    ImageFormField(
                        label: 'Send Payment Confirmation',
                        onImagePicked: (file){
                          setState(() {
                            _paymentFile = file;
                          });
                        },
                    ),
                    SizedBox(height: 20,),
                    Align(
                      alignment: Alignment.center,
                      child: RoundedLoadingButton(
                          controller: _buttonController,
                          color: kPrimaryColor,
                          onPressed: () async {
                            if (_paymentFile == null) {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) => ErrorAlert(errorMessage: 'Payment confirmation is required.')
                              );
                              _buttonController.reset();
                              return;
                            }
                            final isSuccess = await RegisterViewModel(_howManyController.text, _paymentFile!).buyTicket();
                            if (isSuccess) {
                              _buttonController.success();
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) => AlertDialog(
                                    title: Text('Success'),
                                    content: Text('Your giveaway transaction has been submitted, please wait for the approval.'),
                                    actions: [
                                      TextButton(
                                          onPressed: () => Navigator.pop(context, 'OK'),
                                          child: Text('OK'))
                                    ],
                                  )
                              );
                            }else{
                              _buttonController.reset();
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) => ErrorAlert(errorMessage: 'Failed to buy ticket')
                              );
                            }
                          },
                          child: Text('CONFIRM', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                      ),
                    ),
                  ]
              ),
            )
          ],
        )
      ),
    );
  }
}
