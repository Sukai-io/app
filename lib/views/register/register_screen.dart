import 'package:flutter/material.dart';
import 'package:sukai/views/register/components/body.dart';

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Signup / Registration'),
      ),
      body: Body(),
    );
  }
}
