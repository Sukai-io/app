
import 'package:sukai/models/sukai_api.dart';
import 'package:sukai/models/user.dart';

class MemberFormViewModel {

  late final RegisterMember form;

  MemberFormViewModel(
      String sponsor,
      String username,
      String name,
      String email,
      String password,
      String idnumber,
      String idfilepath,
      String dateofbirth,
      String gender,
      String country,
      String phone,
      String city,
      String address,
      String smoker,
      String married,
      String hobby,
      String profession
      ){
    form = RegisterMember(
        sponsor,
        username,
        name,
        email, password, idnumber, idfilepath, dateofbirth, gender, country, phone, city, address, smoker, married, hobby, profession);
  }

  MemberFormViewModel.simpleMemberFormViewModel(
      String sponsor,
      String username,
      String name,
      String email,
      String password
      ) {
    form = RegisterMember(sponsor, username, name, email, password, "", "", "", "", "", "", "", "", "", "", "", "");
  }

  Future<RegisterResult> register() async {
    final result = await SukaiApi().registerMember(form);
    return RegisterResult(result.success, result.message);
  }

  static Future<bool> checkReferral(String username) async {
    final result = await SukaiApi().searchsponsordata(username);
    return result.success;
  }

  static Future<bool> checkUsername(String username) async {
    final result = await SukaiApi().checkusername(username);
    return result.success;
  }

  Future<RegisterResult> simpleRegister() async {
    final result = await SukaiApi().simpleRegister(form);
    return RegisterResult(result.success, result.message);
  }
}

class RegisterResult {
  final bool success;
  final String? error;
  RegisterResult(this.success, this.error);
}