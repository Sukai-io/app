import 'package:android_play_install_referrer/android_play_install_referrer.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:sukai/intl_phone_field/intl_phone_field.dart';
import 'package:sukai/models/sukai_api.dart';
import 'package:sukai/views/components/alerts.dart';
import 'package:sukai/views/components/imagepicker.dart';
import 'package:sukai/views/login/login_screen.dart';
import 'package:sukai/views/register/components/dropdowns.dart';
import 'dart:io';
import '../../../constants.dart';
import 'client_form_view_model.dart';

class ClientForm extends StatefulWidget {
  const ClientForm({Key? key}) : super(key: key);

  @override
  _ClientFormState createState() => _ClientFormState();
}

class _ClientFormState extends State<ClientForm> {
  final TextEditingController _referralController = TextEditingController();
  bool? _referralValid;
  final TextEditingController _usernameController = TextEditingController();
  bool? _usernameValid;
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  bool? _emailExist;
  final TextEditingController _passwordController = TextEditingController();
  String _country = 'US';
  final TextEditingController _phoneController = TextEditingController();
  String? _phone;
  bool? _phoneExist;
  final TextEditingController _idNumberController = TextEditingController();
  bool? _idNumberExist;
  File? _idFile;
  String? _gender;
  final TextEditingController _cityController = TextEditingController();
  final TextEditingController _companyNameController = TextEditingController();
  final TextEditingController _companyAddressController = TextEditingController();
  final TextEditingController _brandNameController = TextEditingController();
  String? _productType;

  RoundedLoadingButtonController _buttonController = RoundedLoadingButtonController();

  final _formKey = GlobalKey<FormState>();
  final _referralKey = GlobalKey<FormFieldState>();
  final _usernameKey = GlobalKey<FormFieldState>();

  @override
  void initState() {
    super.initState();
    _getReferrer();
  }

  void _getReferrer() async {
    String referral = '';
    try {
      ReferrerDetails? referrerDetails = await AndroidPlayInstallReferrer.installReferrer;
      if (referrerDetails != null && referrerDetails.installReferrer != null) {
        // utm_source=(not%20set)&utm_medium=(not%20set)
        List<String> pairs = referrerDetails.installReferrer!.split('&');
        List<String> srcPairs = pairs[0].split('=');
        String source = srcPairs[1];
        if ( source.isNotEmpty && source != '(not%20set)' && source != 'google-play') {
          referral = source;
        }
      }
    } catch (e) {
      print(e);
    }

    if (!mounted) return;

    setState(() {
      _referralController.text = referral;
    });
  }

  @override
  Widget build(BuildContext context) {
    Icon referralIcon = Icon(LineIcons.search);
    if (_referralValid != null && _referralValid == true) {
      referralIcon = Icon(LineIcons.check, color: Colors.greenAccent,);
    }

    Icon usernameIcon = Icon(LineIcons.search);
    if (_usernameValid != null && _usernameValid == true) {
      usernameIcon = Icon(LineIcons.check, color: Colors.greenAccent,);
    }

    return Container(
      padding: EdgeInsets.all(16),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                'Client Information Member: ',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
            ),
            TextFormField(
              key: _referralKey,
              controller: _referralController,
              decoration: InputDecoration(
                hintText: "Referral's username",
                labelText: 'Username Referral',
                suffixIcon: IconButton(
                  icon: referralIcon,
                  onPressed: () async {
                    final isValid = await ClientFormViewModel.checkReferral(_referralController.text);
                    setState(() {
                      _referralValid = isValid;
                    });
                    _referralKey.currentState?.validate();
                  },
                )
              ),
              onChanged: (value) {
                setState(() {
                  _referralValid = null;
                });
              },
              validator: (String? value) {
                if (value == null || value.isEmpty) return 'Referral cannot be empty';
                if (_referralValid != null && _referralValid == false) return 'Invalid referral username';
                return null;
              },
            ),
            TextFormField(
              key: _usernameKey,
              controller: _usernameController,
              decoration: InputDecoration(
                hintText: 'Your username',
                labelText: 'Username',
                suffixIcon: IconButton(
                  icon: usernameIcon,
                  onPressed: () async {
                    final isValid = await ClientFormViewModel.checkUsername(_usernameController.text);
                    setState(() {
                      _usernameValid = isValid;
                    });
                    _usernameKey.currentState?.validate();
                  },
                )
              ),
              onChanged: (value) {
                setState(() {
                  _usernameValid = null;
                });
              },
              validator: (String? value) {
                if (value == null || value.isEmpty) return 'Username cannot be empty';
                if (_usernameValid != null && _usernameValid == false) return 'Invalid username';
                return null;
              },
            ),
            TextFormField(
              controller: _nameController,
              decoration: InputDecoration(
                hintText: 'Input your name',
                labelText: 'Name'
              ),
              validator: (String? value) {
                if (value == null || value.isEmpty) return 'Name cannot be empty';
              },
            ),
            TextFormField(
              controller: _emailController,
              decoration: InputDecoration(
                hintText: 'Input your email',
                labelText: 'Email'
              ),
              validator: (String? value) {
                if (value == null || value.isEmpty) return 'Email cannot be empty';
                bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value);
                if (!emailValid) return 'Email format not valid';
                if (_emailExist != null && _emailExist!) return 'Email already exist';
              },
            ),
            TextFormField(
              controller: _passwordController,
              obscureText: true,
              decoration: InputDecoration(
                hintText: 'Input your password',
                labelText: 'Password',
              ),
              validator: (String? value) {
                if (value == null || value.isEmpty) return 'Password cannot be empty';
              },
            ),
            TextFormField(
              obscureText: true,
              decoration: InputDecoration(
                hintText: 'Retype your password',
                labelText: 'Retype Password',
              ),
              validator: (String? value) {
                if (value == null || value.isEmpty) return 'Retype your password';
                if (value != _passwordController.text) return "Password must match";
              },
              autovalidateMode: AutovalidateMode.onUserInteraction,
            ),
            IntlPhoneField(
              controller: _phoneController,
              decoration: InputDecoration(
                labelText: 'Phone Number',
                hintText: 'Input your phone'
              ),
              initialCountryCode: _country,
              onChanged: (phone) {
                _phone = phone.completeNumber;
              },
              onCountryChanged: (phone){
                _country = phone.countryISOCode??'US';
                if (phone.countryCode != null) {
                  _phone = phone.countryCode! + _phoneController.text;
                }
              },
              autoValidate: false,
              validator: (String? value){
                if (value == null || value.isEmpty) return 'Phone cannot be empty';
                if (_phoneExist != null && _phoneExist!) return 'Phone already used';
              },
            ),
            TextFormField(
              controller: _idNumberController,
              decoration: InputDecoration(
                hintText: 'Input your National ID or Passport',
                labelText: 'Identity Number'
              ),
              validator: (String? value) {
                if (value == null || value.isEmpty) return 'Identity number cannot be empty';
                if (_idNumberExist != null && _idNumberExist!) return 'Identity number already used';
              },
            ),
            ImageFormField(
              label: 'Upload Identity',
              onImagePicked: (file) {
                setState(() {
                  _idFile = file;
                });
              },
            ),
            DropdownButtonFormField(
              value: _gender,
              isExpanded: true,
              decoration: InputDecoration(
                labelText: 'Gender',
              ),
              items: <DropdownMenuItem<String>>[
                DropdownMenuItem(
                    value: 'male',
                    child: Text('Male')
                ),
                DropdownMenuItem(
                    value: 'female',
                    child: Text('Female')
                ),
              ].toList(),
              onChanged: (String? value){
                setState(() {
                  _gender = value;
                });
              },
              validator: (String? value) {
                if (value == null || value.isEmpty) return 'Please select gender';
                return null;
              },
            ),
            TextFormField(
              controller: _cityController,
              decoration: InputDecoration(
                hintText: 'Input your city',
                labelText: 'City'
              ),
              validator: (String? value){
                if (value == null || value.isEmpty) return 'City cannot be empty';
                return null;
              },
            ),
            TextFormField(
              controller: _companyNameController,
              decoration: InputDecoration(
                hintText: 'Input your company name',
                labelText: 'Company Name'
              ),
              validator: (String? value){
                if (value == null || value.isEmpty) return 'Company name cannot be empty';
                return null;
              },
            ),
            TextFormField(
              controller: _companyAddressController,
              decoration: InputDecoration(
                hintText: 'Input company address',
                labelText: 'Company Address',
              ),
              validator: (String? value){
                if (value == null || value.isEmpty) return 'Company address cannot be empty';
                return null;
              },
            ),
            TextFormField(
              controller: _brandNameController,
              decoration: InputDecoration(
                hintText: 'Input company brand',
                labelText: 'Brand Name',
              ),
              validator: (String? value){
                if (value == null || value.isEmpty) return 'Company brand cannot be empty';
                return null;
              },
            ),
            DropdownButtonFormField(
              value: _productType,
              isExpanded: true,
              decoration: InputDecoration(
                  labelText: 'Product Type'
              ),
              items: kProductItems,
              onChanged: (String? value){
                setState(() {
                  _productType = value;
                });
              },
              validator: (String? value) {
                if (value == null || value.isEmpty) return 'Please select your product type';
                return null;
              },
            ),
            SizedBox(height: 20,),
            RoundedLoadingButton(
              controller: _buttonController,
              color: kPrimaryColor,
              onPressed: () async {
                try {
                  if (_emailController.text.isNotEmpty){
                    _emailExist = await SukaiApi().checkEmailExist(_emailController.text);
                  }

                  if (_idNumberController.text.isNotEmpty){
                    _idNumberExist = await SukaiApi().checkIdNumberExist(_idNumberController.text);
                  }

                  if (_phone != null && _phone!.isNotEmpty){
                    _phoneExist = await SukaiApi().checkPhoneExist(_phone!);
                  }

                  if (_formKey.currentState!.validate()) {
                    if (_idFile == null) {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) => ErrorAlert(errorMessage: 'Please attach your id card photo.')
                      );
                      _buttonController.reset();
                      return;
                    }
                    // check referral again
                    _referralValid = await ClientFormViewModel.checkReferral(_referralController.text);
                    if (!_referralValid!) {
                      _referralKey.currentState?.validate();
                      showDialog(
                          context: context,
                          builder: (BuildContext context) => ErrorAlert(errorMessage: 'Invalid referral. Please check your referral username.')
                      );
                      _buttonController.reset();
                      return;
                    }

                    // check username again
                    _usernameValid = await ClientFormViewModel.checkUsername(_usernameController.text);
                    if (!_usernameValid!) {
                      _usernameKey.currentState?.validate();
                      showDialog(
                          context: context,
                          builder: (BuildContext context) => ErrorAlert(errorMessage: 'Invalid username. Please change your username.')
                      );
                      _buttonController.reset();
                      return;
                    }

                    final result = await ClientFormViewModel(
                        _referralController.text,
                        _usernameController.text,
                        _nameController.text,
                        _emailController.text,
                        _passwordController.text,
                        _idNumberController.text,
                        _idFile!.path,
                        _gender!,
                        _country,
                        _phone!,
                        _cityController.text,
                        _companyNameController.text,
                        _companyAddressController.text,
                        _brandNameController.text,
                        _productType!).register();

                    if (result.success) {
                      showDialog(context: context, builder: (BuildContext context) => AlertDialog(
                        title: Text('Success'),
                        content: Text('Sign up success!'),
                        actions: [
                          TextButton(
                              onPressed: () {
                                Navigator.push(context,
                                    MaterialPageRoute(builder: (context) => LoginScreen())
                                );
                              },
                              child: Text('OK'))
                        ],
                      ));

                    } else {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) => ErrorAlert(errorMessage: result.error??'Sign up failed.')
                      );
                    }
                  } else {

                  }
                } catch (e) {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) => ErrorAlert(errorMessage: e.toString())
                  );
                } finally {
                  _buttonController.reset();
                }
              },
              child: Text('SIGN UP', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
            ),
          ],
        ),
      ),
    );
  }
}
