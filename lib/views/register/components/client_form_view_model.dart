
import 'package:sukai/models/sukai_api.dart';
import 'package:sukai/models/user.dart';

class ClientFormViewModel {

  late final RegisterClient form;

  ClientFormViewModel(
      String sponsor,
      String username,
      String name,
      String email,
      String password,
      String idnumber,
      String idfilepath,
      String gender,
      String country,
      String phone,
      String city,
      String companyName,
      String companyAddress,
      String brandName,
      String productType,
      ){
    form = RegisterClient(
        sponsor,
        username,
        name,
        email,
        password,
        idnumber,
        idfilepath,
        gender,
        country,
        phone,
        city,
        companyName,
        companyAddress,
        brandName,
        productType);
  }

  Future<RegisterResult> register() async {
    final result = await SukaiApi().registerclient(form);
    return RegisterResult(result.success, result.message);
  }

  static Future<bool> checkReferral(String username) async {
    final result = await SukaiApi().searchsponsordata(username);
    return result.success;
  }

  static Future<bool> checkUsername(String username) async {
    final result = await SukaiApi().checkusername(username);
    return result.success;
  }
}

class RegisterResult {
  final bool success;
  final String? error;
  RegisterResult(this.success, this.error);
}