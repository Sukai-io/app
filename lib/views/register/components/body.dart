import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:sukai/constants.dart';
import 'package:sukai/views/register/components/member_form.dart';
import 'package:sukai/views/register/components/client_form.dart';

enum UserType { member, client }

class Body extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  UserType? _userType = UserType.member;

  Widget build(BuildContext context) {
    Widget _form = MemberForm();
    if (_userType == UserType.client) {
        _form = ClientForm();
    }
    return ListView(
      children: [
        Container(
          child: Padding(
            padding: EdgeInsets.all(16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                      'User Type Information',
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)
                  ),
                ),
                SizedBox(height: 8,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      height: 30,
                      width: 30,
                      child: Radio(
                        value: UserType.member,
                        groupValue: _userType,
                        onChanged: (UserType? value) {
                          setState(() {
                            _userType = value;
                          });
                        },
                      ),
                    ),
                    GestureDetector(
                      onTap: (){
                        setState(() {
                          _userType = UserType.member;
                        });
                      },
                      child: Text(
                          'Register as Member',
                          style: TextStyle(fontSize: 16)
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: GestureDetector(
                        onTap: () {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) => AlertDialog(
                                title: Text('Register as Member'),
                                content: Text('Choose member if you want to become a Sukai member'),
                                actions: [
                                  TextButton(
                                      onPressed: () => Navigator.pop(context, 'OK'),
                                      child: Text('OK'))
                                ],
                              )
                          );
                        },
                        child: Icon(LineIcons.questionCircle),
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      height: 30,
                      width: 30,
                      child: Radio(
                        value: UserType.client,
                        groupValue: _userType,
                        onChanged: (UserType? value) {
                          setState(() {
                            _userType = value;
                          });
                        },
                      ),
                    ),
                    GestureDetector(
                      onTap: (){
                        setState(() {
                          _userType = UserType.client;
                        });
                      },
                      child: Text(
                          'Register as Client',
                          style: TextStyle(fontSize: 16)
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: GestureDetector(
                        onTap: () {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) => AlertDialog(
                                title: Text('Register as Client'),
                                content: Text('Choose client if you or your company want to post ads or become an official distributor Store on Sukai marketplace'),
                                actions: [
                                  TextButton(
                                      onPressed: () => Navigator.pop(context, 'OK'),
                                      child: Text('OK'))
                                ],
                              )
                          );
                        },
                        child: Icon(LineIcons.questionCircle),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        Container(
          color: kSystemColor,
          height: 16,
        ),
        AnimatedSwitcher(
          duration: Duration(milliseconds: 300),
          child: _form,
        )
      ],
    );
  }
}
