import 'package:flutter/material.dart';

const kHobbyItems = <DropdownMenuItem<String>>[
  DropdownMenuItem(
      value: 'Automotive',
      child: Text('Automotive')
  ),
  DropdownMenuItem(
      value: 'Culinary',
      child: Text('Culinary')
  ),
  DropdownMenuItem(
      value: 'Fashion',
      child: Text('Fashion')
  ),
  DropdownMenuItem(
      value: 'Games',
      child: Text('Games')
  ),
  DropdownMenuItem(
      value: 'Entertainment',
      child: Text('Entertainment')
  ),
  DropdownMenuItem(
      value: 'Gadget',
      child: Text('Gadget')
  ),
  DropdownMenuItem(
      value: 'Music',
      child: Text('Music')
  ),
  DropdownMenuItem(
      value: 'Design',
      child: Text('Design')
  ),
  DropdownMenuItem(
      value: 'Photo & Video',
      child: Text('Photo & Video')
  ),
  DropdownMenuItem(
      value: 'Sport',
      child: Text('Sport')
  ),
  DropdownMenuItem(
      value: 'Technology',
      child: Text('Technology')
  ),
  DropdownMenuItem(
      value: 'Healthy',
      child: Text('Healthy')
  ),
  DropdownMenuItem(
      value: 'Other',
      child: Text('Other')
  ),
];

const kProductItems = <DropdownMenuItem<String>>[
  DropdownMenuItem(
      value: 'Automotive',
      child: Text('Automotive')
  ),
  DropdownMenuItem(
      value: 'Supplement',
      child: Text('Supplement')
  ),
  DropdownMenuItem(
      value: 'Food',
      child: Text('Food')
  ),
  DropdownMenuItem(
      value: 'Fashion',
      child: Text('Fashion')
  ),
  DropdownMenuItem(
      value: 'Cafe',
      child: Text('Cafe')
  ),
  DropdownMenuItem(
      value: 'Property',
      child: Text('Property')
  ),
  DropdownMenuItem(
      value: 'Cigarette',
      child: Text('Cigarette')
  ),
  DropdownMenuItem(
      value: 'Games',
      child: Text('Games')
  ),
  DropdownMenuItem(
      value: 'Digital',
      child: Text('Digital')
  ),
  DropdownMenuItem(
      value: 'Design',
      child: Text('Design')
  ),
  DropdownMenuItem(
      value: 'Home Appliances',
      child: Text('Home Appliances')
  ),
  DropdownMenuItem(
      value: 'Office Equipment',
      child: Text('Office Equipment')
  ),
  DropdownMenuItem(
      value: 'Stationary',
      child: Text('Stationary')
  ),
  DropdownMenuItem(
      value: 'Electronic',
      child: Text('Electronic')
  ),
  DropdownMenuItem(
      value: 'Sport',
      child: Text('Sport')
  ),
  DropdownMenuItem(
      value: 'Healthy Food',
      child: Text('Healthy Food')
  ),
  DropdownMenuItem(
      value: 'Other',
      child: Text('Other')
  ),
];