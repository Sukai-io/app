import 'package:android_play_install_referrer/android_play_install_referrer.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:sukai/models/sukai_api.dart';
import 'package:sukai/views/components/alerts.dart';
import 'package:sukai/views/login/login_screen.dart';

import '../../constants.dart';
import 'components/member_form_view_model.dart';

class SimpleRegisterScreen extends StatefulWidget {
  const SimpleRegisterScreen({Key? key}) : super(key: key);

  @override
  _SimpleRegisterScreenState createState() => _SimpleRegisterScreenState();
}

class _SimpleRegisterScreenState extends State<SimpleRegisterScreen> {
  final _formKey = GlobalKey<FormState>();
  final _referralKey = GlobalKey<FormFieldState>();
  final _usernameKey = GlobalKey<FormFieldState>();

  final TextEditingController _referralController = TextEditingController();
  bool? _referralValid;
  final TextEditingController _usernameController = TextEditingController();
  bool? _usernameValid;
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  bool? _emailExist;
  final TextEditingController _passwordController = TextEditingController();

  RoundedLoadingButtonController _buttonController = RoundedLoadingButtonController();

  @override
  void initState() {
    super.initState();
    _getReferrer();
  }

  void _getReferrer() async {
    String referral = '';
    try {
      ReferrerDetails? referrerDetails = await AndroidPlayInstallReferrer.installReferrer;
      if (referrerDetails != null && referrerDetails.installReferrer != null) {
        // utm_source=(not%20set)&utm_medium=(not%20set)
        List<String> pairs = referrerDetails.installReferrer!.split('&');
        List<String> srcPairs = pairs[0].split('=');
        String source = srcPairs[1];
        if ( source.isNotEmpty && source != '(not%20set)' && source != 'google-play') {
          referral = source;
        }
      }
    } catch (e) {
      print(e);
    }

    if (!mounted) return;

    setState(() {
      _referralController.text = referral;
    });
  }

  @override
  Widget build(BuildContext context) {
    Icon referralIcon = Icon(LineIcons.search);
    if (_referralValid != null && _referralValid == true) {
      referralIcon = Icon(LineIcons.check, color: Colors.greenAccent,);
    }

    Icon usernameIcon = Icon(LineIcons.search);
    if (_usernameValid != null && _usernameValid == true) {
      usernameIcon = Icon(LineIcons.check, color: Colors.greenAccent,);
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Signup / Registration'),
      ),
      body: ListView(
        children: [
          Container(
          padding: EdgeInsets.all(16),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                TextFormField(
                  key: _referralKey,
                  controller: _referralController,
                  decoration: InputDecoration(
                      hintText: "Referral's username",
                      labelText: 'Username Referral',
                      suffixIcon: IconButton(
                        icon: referralIcon,
                        onPressed: () async {
                          final isValid = await MemberFormViewModel.checkReferral(_referralController.text);
                          setState(() {
                            _referralValid = isValid;
                          });
                          _referralKey.currentState?.validate();
                        },
                      )
                  ),
                  onChanged: (value) {
                    setState(() {
                      _referralValid = null;
                    });
                  },
                  validator: (String? value) {
                    if (value == null || value.isEmpty) return 'Referral cannot be empty';
                    if (_referralValid != null && _referralValid == false) return 'Invalid referral username';
                    return null;
                  },
                ),
                TextFormField(
                  key: _usernameKey,
                  controller: _usernameController,
                  decoration: InputDecoration(
                      hintText: 'Your username',
                      labelText: 'Username',
                      suffixIcon: IconButton(
                        icon: usernameIcon,
                        onPressed: () async {
                          final isValid = await MemberFormViewModel.checkUsername(_usernameController.text);
                          setState(() {
                            _usernameValid = isValid;
                          });
                          _usernameKey.currentState?.validate();
                        },
                      )
                  ),
                  onChanged: (value) {
                    setState(() {
                      _usernameValid = null;
                    });
                  },
                  validator: (String? value) {
                    if (value == null || value.isEmpty) return 'Username cannot be empty';
                    if (_usernameValid != null && _usernameValid == false) return 'Invalid username';
                    return null;
                  },
                ),
                TextFormField(
                  controller: _nameController,
                  decoration: InputDecoration(
                    hintText: 'Input your name',
                    labelText: 'Name',
                  ),
                  validator: (String? value) {
                    if (value == null || value.isEmpty) return 'Name cannot be empty';
                  },
                ),
                TextFormField(
                  controller: _emailController,
                  decoration: InputDecoration(
                    hintText: 'Input your email',
                    labelText: 'Email',
                  ),
                  validator: (String? value) {
                    if (value == null || value.isEmpty) return 'Email cannot be empty';
                    bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value);
                    if (!emailValid) return 'Email format not valid';
                    if (_emailExist != null && _emailExist!) return 'Email already exist';
                  },
                ),
                TextFormField(
                  controller: _passwordController,
                  obscureText: true,
                  decoration: InputDecoration(
                    hintText: 'Input your password',
                    labelText: 'Password',
                  ),
                  validator: (String? value) {
                    if (value == null || value.isEmpty) return 'Password cannot be empty';
                  },
                ),
                TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(
                    hintText: 'Retype your password',
                    labelText: 'Retype Password',
                  ),
                  validator: (String? value) {
                    if (value == null || value.isEmpty) return 'Retype your password';
                    if (value != _passwordController.text) return "Password must match";
                  },
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                ),
                SizedBox(height: 20,),
                RoundedLoadingButton(
                  controller: _buttonController,
                  color: kPrimaryColor,
                  onPressed: () async {
                    try{
                      if (_emailController.text.isNotEmpty){
                        _emailExist = await SukaiApi().checkEmailExist(_emailController.text);
                      }

                      if (_formKey.currentState!.validate()) {

                        // check referral again
                        _referralValid = await MemberFormViewModel.checkReferral(_referralController.text);
                        if (!_referralValid!) {
                          _referralKey.currentState?.validate();
                          showDialog(
                              context: context,
                              builder: (BuildContext context) => ErrorAlert(errorMessage: 'Invalid referral. Please check your referral username.')
                          );
                          _buttonController.reset();
                          return;
                        }

                        // check username again
                        _usernameValid = await MemberFormViewModel.checkUsername(_usernameController.text);
                        if (!_usernameValid!) {
                          _usernameKey.currentState?.validate();
                          showDialog(
                              context: context,
                              builder: (BuildContext context) => ErrorAlert(errorMessage: 'Invalid username. Please change your username.')
                          );
                          _buttonController.reset();
                          return;
                        }

                        final result = await MemberFormViewModel.simpleMemberFormViewModel(
                            _referralController.text,
                            _usernameController.text,
                            _nameController.text,
                            _emailController.text,
                            _passwordController.text).simpleRegister();

                        if (result.success) {
                          showDialog(context: context, builder: (BuildContext context) => AlertDialog(
                            title: Text('Success'),
                            content: Text('Sign up success!'),
                            actions: [
                              TextButton(
                                  onPressed: () {
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (context) => LoginScreen())
                                    );
                                  },
                                  child: Text('OK'))
                            ],
                          ));

                        } else {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) => ErrorAlert(errorMessage: result.error??'Sign up failed.')
                          );
                        }
                      } else {

                      }
                    } catch(e) {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) => ErrorAlert(errorMessage: e.toString())
                      );
                    } finally {
                      _buttonController.reset();
                    }
                  },
                  child: Text('SIGN UP', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                ),
              ],
            ),
          ) ,
        ),
        ]
      )
    );
  }
}
