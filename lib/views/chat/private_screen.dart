import 'package:flutter/material.dart';

class PrivateScreen extends StatefulWidget {
  const PrivateScreen({Key? key}) : super(key: key);

  @override
  _PrivateScreenState createState() => _PrivateScreenState();
}

class _PrivateScreenState extends State<PrivateScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(child: Text('Private Chat')),
    );
  }
}
