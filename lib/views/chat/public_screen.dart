import 'package:connectycube_sdk/connectycube_chat.dart';
import 'package:flutter/material.dart';

class PublicScreen extends StatefulWidget {
  const PublicScreen({Key? key}) : super(key: key);

  @override
  _PublicScreenState createState() => _PublicScreenState();
}

class _PublicScreenState extends State<PublicScreen> with WidgetsBindingObserver {
  late CubeUser user;
  late CubeDialog newDialog;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    user = CubeUser(id: 4962445, login: "andhikalegawa", password: "dhikakeyen");

    CubeChatConnection.instance.login(user).then((user){
      print("Success login to the chat");
    }).catchError((error){
      print("Error was occured during login to the chat");
    });

    newDialog = CubeDialog(
        CubeDialogType.PUBLIC,
        name: "Blockchain trends",
        description: "Public dialog Description",
        photo: "https://some.url/to/avatar.jpeg");

    createDialog(newDialog).then((createdDialog) {

    }).catchError((error) {});
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(child: Text('Public Chat')),
    );
  }
}
