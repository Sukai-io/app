import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:collection/collection.dart' show IterableExtension;
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:connectycube_sdk/connectycube_chat.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:sukai/constants.dart';
import 'package:sukai/models/sukai_api.dart';
import 'package:sukai/views/chat/components/chat_dialog_screen.dart';
import 'package:sukai/views/components/alerts.dart';
import 'package:sukai/views/components/menu.dart';
import 'package:sukai/views/home/home_screen.dart';
import 'components/api_utils.dart';
import 'components/common.dart';
import 'components/new_dialog_screen.dart';

class ChatScreen extends StatefulWidget {
  ChatScreen({Key? key}) : super(key: key);

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> with WidgetsBindingObserver {
  late StreamSubscription<ConnectivityResult> connectivityStateSubscription;
  AppLifecycleState? appState;
  CubeUser? currentUser;
  var _isDialogContinues = true;

  List<ListItem<CubeDialog>> dialogList = [];
  List<CubeUser> userList = [];

  StreamSubscription<CubeMessage>? msgSubscription;
  late ChatMessagesManager? chatMessagesManager;

  @override
  void initState() {
    super.initState();
    final chatUser = CubeUser(login: SukaiApi().currentUser!.username, password: SukaiApi().getToken());
    init(kCCubeAppId, kCCUbeAuthKey, kCCUbeSecret);

    createSession(chatUser).then((cubeSession) async {
      if(!CubeChatConnection.instance.isAuthenticated()) {
        var tempUser = cubeSession.user!..password = cubeSession.token;
        _loginToCubeChat(tempUser);
      } else {
        setState(() {
          currentUser = cubeSession.user;
        });
        _loadDialogs();
      }
    }).catchError((error) {
      _showError();
    });

    connectivityStateSubscription =
        Connectivity().onConnectivityChanged.listen((connectivityType) {
          if (AppLifecycleState.resumed != appState) return;

          if (connectivityType != ConnectivityResult.none) {
            log("chatConnectionState = ${CubeChatConnection.instance.chatConnectionState}");
            bool isChatDisconnected =
                CubeChatConnection.instance.chatConnectionState ==
                    CubeChatConnectionState.Closed ||
                    CubeChatConnection.instance.chatConnectionState ==
                        CubeChatConnectionState.ForceClosed;

            if (isChatDisconnected &&
                CubeChatConnection.instance.currentUser != null) {
              CubeChatConnection.instance.relogin();
            }
          }
        });

    appState = WidgetsBinding.instance!.lifecycleState;
    WidgetsBinding.instance!.addObserver(this);
  }

  void _loadDialogs() async {
    final roomList = await SukaiApi().getPublicChat();
    for (var i=0; i < roomList.length; i++) {
      final publicDialog = await subscribeToDialog(roomList[i]);
      log("join public dialog= ${publicDialog.name}");
    }

    final dialogs = await getDialogs();
    if (dialogs == null) return;
    _isDialogContinues = false;
    List<CubeUser> participantList = [];
    for(var i=0; i < dialogs.items.length; i++) {
      final dialog = dialogs.items[i];
      if (dialog.type != CubeDialogType.PUBLIC) continue;
      final pagedUsers = await getDialogOccupants(dialog.dialogId!);
      if (pagedUsers == null) continue;
      final userList = pagedUsers.items.whereNot((user) {return user.id == currentUser!.id;}).toList();
      userList.forEach((user) {
        if (participantList.contains(user)) return;
        participantList.add(user);
      });
    }

    setState(() {
      dialogList.clear();
      dialogList.addAll(dialogs.items.map((dialog) => ListItem(dialog)).toList());
      userList.clear();
      userList = participantList;
    });
  }

  void _loginToCubeChat(CubeUser user) {
    CubeChatConnectionSettings.instance.totalReconnections = 0;
    CubeChatConnection.instance.login(user).then((cubeUser) {
      setState(() {
        currentUser = cubeUser;
      });
      _loadDialogs();
    }).catchError((error) {
      _showError();
    });
  }

  void _showError() {
    showDialog(
        context: context,
        builder: (BuildContext context) => ErrorAlert(errorMessage: 'Chat session error')
    );
  }

  void onReceiveMessage(CubeMessage message) {
    log("onReceiveMessage global message= $message");
    updateDialog(message);
  }

  updateDialog(CubeMessage msg) {
    ListItem<CubeDialog>? dialogItem = dialogList.firstWhereOrNull(
            (dlg) => dlg.data.dialogId == msg.dialogId);
    if (dialogItem == null) return;
    dialogItem.data.lastMessage = msg.body;
    setState(() {
      dialogItem.data.lastMessage = msg.body;
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
          appBar: AppBar(
            title: Text('Sukai Chat'),
            bottom: TabBar(
              tabs: [
                Tab(text: "PUBLIC",),
                Tab(text: "GROUP",),
                Tab(text: "PRIVATE",),
              ],
            ),
            actions: <Widget>[
              IconButton(
                onPressed: () => _logout(context),
                icon: Icon(
                  Icons.logout,
                  color: Colors.white,
                ),
              ),
            ],
          ),
          floatingActionButton: FloatingActionButton(
            heroTag: "New chat",
            child: Icon(
              Icons.chat,
              color: Colors.white,
            ),
            onPressed: () => _createNewDialog(context),
          ),
          resizeToAvoidBottomInset: false,
          drawer: MenuDrawer(),
          body: TabBarView(
              children: [
                Container(
                  child: Column(
                    children: [
                      Visibility(
                        visible: currentUser == null,
                        child: Container(
                          margin: EdgeInsets.all(40),
                          alignment: FractionalOffset.center,
                          child: CircularProgressIndicator(
                            strokeWidth: 2,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: _getDialogsList(context, CubeDialogType.PUBLIC),
                      ),
                      Container(
                        padding: EdgeInsets.all(16),
                        color: kSystemColor,
                        alignment: AlignmentDirectional.centerStart,
                        child: Text('Public chat participants :'),
                      ),
                      Expanded(
                        flex: 5,
                        child: _getParticipantList(context),
                      ),
                      Container(
                        color: kPrimaryLightColor,
                        padding: EdgeInsets.fromLTRB(16, 16, 16, 64),
                        child: Text('Tap a public chat or create a new group/private dialog to start chatting.'),
                      )
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children: [
                      Visibility(
                        visible: currentUser != null && _isDialogContinues && dialogList.isEmpty,
                        child: Container(
                          margin: EdgeInsets.all(40),
                          alignment: FractionalOffset.center,
                          child: CircularProgressIndicator(
                            strokeWidth: 2,
                          ),
                        ),
                      ),
                      Expanded(
                        child: _getDialogsList(context, CubeDialogType.GROUP),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children: [
                      Visibility(
                        visible: currentUser != null && _isDialogContinues && dialogList.isEmpty,
                        child: Container(
                          margin: EdgeInsets.all(40),
                          alignment: FractionalOffset.center,
                          child: CircularProgressIndicator(
                            strokeWidth: 2,
                          ),
                        ),
                      ),
                      Expanded(
                        child: _getDialogsList(context, CubeDialogType.PRIVATE),
                      ),
                    ],
                  ),
                ),
              ]
          ),
      ),
    );
  }

  void _logout(BuildContext context) {
    // set up the buttons
    Widget cancelButton = TextButton(
      child: Text("Cancel"),
      onPressed:  () {},
    );
    Widget continueButton = TextButton(
      child: Text("Continue"),
      onPressed:  () {
        CubeChatConnection.instance.logout();
        Navigator.pushAndRemoveUntil(context,
          MaterialPageRoute(builder: (context) => HomeScreen()),
              (Route<dynamic> route) => false,
        );
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Logout"),
      content: Text("Logout from chat?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Widget _getDialogsList(BuildContext context, int type) {
    if (currentUser == null) {
      return Center(
        child: Text('No Chat Session'),
      );
    }

    final filteredDialogList = dialogList.where((dialog) => dialog.data.type == type).toList();

    if (_isDialogContinues && filteredDialogList.isEmpty)
      return SizedBox.shrink();
    else if (filteredDialogList.isEmpty)
      return Center(
        child: Text('Empty Chat'),
      );
    else
      return ListView.separated(
        itemCount: filteredDialogList.length,
        itemBuilder: (BuildContext context, int index) {
          getDialogIcon() {
            var dialog = filteredDialogList[index].data;
            if (dialog.type == CubeDialogType.PRIVATE)
              return Icon(
                Icons.person,
                size: 40.0,
                color: greyColor,
              );
            else {
              return Icon(
                Icons.group,
                size: 40.0,
                color: greyColor,
              );
            }
          }

          getDialogAvatarWidget() {
            var dialog = filteredDialogList[index].data;
            if (dialog.photo == null) {
              return CircleAvatar(
                  radius: 25, backgroundColor: greyColor3, child: getDialogIcon());
            } else {
              return CachedNetworkImage(
                placeholder: (context, url) => Container(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                  ),
                  width: 40.0,
                  height: 40.0,
                  padding: EdgeInsets.all(70.0),
                  decoration: BoxDecoration(
                    color: greyColor2,
                    borderRadius: BorderRadius.all(
                      Radius.circular(8.0),
                    ),
                  ),
                ),
                imageUrl: filteredDialogList[index].data.photo!,
                width: 45.0,
                height: 45.0,
                fit: BoxFit.cover,
              );
            }
          }

          return Container(
            child: TextButton(
              child: Row(
                children: <Widget>[
                  Material(
                    child: getDialogAvatarWidget(),
                    borderRadius: BorderRadius.all(Radius.circular(25.0)),
                    clipBehavior: Clip.hardEdge,
                  ),
                  Flexible(
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Container(
                            child: Text(
                              '${filteredDialogList[index].data.name ?? 'Not available'}',
                              style: TextStyle(
                                  color: primaryColor,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20.0),
                            ),
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 5.0),
                          ),
                          Container(
                            child: Text(
                              '${filteredDialogList[index].data.lastMessage ?? filteredDialogList[index].data.description ?? ""}',
                              style: TextStyle(color: primaryColor),
                            ),
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
                          ),
                        ],
                      ),
                      margin: EdgeInsets.only(left: 20.0),
                    ),
                  ),
                  Visibility(
                    child: IconButton(
                      iconSize: 25.0,
                      icon: Icon(
                        Icons.delete,
                        color: themeColor,
                      ),
                      onPressed: () {
                        _deleteDialog(context, filteredDialogList[index].data);
                      },
                    ),
                    maintainSize: true,
                    maintainAnimation: true,
                    maintainState: true,
                    visible: filteredDialogList[index].isSelected,
                  ),
                  Container(
                    child: Text(
                      '${filteredDialogList[index].data.lastMessageDateSent != null ? DateFormat('MMM dd').format(DateTime.fromMillisecondsSinceEpoch(filteredDialogList[index].data.lastMessageDateSent! * 1000)) : ""}',
                      style: TextStyle(color: primaryColor),
                    ),
                  ),
                ],
              ),
              onLongPress: () {
                if ( filteredDialogList[index].data.type == CubeDialogType.PUBLIC ) {
                  return;
                }
                setState(() {
                  filteredDialogList[index].isSelected = !filteredDialogList[index].isSelected;
                });
              },
              onPressed: () {
                _openDialog(context, filteredDialogList[index].data);
              },
            ),
            margin: EdgeInsets.only(left: 5.0, right: 5.0),
          );
        },
        separatorBuilder: (context, index) {
          return Divider(thickness: 2, indent: 8, endIndent: 8);
        },
      );
  }

  Widget _getParticipantList(BuildContext context) {
    if (userList.length == 0) {
      return Center(child: Text('No Participants'));
    }

    return ListView.builder(
        itemCount: userList.length,
        itemBuilder: (BuildContext context, int index) {
          getPrivateWidget() {
            return Container(
              child: TextButton(
                child: Row(
                  children: <Widget>[
                    Material(
                      child: CircleAvatar(
                        radius: 30,
                        backgroundColor: Colors.white,
                        child: CircleAvatar(
                          backgroundImage: userList[index].avatar != null &&
                              userList[index].avatar!.isNotEmpty
                              ? NetworkImage(userList[index].avatar!)
                              : null,
                          radius: 25,
                          child: getAvatarTextWidget(
                              userList[index].avatar != null &&
                                  userList[index].avatar!.isNotEmpty,
                              userList[index].fullName!.substring(0, 2).toUpperCase()),
                        ),
                      ),
                      borderRadius: BorderRadius.all(
                        Radius.circular(40.0),
                      ),
                      clipBehavior: Clip.hardEdge,
                    ),
                    Flexible(
                      child: Container(
                        child: Column(
                          children: <Widget>[
                            Container(
                              child: Text(
                                '${userList[index].fullName}',
                                style: TextStyle(color: primaryColor),
                              ),
                              alignment: Alignment.centerLeft,
                              margin: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 5.0),
                            ),
                          ],
                        ),
                        margin: EdgeInsets.only(left: 20.0),
                      ),
                    ),
                    Container(
                      child: Icon(
                        Icons.arrow_forward,
                        size: 25.0,
                        color: themeColor,
                      ),
                    ),
                  ],
                ),
                onPressed: () {
                  _createPrivateDialog(context, userList[index].id!);
                },
              ),
              margin: EdgeInsets.only(bottom: 10.0, left: 5.0, right: 5.0),
            );
          }
          return getPrivateWidget();
        });
  }

  void _openDialog(BuildContext context, CubeDialog dialog) async {
    Navigator.push(context,
      MaterialPageRoute(
          builder: (context) => ChatDialogScreen(currentUser!, dialog)
      ),
    ).then((value) => refresh());
  }

  void _createPrivateDialog(context, int userId) {
    CubeDialog newDialog =
    CubeDialog(CubeDialogType.PRIVATE, occupantsIds: [userId]);
    createDialog(newDialog).then((createdDialog) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ChatDialogScreen(currentUser!, createdDialog),
        ),
      ).then((value) => refresh());
    }).catchError((error) {
      Fluttertoast.showToast(msg: error);
    });
  }

  void _deleteDialog(BuildContext context, CubeDialog dialog) async {
    log("_deleteDialog= $dialog");
    if (dialog.dialogId != null) {
      deleteDialog(dialog.dialogId!, false)
          .then((value) => refresh())
          .catchError((error) {
            Fluttertoast.showToast(msg: error);
          });
    }
  }

  void _createNewDialog(BuildContext context) async {
    Navigator.push(context,
      MaterialPageRoute(
        builder: (context) => CreateChatScreen(currentUser!),
      ),
    ).then((value) => refresh());
  }

  void refresh() {
    _loadDialogs();
  }

  @override
  void dispose() {
    connectivityStateSubscription.cancel();
    WidgetsBinding.instance!.removeObserver(this);
    msgSubscription?.cancel();
    CubeChatConnection.instance.logout();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    log("Current app state: $state");
    appState = state;

    if (AppLifecycleState.paused == state) {

    } else if (AppLifecycleState.resumed == state) {

    }
  }
}