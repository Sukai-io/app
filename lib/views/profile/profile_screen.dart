import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:sukai/intl_phone_field/countries.dart';
import 'package:sukai/intl_phone_field/intl_phone_field.dart';
import 'package:sukai/models/sukai_api.dart';
import 'package:sukai/models/user.dart';
import 'package:sukai/views/components/alerts.dart';
import 'package:sukai/views/components/menu.dart';
import 'package:sukai/views/home/home_screen.dart';
import 'package:sukai/views/register/components/dropdowns.dart';
import 'dart:io';
import '../../constants.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _referralController = TextEditingController();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _cityController = TextEditingController();
  final TextEditingController _addressController = TextEditingController();
  final TextEditingController _professionController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final TextEditingController _walletController = TextEditingController();

  RoundedLoadingButtonController _buttonController = RoundedLoadingButtonController();

  PickedFile? _file;
  final ImagePicker _picker = ImagePicker();
  bool? _emailExist;
  String _country = 'US';
  String? _phone;
  bool? _phoneExist;
  String? _hobby;
  Image avatar = Image.asset("assets/icons/ic_profile.png");

  MemberProfile? _profile;

  @override
  void initState() {
    _loadProfile();
    super.initState();
  }

  _loadProfile() async {
    final profile = await SukaiApi().getProfile();
    setState(() {
      _profile = profile;
      avatar = Image.network(_profile!.avatarUrl);
      _referralController.text = _profile!.referral;
      _usernameController.text = _profile!.username;
      _nameController.text = _profile!.name;
      _emailController.text = _profile!.email;
      _country = _profile!.country;
      _phone = _profile!.phone;
      try {
        final country = countries.firstWhere((c) => c["code"] == _country);
        _phoneController.text = _phone!.substring(country["dial_code"].toString().length+1);
      } catch (e) {
        _country = 'US';
        _phoneController.text = _phone!;
      }
      _cityController.text = _profile!.city;
      _addressController.text = _profile!.address;
      try {
        final hobbyDropdown = kHobbyItems.firstWhere((dropdown) => dropdown.value == _profile!.hobby);
        _hobby = hobbyDropdown.value;
      } catch (e) {
      }
      _professionController.text = _profile!.profession;
      _descriptionController.text = _profile!.description;
      _walletController.text = _profile!.wallet;
    });
  }

  _chooseImage() async {
    try {
      final pickedFile = await _picker.getImage(source: ImageSource.gallery);
      setState(() {
        _file = pickedFile;
        avatar = Image.file(File(_file!.path));
      });
    } catch(e) {
      showDialog(
          context: context,
          builder: (BuildContext context) => ErrorAlert(errorMessage: e.toString())
      );
    }
  }

  void rebuildAllChildren(BuildContext context) {
    void rebuild(Element el) {
      el.markNeedsBuild();
      el.visitChildren(rebuild);
    }
    (context as Element).visitChildren(rebuild);
  }

  @override
  Widget build(BuildContext context) {
    rebuildAllChildren(context);
    return Scaffold(
        appBar: AppBar(title: Text('PROFILE'),),
        drawer: MenuDrawer(),
        body: ListView(
            children: [
              Container(
                padding: EdgeInsets.all(16),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Container(
                        child: Center(
                          child: Column(
                            children: [
                              GestureDetector(
                                onTap: (){
                                  _chooseImage();
                                },
                                child: CircleAvatar(
                                  radius: 52,
                                  backgroundColor: Color(0xffFDCF09),
                                  child: CircleAvatar(
                                    radius: 50,
                                    backgroundColor: Colors.white,
                                    backgroundImage: avatar.image,
                                  ),
                                ),
                              ),
                              GestureDetector(
                                  onTap: (){
                                    _chooseImage();
                                  },
                                  child: Text('Edit Photo', style: TextStyle(decoration: TextDecoration.underline),)
                              ),
                            ],
                          ),
                        ),
                      ),
                      TextFormField(
                        enabled: false,
                        controller: _referralController,
                        decoration: InputDecoration(
                          hintText: 'Username',
                          labelText: 'Referral Username',
                          border: InputBorder.none,
                        )
                      ),
                      TextFormField(
                        enabled: false,
                          controller: _usernameController,
                        decoration: InputDecoration(
                          hintText: 'Username',
                          labelText: 'Username',
                          border: InputBorder.none,
                        )
                      ),
                      TextFormField(
                          controller: _descriptionController,
                          decoration: InputDecoration(
                            hintText: 'Description',
                            labelText: 'Description',
                          )
                      ),
                      TextFormField(
                          controller: _walletController,
                          maxLines: 2,
                          decoration: InputDecoration(
                            hintText: 'Input Wallet address',
                            labelText: 'Wallet',
                            floatingLabelBehavior: FloatingLabelBehavior.always
                          )
                      ),
                      TextFormField(
                        controller: _emailController,
                        decoration: InputDecoration(
                          hintText: 'Input your email',
                          labelText: 'Email',
                        ),
                        validator: (String? value) {
                          if (value == null || value.isEmpty) return 'Email cannot be empty';
                          bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value);
                          if (!emailValid) return 'Email format not valid';
                          if (_emailExist != null && _emailExist!) return 'Email already exist';
                        },
                      ),
                      TextFormField(
                        controller: _nameController,
                        decoration: InputDecoration(
                          hintText: 'Input your name',
                          labelText: 'Name',
                        ),
                        validator: (String? value) {
                          if (value == null || value.isEmpty) return 'Name cannot be empty';
                        },
                      ),
                      IntlPhoneField(
                        controller: _phoneController,
                        decoration: InputDecoration(
                            labelText: 'Phone Number',
                            hintText: 'Input your phone'
                        ),
                        initialCountryCode: _country,
                        onChanged: (phone) {
                          _phone = phone.completeNumber;
                        },
                        onCountryChanged: (phone){
                          _country = phone.countryISOCode??'US';
                          if (phone.countryCode != null) {
                            _phone = phone.countryCode! + _phoneController.text;
                          }
                        },
                        autoValidate: false,
                        validator: (String? value){
                          if (value == null || value.isEmpty) return 'Phone cannot be empty';
                          if (_phoneExist != null && _phoneExist!) return 'Phone already used';
                        },
                      ),
                      TextFormField(
                        controller: _cityController,
                        decoration: InputDecoration(
                          hintText: 'Input your city',
                          labelText: 'City',
                        ),
                      ),
                      TextFormField(
                        controller: _addressController,
                        decoration: InputDecoration(
                          hintText: 'Input your address',
                          labelText: 'Address',
                        ),
                      ),
                      DropdownButtonFormField(
                        value: _hobby,
                        isExpanded: true,
                        decoration: InputDecoration(
                            labelText: 'Select your hobby'
                        ),
                        items: kHobbyItems,
                        onChanged: (String? value){
                          setState(() {
                            _hobby = value;
                          });
                        },
                        validator: (String? value) {
                          if (value == null || value.isEmpty) return 'Please select a hobby';
                          return null;
                        },
                      ),
                      TextFormField(
                        controller: _professionController,
                        decoration: InputDecoration(
                            hintText: 'Input your profession',
                            labelText: 'Your Profession'
                        ),
                        validator: (String? value){
                          if (value == null || value.isEmpty) return 'Profession cannot be empty';
                          return null;
                        },
                      ),
                      SizedBox(height: 32,),
                      RoundedLoadingButton(
                        controller: _buttonController,
                        color: kPrimaryColor,
                        onPressed: () async {
                          try{
                            if (_emailController.text.isNotEmpty && _profile!.email != _emailController.text) {
                              _emailExist = await SukaiApi().checkEmailExist(_emailController.text);
                            }

                            if (_phone != null && _phone!.isNotEmpty && _profile!.phone != _phone) {
                              _phoneExist = await SukaiApi().checkPhoneExist(_phone!);
                            }

                            if (_formKey.currentState!.validate()) {
                              final result = await SukaiApi().updateProfile(
                                  MemberProfile("", "", "",
                                      _emailController.text,
                                      _nameController.text,
                                      _country,
                                      _phone!, "",
                                      _cityController.text,
                                      _addressController.text,
                                      _hobby!,
                                      _professionController.text,
                                      _descriptionController.text,
                                      _walletController.text), _file?.path);
                              if (result.success) {
                                showDialog(context: context, builder: (BuildContext context) => AlertDialog(
                                  title: Text('Success'),
                                  content: Text('Profile updated!'),
                                  actions: [
                                    TextButton(
                                        onPressed: () {
                                          Navigator.push(context,
                                              MaterialPageRoute(builder: (context) => HomeScreen())
                                          );
                                        },
                                        child: Text('OK'))
                                  ],
                                ));

                              } else {
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) => ErrorAlert(errorMessage: result.message??'Profile update failed.')
                                );
                              }
                            } else {

                            }
                          } catch(e) {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) => ErrorAlert(errorMessage: e.toString())
                            );
                          } finally {
                            _buttonController.reset();
                          }
                        },
                        child: Text('UPDATE', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                      ),
                    ],
                  ),
                ) ,
              ),
            ]
        )
    );
  }
}