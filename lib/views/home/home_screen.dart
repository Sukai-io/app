import 'package:flutter/material.dart';
import 'package:sukai/views/components/appbar.dart';
import 'package:sukai/views/components/menu.dart';
import 'package:sukai/views/home/components/body.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: ProfileAppBar(showDetail: true),
        drawer: MenuDrawer(),
        body: Body()
    );
  }
}