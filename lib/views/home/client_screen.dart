import 'package:flutter/material.dart';
import 'package:sukai/views/components/client_appbar.dart';
import 'package:sukai/views/components/client_menu.dart';
import 'package:sukai/views/home/components/client_body.dart';

class ClientScreen extends StatelessWidget {
  const ClientScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: ClientAppbar(showDetail: true),
        drawer: ClientMenuDrawer(),
        body: ClientBody()
    );
  }
}