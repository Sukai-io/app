import 'package:flutter/material.dart';
import 'package:speech_bubble/speech_bubble.dart';
import 'package:sukai/constants.dart';
import 'package:sukai/views/components/alerts.dart';

class SmartAdvert extends StatefulWidget {
  const SmartAdvert({Key? key}) : super(key: key);

  @override
  _SmartAdvertState createState() => _SmartAdvertState();
}

class _SmartAdvertState extends State<SmartAdvert> {
  @override
  Widget build(BuildContext context) {
    double sWidth = MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.all(4),
      child: Column(
        children: [
          Row(
            children: [
              Text('Smart Advertising:'),
              Spacer(),
              Icon(Icons.circle, color: kPrimaryColor, size: 8,),
              GestureDetector(
                onTap: (){
                  showDialog(
                    context: context,
                    builder: (BuildContext context) => UnderConstruction(),
                  );
                },
                child: Text(' View All'),
              )
            ],
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 8, horizontal: 0),
            color: kPrimaryColor,
            height: 2,
          ),
          Row(
            children: [
              Container(
                width: (sWidth-8) * 0.25,
                height: ((sWidth-8) * 0.25) * 1.4,
                child: AdvertColumn('LIP GLOSS', 'assets/images/01_lip_gloss.jpg'),
              ),
              Container(
                width: (sWidth-8) * 0.25,
                height: ((sWidth-8) * 0.25) * 1.4,
                child: AdvertColumn('VITAMIN C', 'assets/images/02_vitamin_c.jpg'),
              ),
              Container(
                width: (sWidth-8) * 0.25,
                height: ((sWidth-8) * 0.25) * 1.4,
                child: AdvertColumn('LUXURY CAR', 'assets/images/03_luxury_car.jpg'),
              ),
              Container(
                width: (sWidth-8) * 0.25,
                height: ((sWidth-8) * 0.25) * 1.4,
                child: AdvertColumn('RUNNING SHOES', 'assets/images/04_running_shoes.jpg'),
              ),
            ],
          )
        ],
      )
    );
  }
}

class AdvertColumn extends StatelessWidget {
  final String title;
  final String imageUrl;
  const AdvertColumn(this.title, this.imageUrl, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Flexible(
            flex: 1,
            child: Container(
              padding: EdgeInsets.all(4),
              child: Text(
                title,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold
                ),
              ),
            )
        ),
        Flexible(
            flex: 2,
            child: Container(
              padding: EdgeInsets.all(4),
              child: GestureDetector(
                onTap: (){
                  showDialog(
                    context: context,
                    builder: (BuildContext context) => UnderConstruction(),
                  );
                },
                child: Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                        image: Image.asset(imageUrl).image,
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(
                          color: kPrimaryColor,
                          width: 1
                      )
                  ),
                ),
              ),
            )
        ),
        Flexible(
            flex: 1,
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: (){
                      showDialog(
                        context: context,
                        builder: (BuildContext context) => UnderConstruction(),
                      );
                    },
                    child: Container(
                      margin: EdgeInsets.all(4),
                      child: LayoutBuilder(
                          builder: (context, constraint) {
                            return SpeechBubble(
                              padding: EdgeInsets.all(6),
                              color: kPrimaryColor,
                              nipLocation: NipLocation.BOTTOM,
                              child : Row(
                                children: [
                                  Icon(Icons.person, color: Colors.white, size: constraint.biggest.height * 0.5,),
                                  SizedBox(width: constraint.biggest.height * 0.5,),
                                  Icon(Icons.favorite, color: Colors.white, size: constraint.biggest.height * 0.5,),
                                  SizedBox(width: constraint.biggest.height * 0.5,),
                                  Icon(Icons.chat_bubble, color: Colors.white, size: constraint.biggest.height * 0.5,)
                                ]),
                            );
                          }
                      ),
                    ),
                  )
                ],
              ),
            )
        ),
      ],
    );
  }
}

