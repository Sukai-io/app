import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sukai/constants.dart';
import 'package:sukai/models/smart_ads.dart';

typedef SmartAdsCallback = void Function(SmartAds ads);

class ClientAdsCard extends StatelessWidget {
  final SmartAds ads;
  final SmartAdsCallback onTap;
  const ClientAdsCard({Key? key, required this.ads, required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextStyle style = TextStyle(
      color: kPrimaryColor,
      fontSize: 14,
      fontWeight: FontWeight.bold
    );
    final TextStyle normal = TextStyle(
        color: kPrimaryColor,
        fontSize: 12,
    );
    final TextStyle small = TextStyle(
      fontSize: 11,
      fontWeight: FontWeight.bold
    );

    String statusString = 'Display Until : AUG 31, 2021';
    Color statusColor = Colors.lightGreen;
    switch (ads.status) {
      case AdsStatus.finished :
        statusString = 'FINISHED';
        statusColor = Colors.red;
        break;
      case AdsStatus.draft :
        statusString = 'DRAFT';
        statusColor = Colors.yellow;
        break;
      case AdsStatus.active :
        break;
    }
    return Card(
      elevation: 3,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(5)),),
      child: InkWell(
        onTap:(){
          onTap(ads);
        },
        child: Row(
          children: [
            Container(
              width: 100,
              height: 100,
              child: ClipRRect(
                  borderRadius: BorderRadius.only(topLeft:Radius.circular(5), bottomLeft: Radius.circular(5)),
                  child: Image.asset(ads.thumbnail, fit: BoxFit.cover,)),
            ),
            Expanded(
              child: Container(
                height: 100,
                padding: EdgeInsets.all(4),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(ads.title, style: style, overflow: TextOverflow.ellipsis,),
                    Text('CLICKS: 10000 / 15000', style: normal, overflow: TextOverflow.ellipsis,),
                    Spacer(),
                    Table(
                      border: null,
                      children: [
                        TableRow(
                          children: [
                            Row(
                              children: [
                                Icon(Icons.people, color: kPrimaryColor, size: 14,),
                                Text('10000', style: small,),
                              ],
                            ),
                            Row(
                              children: [
                                Icon(Icons.male, color: kPrimaryColor, size: 14,),
                                Text('7000', style: small,),
                              ],
                            ),
                            Row(
                              children: [
                                Icon(Icons.female, color: kPrimaryColor, size: 14,),
                                Text('3000', style: small,),
                              ],
                            )
                          ]
                        ),
                        TableRow(
                          children: [
                            Row(
                              children: [
                                Icon(Icons.favorite, color: kPrimaryColor, size: 14,),
                                Text('10', style: small,),
                              ],
                            ),
                            Row(
                              children: [
                                Icon(Icons.chat_bubble, color: kPrimaryColor, size: 14,),
                                Text('10000', style: small,),
                              ],
                            ),
                            Row(
                              children: [
                                Icon(Icons.block_rounded, color: kPrimaryColor, size: 14,),
                                Text('3', style: small,),
                              ],
                            )
                          ]
                        )
                      ],
                    ),
                    Spacer(),
                    Center(child: Text(
                      statusString,
                      style: small,
                      overflow: TextOverflow.ellipsis,)
                    ),
                  ],
                ),
              ),
            ),
            Container(
              width: 8,
              height: 100,
              child: ClipRRect(
                  borderRadius: BorderRadius.only(topRight:Radius.circular(5), bottomRight: Radius.circular(5)),
                  child: DecoratedBox(
                    decoration: BoxDecoration(
                      color: statusColor
                    ),
                  ),
              )
            ),
          ],
        ),
      ),
    );
  }
}

enum FilterType { none, active, finished, draft }

class ClientAdsHeader extends StatefulWidget {

  ClientAdsHeader({Key? key}) : super(key: key);

  @override
  _ClientAdsHeaderState createState() => _ClientAdsHeaderState();
}

class _ClientAdsHeaderState extends State<ClientAdsHeader> {
  FilterType? _filterType = FilterType.none;

  static double? _radioSize = 24.0;

  @override
  Widget build(BuildContext context) {
    final TextStyle style = TextStyle(
      color: kPrimaryColor,
      fontSize: 16,
    );
    return Container(
      padding: EdgeInsets.all(16),
      child: Row(
        children: [
          Container(
            width: 75,
            child: Text('FILTER :', style: style),
          ),
          Expanded(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Row(
                    children: [
                      Container(
                        width: _radioSize,
                        height: _radioSize,
                        child: Radio(
                          value: FilterType.active,
                          groupValue: _filterType,
                          onChanged: (FilterType? value) {
                            setState(() {
                              _filterType = value;
                            });
                          },
                        ),
                      ),
                      Text('ACTIVE', style: style),
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        width: _radioSize,
                        height: _radioSize,
                        child: Radio(
                          value: FilterType.finished,
                          groupValue: _filterType,
                          onChanged: (FilterType? value) {
                            setState(() {
                              _filterType = value;
                            });
                          },
                        ),
                      ),
                      Text('FINISHED', style: style),
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        width: _radioSize,
                        height: _radioSize,
                        child: Radio(
                          value: FilterType.draft,
                          groupValue: _filterType,
                          onChanged: (FilterType? value) {
                            setState(() {
                              _filterType = value;
                            });
                          },
                        ),
                      ),
                      Text('DRAFT', style: style),
                    ],
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

