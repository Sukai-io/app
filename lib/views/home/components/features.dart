import 'package:flutter/material.dart';
import 'package:share_plus/share_plus.dart';
import 'package:sukai/models/sukai_api.dart';
import 'package:sukai/views/chat/chat_screen.dart';
import 'package:sukai/views/giveaway/giveaway_screen.dart';
import 'package:sukai/views/nft/nft_market_screen.dart';

class Features extends StatelessWidget {
  const Features({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double sWidth = MediaQuery.of(context).size.width;
    return Container(
      child: Column(
        children: [
          // Container(
          //   margin: EdgeInsets.symmetric(vertical: 0, horizontal: 4),
          //   color: kPrimaryColor,
          //   height: 2,
          // ),
          Container(
            margin: EdgeInsets.only(bottom: 10),
            width: sWidth * 1,
            child: GestureDetector(
              onTap: (){
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => GiveawayScreen())
                );
              },
              child: Image.asset('assets/images/features/give-away-ns.png'),
            ),
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 2,
                  blurRadius: 5,
                  offset: Offset(0, 4), // changes position of shadow
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 10),
            width: sWidth * 1,
            child: GestureDetector(
              onTap: (){
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => NFTMarketScreen())
                );
              },
              child: Image.asset('assets/images/features/menu-nft-ns.png'),
            ),
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 2,
                  blurRadius: 5,
                  offset: Offset(0, 4), // changes position of shadow
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 10),
            width: sWidth * 1,
            child: GestureDetector(
              onTap: (){
                showDialog(
                  context: context,
                  builder: (BuildContext context) => UnderConstruction(),
                );
              },
              child: Image.asset('assets/images/features/menu-dating-ns.png'),
            ),
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 2,
                  blurRadius: 5,
                  offset: Offset(0, 4), // changes position of shadow
                ),
              ],
            ),
          ),
          Container(
            child: Row(
              children: [
                GestureDetector(
                  onTap: (){
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => ChatScreen())
                    );
                  },
                  child: Container(
                    width: sWidth * 0.8,
                    height: sWidth * 0.2,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: Image.asset('assets/images/features/menu-chat.png').image,
                            fit: BoxFit.cover
                        )
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: (){
                    showDialog(
                      context: context,
                      builder: (BuildContext context) => UnderConstruction(),
                    );
                  },
                  child: Container(
                    width: sWidth * 0.2,
                    height: sWidth * 0.2,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: Image.asset('assets/images/features/games.png').image,
                            fit: BoxFit.cover
                        )
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: GestureDetector(
              onTap: (){
                final loginUser = SukaiApi().currentUser;
                final username = loginUser!=null?loginUser.username:'';
                Share.share('https://play.google.com/store/apps/details?id=io.sukai&referrer=utm_source%3D$username');
              },
              child: Image.asset('assets/images/features/menu-sharelink.png'),
            ),
          ),

          // Container(
          //   child: Row(
          //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          //     children: [
          //       GestureDetector(
          //         onTap: (){
          //           showDialog(
          //             context: context,
          //             builder: (BuildContext context) => UnderConstruction(),
          //           );
          //         },
          //         child: Container(
          //           width: sWidth * 0.2,
          //           height: sWidth * 0.2,
          //           decoration: BoxDecoration(
          //             image: DecorationImage(
          //               image: Image.asset('assets/images/features/nft.png').image,
          //               fit: BoxFit.cover
          //             )
          //           ),
          //         ),
          //       ),
          //       GestureDetector(
          //         onTap: (){
          //           showDialog(
          //             context: context,
          //             builder: (BuildContext context) => UnderConstruction(),
          //           );
          //         },
          //         child: Container(
          //           width: sWidth * 0.2,
          //           height: sWidth * 0.2,
          //           decoration: BoxDecoration(
          //               image: DecorationImage(
          //                   image: Image.asset('assets/images/features/marketplace.png').image,
          //                   fit: BoxFit.cover
          //               )
          //           ),
          //         ),
          //       ),
          //       GestureDetector(
          //         onTap: (){
          //           showDialog(
          //             context: context,
          //             builder: (BuildContext context) => UnderConstruction(),
          //           );
          //         },
          //         child: Container(
          //           width: sWidth * 0.2,
          //           height: sWidth * 0.2,
          //           decoration: BoxDecoration(
          //               image: DecorationImage(
          //                   image: Image.asset('assets/images/features/official-store.png').image,
          //                   fit: BoxFit.cover
          //               )
          //           ),
          //         ),
          //       ),
          //       GestureDetector(
          //         onTap: (){
          //           showDialog(
          //             context: context,
          //             builder: (BuildContext context) => UnderConstruction(),
          //           );
          //         },
          //         child: Container(
          //           width: sWidth * 0.2,
          //           height: sWidth * 0.2,
          //           decoration: BoxDecoration(
          //               image: DecorationImage(
          //                   image: Image.asset('assets/images/features/competition.png').image,
          //                   fit: BoxFit.cover
          //               )
          //           ),
          //         ),
          //       ),
          //       GestureDetector(
          //         onTap: (){
          //           showDialog(
          //             context: context,
          //             builder: (BuildContext context) => UnderConstruction(),
          //           );
          //         },
          //         child: Container(
          //           width: sWidth * 0.2,
          //           height: sWidth * 0.2,
          //           decoration: BoxDecoration(
          //               image: DecorationImage(
          //                   image: Image.asset('assets/images/features/games.png').image,
          //                   fit: BoxFit.cover
          //               )
          //           ),
          //         ),
          //       ),
          //     ],
          //   ),
          // ),
          // Container(
          //   child: Row(
          //     mainAxisAlignment: MainAxisAlignment.center,
          //     children: [
          //       SizedBox(
          //         height: 45,
          //         width: 120,
          //         child: IconButton(
          //             padding: EdgeInsets.zero,
          //             onPressed: (){
          //               showDialog(
          //                 context: context,
          //                 builder: (BuildContext context) => UnderConstruction(),
          //               );
          //             }, icon: Image.asset('assets/images/features/chatting-button.png')),
          //       ),
          //       SizedBox(
          //         height: 45,
          //         width: 120,
          //         child: IconButton(
          //             padding: EdgeInsets.zero,
          //             onPressed: (){
          //               final loginUser = SukaiApi().currentUser;
          //               final username = loginUser!=null?loginUser.username:'';
          //               Share.share('https://play.google.com/store/apps/details?id=io.sukai&referrer=utm_source%3D$username');
          //             }, icon: Image.asset('assets/images/features/share-link-button.png')),
          //       )
          //     ],
          //   )
          // )
        ],
      ),
    );
  }
}

class UnderConstruction extends StatelessWidget {
  const UnderConstruction({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Under construction'),
      content: Text('Feature still under development'),
      actions: [
        TextButton(
            onPressed: () => Navigator.pop(context, 'OK'),
            child: Text('OK'))
      ],
    );
  }
}
