import 'package:flutter/material.dart';
import 'package:sukai/views/home/components/features.dart';
import 'package:sukai/views/home/components/banner.dart';
import 'package:sukai/views/home/components/smart_advert.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: [
          SlidingBanner(),
          Features()
        ],
      ),
    );
  }
}
