import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

final List<String> imgList = [
'assets/images/dummies/home-main-banner.png'
];

class SlidingBanner extends StatefulWidget {
  const SlidingBanner({Key? key}) : super(key: key);

  @override
  _SlidingBannerState createState() => _SlidingBannerState();
}

class _SlidingBannerState extends State<SlidingBanner> {
  // int _current = 0;
  final CarouselController _controller = CarouselController();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      child: Column(
        children: [
          CarouselSlider(
            items: imgList.map((item) => Container(
              child: GestureDetector(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (_) {
                    return DetailScreen(item);
                  }));
                },
                child: Container(
                  width: size.width,
                  margin: EdgeInsets.all(0),
                  child: Image.asset(item, fit: BoxFit.cover),
                ),
              ),
            )).toList(),
            carouselController: _controller,
            options: CarouselOptions(
              autoPlay: false,
              viewportFraction: 1.0,
              enlargeCenterPage: false,
              autoPlayInterval: Duration(seconds: 5),
              onPageChanged: (index, reason) {
                // setState(() {
                //   _current = index;
                // });
              }
            ),
          ),
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.center,
          //   children: imgList.asMap().entries.map((entry) {
          //     return GestureDetector(
          //       onTap: () => _controller.animateToPage(entry.key),
          //       child: Container(
          //         width: 8.0,
          //         height: 8.0,
          //         margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
          //         decoration: BoxDecoration(
          //             shape: BoxShape.circle,
          //             color: (Theme.of(context).brightness == Brightness.dark
          //                 ? Colors.white
          //                 : Colors.black)
          //                 .withOpacity(_current == entry.key ? 0.9 : 0.4)),
          //       ),
          //     );
          //   }).toList(),
          // )
        ],
      ),
    );
  }
}

class DetailScreen extends StatelessWidget {
  final String _imageSrc;

  DetailScreen(this._imageSrc);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          Navigator.pop(context);
        },
        child: Center(
          child: Image.asset(_imageSrc),
        ),
      ),
    );
  }
}
