import 'package:flutter/material.dart';
import 'package:sukai/models/smart_ads.dart';
import 'package:sukai/views/smartads/comments_screen.dart';
import 'package:sukai/views/smartads/create_screen.dart';
import 'package:sukai/views/smartads/orders_screen.dart';
import 'package:sukai/views/smartads/reseller_screen.dart';
import '../../../constants.dart';
import 'ads_card.dart';

class ClientBody extends StatelessWidget {
  const ClientBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          ClientAdsHeader(),
          Expanded(
            child: ListView(
              children: [
                ClientAdsCard(ads: SmartAds('RUNNING SHOES', AdsStatus.active, 'assets/images/04_running_shoes.jpg'), onTap:(ads){_adsModalBottomSheet(context, ads);},),
                ClientAdsCard(ads: SmartAds('LUXURY CAR', AdsStatus.draft, 'assets/images/03_luxury_car.jpg'), onTap:(ads){_adsModalBottomSheet(context, ads);},),
                ClientAdsCard(ads: SmartAds('SUPER KIDS VITAMIN', AdsStatus.active, 'assets/images/02_vitamin_c.jpg'), onTap:(ads){_adsModalBottomSheet(context, ads);},),
                ClientAdsCard(ads: SmartAds('LIP GLOSS',AdsStatus.finished, 'assets/images/01_lip_gloss.jpg'), onTap:(ads){_adsModalBottomSheet(context, ads);},),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _adsModalBottomSheet(context, SmartAds ads) {
    final TextStyle style = TextStyle(
        color: kTextAccentColor,
        fontSize: 14,
        fontWeight: FontWeight.bold
    );

    String statusString = 'Display Until : AUG 31, 2021';
    switch (ads.status) {
      case AdsStatus.finished :
        statusString = 'FINISHED';
        break;
      case AdsStatus.draft :
        statusString = 'DRAFT';
        break;
      case AdsStatus.active :
        break;
    }

    showModalBottomSheet(
      context: context,
      builder: (BuildContext builderContext) {
        return Column(
          children: [
            Container(
                width: double.infinity,
                height: 150,
                child: Image.asset(ads.thumbnail, fit: BoxFit.cover,)
            ),
            Expanded(
              child: Container(
                color: kPrimaryLightColor,
                padding: EdgeInsets.all(5),
                child: Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.fromLTRB(5, 5, 10, 5),
                            padding: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(5)
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(ads.title, style: style, overflow: TextOverflow.ellipsis,),
                                SizedBox(height: 20,),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('Views'),
                                    Text('10000'),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('Female'),
                                    Text('7000'),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('Male'),
                                    Text('3000'),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('Clicks'),
                                    Text('10000 of 15000'),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('Like'),
                                    Text('500'),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('Comments'),
                                    Text('500'),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('Blocked User'),
                                    Text('3'),
                                  ],
                                ),
                                SizedBox(height: 9,),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          width: 150,
                          padding: EdgeInsets.only(right: 5),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              ElevatedButton(
                                onPressed: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(builder: (context) => CreateAdsScreen())
                                  );
                                },
                                child: Text('DETAIL ADS'),
                                style: ElevatedButton.styleFrom(
                                    minimumSize: Size(double.infinity, 36),
                                    padding: EdgeInsets.all(0),
                                ),
                              ),
                              ElevatedButton(
                                onPressed: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(builder: (context) => ResellerScreen())
                                  );
                                },
                                child: Text('RESELLER'),
                                style: ElevatedButton.styleFrom(
                                    minimumSize: Size(double.infinity, 36),
                                    padding: EdgeInsets.all(0),
                                ),
                              ),
                              ElevatedButton(
                                onPressed: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(builder: (context) => CommentsScreen())
                                  );
                                },
                                child: Text('VIEW COMMENTS'),
                                style: ElevatedButton.styleFrom(
                                    minimumSize: Size(double.infinity, 36),
                                    padding: EdgeInsets.all(0),
                                ),
                              ),
                              ElevatedButton(
                                onPressed: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(builder: (context) => OrdersScreen())
                                  );
                                },
                                child: Text('ORDERS'),
                                style: ElevatedButton.styleFrom(
                                    minimumSize: Size(double.infinity, 36),
                                    padding: EdgeInsets.all(0),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ]
                    ),
                    Spacer(),
                    Text(statusString, style: style,),
                  ],
                )
              ),
            ),
          ],
        );
      },
    );
  }
}
