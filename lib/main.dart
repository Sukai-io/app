import 'package:connectycube_sdk/connectycube_core.dart';
import 'package:flutter/material.dart';
import 'package:sukai/constants.dart';
import 'package:sukai/models/sukai_api.dart';
import 'package:sukai/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:sukai/views/home/client_screen.dart';
import 'package:sukai/views/home/home_screen.dart';
import 'package:sukai/views/login/login_screen.dart';
import 'package:sukai/views/welcome/onboarding.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Sukai',
      theme: ThemeData(
        fontFamily: 'PTSans',
        primaryColor: kPrimaryColor,
        scaffoldBackgroundColor: Colors.white,
        primarySwatch: createMaterialColor(kPrimaryColor),
      ),
      home: LandingScreen(),
    );
  }
}

class LandingScreen extends StatefulWidget {
  @override
  _LandingScreenState createState() => _LandingScreenState();
}

class _LandingScreenState extends State<LandingScreen> {

  @override
  void initState() {
    super.initState();
    _loadInitScreen();
  }

  _loadInitScreen() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final seenOnboarding = prefs.getBool('seen_onboarding') ?? false;
    if (seenOnboarding) {
      if (await SukaiApi().isLogin()) {
        // final user = SukaiApi().currentUser;
        // if (user != null && user.package == 'client') {
        //   Navigator.pushAndRemoveUntil(context,
        //     MaterialPageRoute(builder: (context) => ClientScreen()),
        //         (Route<dynamic> route) => false,
        //   );
        // } else {
        //   Navigator.pushAndRemoveUntil(context,
        //     MaterialPageRoute(builder: (context) => HomeScreen()),
        //         (Route<dynamic> route) => false,
        //   );
        // }
        Navigator.pushAndRemoveUntil(context,
          MaterialPageRoute(builder: (context) => HomeScreen()),
              (Route<dynamic> route) => false,
        );
      } else {
        Navigator.pushAndRemoveUntil(context,
          MaterialPageRoute(builder: (context) => LoginScreen()),
              (Route<dynamic> route) => false,
        );
      }
    } else {
      Navigator.pushAndRemoveUntil(context,
        MaterialPageRoute(builder: (context) => OnBoarding()),
            (Route<dynamic> route) => false,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: kPrimaryColor,
        body: Center(
            child:
            CircularProgressIndicator(color: kPrimaryLightColor,)
        )
    );
  }
}

