# SUKAI Terms & Conditions

### Acceptance of Service and Service Description
SUKAI is social media that is directly connected with SkyX Token (“SUKAI” or “us”). SUKAI has the main business of running Social Media which has various features in it such as Advertising, Marketplace, NFT, Classified Ads which acts as an intermediary service with a user generated content system to bring together buyers and sellers online (“Services”) on sukai site: sukai.io and mobile application (the “Site”). Any reference to SUKAI in these General Terms & Conditions, where relevant, includes its affiliates (companies based on joint control and ownership), some or all of its employees, including its directors and commissioners.

This Site and Services are provided to you subject to these General Terms and Conditions. The use of the term "you" or "user" means any person who uses or accesses the Service or the Site either manually or through an automated system, including anyone who browses the Service and the materials therein, makes comments on the materials or responds to advertisements posted.

By using the Service, you agree to comply with these General Terms and Conditions and any other terms and conditions and instructions applicable to the Service, which may be modified or updated by SUKAI from time to time in its sole discretion.

If you do not agree to these General Terms & Conditions, we invite you to discontinue using the Service.

If you are under 18 years of age, you must use the Service with the consent of and accompanied by your parent or guardian.

SUKAI uses English as the main language in its website and mobile application, and will develop it into several foreign languages in the future.

### Limitation of Liability

1. SUKAI is not responsible for the correctness of information, images and descriptions, including but not limited to details regarding the ad title, description, price, address, telephone number provided by the advertiser. You are advised to deal directly with advertisers to ensure the information you are looking for. Any information made by advertisers on the Site about companies, individuals or other entities, and/or about their products or services, does not constitute an endorsement or endorsement, nor implies endorsement or endorsement, of the quality or eligibility of such individuals, companies or entities. , or to its services or products. The responsibility for the content and/or advertising materials posted by advertisers (“Materials”) is the sole responsibility of the advertiser. SUKAI does not own the rights to the advertisements posted by users, nor is it involved in the process of agreement, payment, delivery and after-sales process between the seller and the buyer. The engagement for the delivery of goods or services that occurs through the Service or as a result of the visit and the success of the offer submitted by the user is made freely between the seller and the buyer. Information, images and other information contained or published on this Site may also contain inaccuracies or typographical errors.  Advertisers may make changes or improvements, and/or update the information contained on the Site from time to time.  SUKAI assumes no obligation to update Material that has become outdated or inaccurate.

2. SUKAI is not responsible for all warranties and conditions, including all implications of the guarantee, quality, eligibility for information submitted by advertisers.  Under no circumstances shall SUKAI be responsible for any special, direct, indirect or consequential loss, or any loss or damage as a result of loss of use, data or profits, whether in the act of engagement, negligence or other wrongful act,  arising from or relating to the use or performance of the information and/or images provided by the advertiser.

3. SUKAI is not responsible for the direct or indirect consequences of the decision of the user/potential buyer in submitting an offer or not making an offer to the advertiser, buying and selling or not buying and selling with the advertiser.

4. SUKAI is not responsible or liable for the delivery of goods or services, including the compliance of buyers and advertisers with the statutory provisions in force in each country, both in terms of offering or selling goods and services or other transactions.

5. SUKAI is not responsible or liable for the Service being temporarily unavailable due to technical issues which are beyond our control. Through the Service, you can connect to other websites which are not under our control. The inclusion of a hyperlink does not constitute a recommendation or endorsement of the views expressed in the hyperlink. Every effort is made to keep the Service running smoothly.

6. The services provided by SUKAI are “as is”, “with all faults”, and “as available” without any warranty whatsoever. SUKAI expressly disclaims any warranties regarding the safety, reliability, and performance of the activities performed by users on the Service and any warranties or conditions regarding non-infringement, merchantability and fitness for a particular purpose.  In no event shall SUKAI be liable for any direct, special, indirect, exemplary or punishable damages, whether in an engagement, unlawful act or other legal theory, even though we have been informed of the possibility of such loss.

7. The user understands and agrees that the use and implementation of activities in connection with the Site and Services by the user is at the user's own discretion and risk and that the user himself is fully responsible for the user's material, and/or for any damage/loss of your electronic system and/or for damage/loss of electronic information that may be caused by the implementation of these activities.

8. The User specifically acknowledges that SUKAI will not be responsible for any material or defamation act, infringing action, or any unlawful act of any third party related to SUKAI and this Service.  Any risk of loss or damage from these things is entirely the responsibility of the user.

### User Obligations
Users are required to protect their personal data from damage/loss, including but not limited to efforts to prevent their personal data from being misused by other parties.

The user hereby acknowledges and agrees that SUKAI cannot be held responsible by the user and/or any party for things that occur, including but not limited to:
- Loss of personal data;

- Forgery of user's personal data;

- Loss of income or income;

- Loss of business;

- Loss of profit or an engagement;

- Loss of previously anticipated savings;

- data loss;

- Loss of goodwill;

- wasted management or office time;

- Loss or other damages of any kind, regardless of how they arise and whether caused by negligence, breach of an engagement or otherwise, even if foreseeable;

- Any losses as a result of the use of personal data or user accounts that are indicated to be in violation of the law and/or misuse by unauthorized parties;  and

- Any loss and or damage due to non-operation and/or disruption to SUKAI services due to force majeure, which includes but is not limited to:

- Natural disasters;

- fire incident;

- Strike;

- War;

- Riot;

- Rebellion or other military action;

- The actions of the authorities that affect the continuity of the service;

- Actions of third parties that cause SUKAI to be unable to provide the Service; and

- There are decisions or changes in decisions from relevant agencies including the government that have an impact on the implementation of this Service.

Users at the time of placing advertisements must ensure that each advertised product and/or service and advertising material has: (i) obtained the required approvals and/or permits, (ii) the required approvals and/or permits are still valid and (iii)  the advertised products and/or services are not products and/or services that are prohibited to be traded in general based on the provisions of laws and regulations.

By accessing the Service, the user accepts without limitation or qualification, and agrees to use the Site and the Service only for lawful purposes and in accordance with these terms and conditions, and the user agrees not to:

- Use the Service in any way that will disable, overburden, or impair, or interfere with other uses of the Service, including, but not limited to, each user's ability to engage in real-time activities through the Service;

- Use robots, spiders or other automated devices, processes or means to access the Services;  for any purpose, including to monitor or copy any material on the Service;

- Use manual processes to monitor or copy any material on the Service to engage in other unauthorized purposes without our prior written consent;

- Use any device, software or routine that may interfere with the performance of the Service;

- Interfere with, interfere with, hinder, hinder the work of the Service;

- Copy, reproduce, modify, modify, create derivative works, or publicly display any content from the Service or the Service without the written permission of SUKAI;  or reverse engineering or attempting to find source code relating to the Service or the tools therein, except to the extent such activity is expressly permitted by applicable law notwithstanding any restrictions in this regard;  or

- Attempt to access any area of the Service or the Site over which you have no authority to access;

- Make any effort that if it can cause material or immaterial losses for us or other users of SUKAI;

- Create new accounts and/or buy and/or sell accounts to circumvent any restrictions we may impose on our users;

- Doing false reporting against other users.  Because every report of violations of our service policies must be true and submitted in good faith and with the intention of addressing violations that are known or suspected to violate our policies;

- Violate or circumvent the law, the rights of third parties or our systems, policies, or the determination of your account status;

- Manipulating the price of goods, including but not limited to price concealment.

- Placing false, inaccurate, misleading, defamatory, or libelous advertisements;

- Installing duplicate/Double post ads

- Transferring your ID including but not limited to your username and password to another party without our consent;

- Distribute or send spam, unsolicited or bulk electronic communications, chain letters, or pyramid schemes;

- Distribute viruses or other technologies that may harm us, or the interests or property of our users;

- Commercialize our applications or the information or software associated with those applications;

- Collect information about our users without consent;

- Avoid the technical measures we use to provide the Service.

- Placing advertisements using hyperlinks of any kind.

- Any attempt to manipulate our services or our policies may result in action against your account.  In addition to deletion, you may be subject to various other actions, including the right to advertise or the suspension of your account, including but not limited to legal action.

Since use of the Service requires an Internet connection, you may have to pay fees associated with Internet access when using the Service or the Site.  Users are responsible for carrying out all necessary arrangements to access the Service or the Site.

Users are responsible for ensuring that everyone who accesses the Service or the Site through the user's internet connection, is aware of these General Terms & Conditions and complies with them.


### Compensation
User agrees to release, defend and hold harmless SUKAI, its affiliated companies and their directors, commissioners, officers, employees and agents from and against any and all claims, liabilities, losses and costs (including legal fees, damages compensation and reasonable settlement amount) incurred  arising out of or relating to or which may arise from the use and/or access of the Service;  violation of a term in these General Terms & Conditions, Privacy Policy and No Advertising by users;  violation of any third party rights, including but not limited to intellectual property rights, assets in the form of movable or immovable property, privacy rights;  and/or any claims in connection with user Materials that cause harm to third parties.  In this case SUKAI can also participate in a joint defense with a legal advisor of his choice.


### Description of Service Contents
The material displayed on the Service is provided without any conditions or warranties in terms of content, material and accuracy. To the extent permitted by applicable international law, SUKAI hereby expressly excludes:

- All provisions, warranties and other conditions which may be stated directly or indirectly by law as to their content, material and accuracy;  and

- Liability for direct, indirect or consequential loss or damage incurred by the user in connection with the Service or in connection with the use, inability to use, or as a result of the use of the Service, any websites linked to it and any materials posted on it.

- The material displayed on the Service is solely for information to build relationships between advertisers and other Service users and to help Service users decide to bid or not to bid for goods or services offered by Advertisers on the Service.

- If as a user you use the information contained on the Service for purposes other than those stated in the paragraph above, then we may limit your access to the Service and take necessary legal steps for your actions.


### Activity Implementation
The implementation of access to the Service is available to the public, however to place an advertisement you are required to become a registered user, for each registered user you can only register 1 (one) phone number and the phone number must be the same as the phone number on your profile.

SUKAI has the right to withdraw and/or change the Services provided without prior notice. From time to time, SUKAI may restrict access to certain parts of the site and/or the entirety of the Service to users.

If the user chooses or is given a password (user identification code) or other piece of information as part of the Service's security procedures, the user is obliged to maintain the confidentiality of that information and is not allowed to disclose it to third parties.

SUKAI has the right to turn off the password function (user identification code), regardless of whether chosen by the user or determined by SUKAI under any circumstances and at any time.

As a registered user, we may give you a special mark including but not limited to the purpose of validating the correctness of the information you provide to us. This validation is free and does not cost you anything but you are expected to participate in this activity.  After we validate the correctness of the information you provide, we will put a special mark on your account that is valid for as long as we deem appropriate, therefore we hope that you can periodically re-validate when our system requests it.  With regard to these special marks, we can also change, delete, or add these special marks.  Of course, if you violate some or all of the provisions in LIKE, we can remove the sign from your account.

Giving special marks to users and advertisements posted by users are not a form of support, recommendation or opinion from SUKAI to users, the goods/services offered, and how users express the goods/services they offer.


### Withdraw
- Only verified user that already completed the profile with the real information and approved by Sukai Admin can do a Withdraw.
- Every rewards will be verified. Rewards that comes from fake users (not verified users) can not be withdraw.


### Service Changes
SUKAI has the right to update and/or change the appearance and/or Service and/or content of the Service regularly and at any time, in order to provide better service to each of its users.
If necessary, SUKAI may suspend access to the Service, or temporarily close it for an indefinite period of time.

With regard to information about users and user visits to the Service, SUKAI has the right to process information about users in accordance with the policies stated in the Privacy Policy.  By using the Service, the user agrees to such processing and guarantees that all data provided is correct.

### Uploading Materials to the Service
You acknowledge and agree that you are responsible for the Content you post, transmit through or link to the Service and any consequences arising from such action.  In particular you are responsible for any Material that you upload such as email or that you make available through the Service or the Site.  With respect to any Content posted, transmitted through or linked to the Service by you, you expressly acknowledge that: (i) you own and will retain the rights to such Material as long as the Material is still available on the Service or the Site;  you have permission, right and approval to use the Material and authorize SUKAI to use the Material in the Service or Site or for the promotion of SUKAI or for other purposes as long as it does not conflict with the Law and these General Terms and Conditions;  and (ii) you have written consent, waiver and/or permission from any and all identifiable individuals and businesses contained in the Materials to use the name or likeness of any and all such individuals and businesses on the Service or the Site in accordance with the Terms.  and these General Terms.  you retain ownership of any Material;  however by inserting Material into the Service, you grant an irrevocable, irrevocable, perpetual, worldwide, non-exclusive, royalty-free and sublicensable, transferable right to use, reproduce, distribute, create work  derivatives, posting and displaying Material in connection with the Service and SUKAI's business, including but not limited to the purpose of promotion and redistribution of part or all of the Service and the Material contained therein.  These rights are required by SUKAI to be able to provide the Services and post your Materials.  Further, by posting the Materials in a public area within the Service, you agree to and hereby grant to SUKAI all necessary rights to prohibit or permit the collection, posting, copying, duplication, reproduction or exploitation of the Materials on the Service by any party for the purposes of  anything.  You also hereby grant each user of the Service and the Site a non-exclusive right to access your Materials through the Service or the Site.  This right will survive and terminate immediately when you or SUKAI remove or withdraw Material from the Site.

SUKAI may impose restrictions on some or all of your activities or charge fees for advertisements placed by users in certain parts of the Service including but not limited to the use of Advertising Promotion Features which must be carried out in accordance with these General Terms and Conditions, and  Advertising Promotion Feature Terms and Conditions.

If the user places an ad in the paid ad category and uses the ad promotion feature, the user grants SUKAI to the right to charge for the activity.

SUKAI has the right to remove any content, material or other parts of advertisements, either in part or in whole, that users post on the Service or the Site without prior notice to users. This includes but is not limited to if the advertisement contains content, material or parts that are sensitive, offensive, can trigger hatred, defame, contain material that triggers ethnic, religious, racial, inter-group divisions, pornography, gambling, or contrary to  ethical norms, decency and laws that apply in the International and not in accordance with the rules that apply in the Service.


### Ad Duration
The active period of the ad is as stated on the advertising page. After the active period of the ad expires, the ad is automatically and permanently deleted.


### Link (hyperlink) of the Service
The Service may contain hyperlinks to sites and/or other resources provided by third parties, where SUKAI confirms that the hyperlinks are provided for informational purposes only.  SUKAI releases itself from the obligation to control the content of sites and/or resources that occur due to hyperlinks on the Service.

SUKAI is not responsible for any loss and/or damages that may arise as a result of using the Service.


### Relations with Third Parties
You acknowledge and agree that SUKAI will not be responsible for your interactions with third parties on the Site or through the Service.  This includes, but is not limited to, payment for and delivery of goods and services, and any other terms, conditions, warranties or representations relating to interactions between you and third parties.  This relationship is between you and the third party.  You acknowledge and agree that SUKAI will not be liable for any loss or damage arising out of such relationship or interaction.  In the event of a dispute between a user and a third party, you understand and agree that SUKAI is not responsible for involving itself in the dispute and you hereby indemnify SUKAI, its employees, officers and agents from any claims and losses arising in any form in connection with the Service.

 
### Jurisdiction and Applicable Law
These General Terms & Conditions, Privacy Policy and No Advertising and the user's relationship with International are subject to, governed and complied with under International law, and the user agrees to be bound by the jurisdiction of International law.

In the event of a dispute in the interpretation and implementation of these General Terms & Conditions, the Privacy Policy and the Prohibition of Advertising, the parties agree to resolve it by deliberation to reach a consensus.


### Changes
Users are expected to check this page from time to time for any changes, as they are binding on the user.

Some of the provisions contained in these General Terms & Conditions may be superseded by other terms or notices published elsewhere in the Service.

 
### Etc
By becoming a user of the Service, you can choose whether you wish to receive information related to the Service, its use, and other matters relating to the use of your account, while we will send such information in good faith via notification in the form of email or email.  application notifications or other forms.  If you choose not to receive information, you can notify us by contacting our customer service.

If users have other things to say or comments or concerns about the material that appears on the Site and Services, please contact our customer service or email at: tech@sukai.io

Thank you for visiting our Site and Services.

Latest Updates on July 01, 2021.
