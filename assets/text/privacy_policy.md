
# SUKAI Privacy & Policy

We care about your privacy and are committed to protecting your personal data.  This privacy statement will tell you how we handle your personal data, your privacy rights and how the law protects you. Please read this privacy statement carefully before using our Services.

In this privacy statement:
- Service means any product, service, content, feature, technology or function, and all related websites, applications and services offered to you by us.
- Platform means our website, mobile application, mobile site or other online thing through which we offer our Services.

### Who are we?

The primary controller of your data for the purposes of our international services is SUKAI a company located at 4494 McKinley Avenue, Colorado, 80110 , United States of America. SUKAI Global is the parent company of SUKAI group entities that provide services to you in accordance with our Terms & Conditions, (hereinafter collectively referred to as “SUKAI”, “we”, “us” or “our” in this privacy statement). These SUKAI group entities are also considered data controllers for the local services they offer.

### What data do we collect about you?

1.1 Data is provided through direct interaction
Registration and other account information

When you register to use our Services, we may collect the following information about you:
- if you register using your Google account: first name, last name and email address;
- if you register using your Facebook account: we collect your first name and last name as they appear on your Facebook account and Facebook ID.  If you have given permission to Facebook via their in-app privacy option (which appears just before you register on our Platform), we may collect your gender, age or email depending on the permissions granted by you;  and
- if you register using your mobile number: mobile number.

Depending on the choices you make during the process of logging into our Services or during the process of engaging our Services, you may choose to provide the following additional personal data:
- Your name;
- Email address;
- Phone number;
- Your credit card details if you wish to purchase our paid services as defined in our Terms & Conditions.
- Communication via the chat feature on our Platform

When you use our chat feature to communicate with other users, we collect information that you choose to provide to other users through this feature.

1.2 Data we collect automatically when you use our Services
When you interact with our Platform or use our Services, we automatically collect the following information about you:

### Device Information

We collect device-specific information such as operating system version, unique identifiers.  For example, the name of the mobile network you are using.  We associate a device identifier with your LIKE account.

### Location information

Depending on your device permissions, if you post something on our Platform, we automatically collect and process information about your actual location.  We use a variety of technologies to determine location, including IP addresses, GPS, Wi-Fi access points, and cell towers.  Your location data allows you to view items of users near you and assists you in posting items within your location.  If we need your location data, we will first show you a pop-up that will ask you to choose whether or not to allow us to access your location data.  If you do not allow us to have access to your location data, you can still use our Services but with limited functionality. If you allow us to access your location data, you can always change it afterwards by going to the settings on our website or our platform app and disabling the permissions related to location sharing.

### Client and Log data

Technical details, including your device's Internet Protocol (IP) address, time zone and operating system.  We will also store your login information (date of registration, date of last password change, date of last successful login), your browser type and version.

### Clickstream data

We collect information about your activity on our Platform which includes the site from which you accessed our Platform, the date and time stamp of each visit, searches you have performed, listings or advertisements you clicked on, your interactions with such advertisements or listings, duration of visits.  you and the order in which you visit the content on our Platform.

### Cookies and Similar Technologies

We use cookies to manage our user sessions, to save your preferred language choice and to send you relevant advertisements.  "Cookies" are small text files that are transferred by a web server to your device's hard drive.  Cookies may be used to collect the date and time of your visit, your browsing history, your preferences, and your username.  You can set your browser to refuse all or some cookies, or to alert you when a website sets or accesses cookies. If you disable or refuse cookies, please note that some parts of our Services/Platform may become inaccessible or not function properly.

1.3 Data from third parties or data available from public sources.
We receive personal data about you from various third parties and public sources as mentioned below:

Certain technical and usage information from analytics providers such as Google, Facebook, and Optimizely; Cooks information from advertising networks such as Criteo and Props.

1.4 Do we collect data from children?
Our services are not directed at children under 18 and we do not knowingly collect data from anyone under the age of 18.  If we become aware that a person under the age of 18 has provided us with their personal data, we will delete it immediately.

Why do we process your personal information?
We will only use your personal data when the law allows us.  Generally, we will use your personal data in the following situations:

- Where we need to enter into a contract that we will enter into or have entered into with you.
- Where necessary for our legitimate interests to improve our Services and to provide you with a safe and secure Platform.
- Where we must comply with legal or regulatory obligations.

In certain circumstances, we may also process your personal data based on your consent.  If we do this, we will tell you which purposes and categories of personal data will be processed when we ask for your consent.

We have described below a description of how we use your personal data, [and from which legal basis we use to do so.  We have also identified what our legitimate interests are in the right places.

2.1 To provide access and provide Services through our Platform
- If you log in using your mobile number or email ID, we use your first and last name, mobile number and/or e-mail address to identify you as a user and provide access to our Platform.

- If you log in using your Facebook account, we use your first and last name from your Facebook profile and Facebook email address to identify you as a user on our Platform and provide you with access to our Platform.

The above log-information data is also used by us to deliver our Services to you in accordance with our Terms & Conditions.
We use your e-mail address and mobile number (via SMS) to make suggestions and recommendations to you about our Services that may be of interest to you.

We process the above information for good performance of our agreement with you and on the basis of our legitimate interest in conducting marketing activities to offer Services that may be of interest to you.

2.2 To improve your experience on the Platform
i.  We use clickstream data to:
- offer customized content, such as providing you with more relevant search results when using our Services.
- to determine how much time you spend on our Platform and in what ways you navigate through our Platform to understand your interests and to improve our Services based on this data.  For example, we may give you suggestions about the content you can visit based on the content you click on.
- to monitor and report on the effectiveness of campaign delivery to our business partners and for internal business analysis.

ii. We use your location data for the following purposes:
- to collect anonymous and aggregated information about the characteristics and behavior of SUKAI users, including for the purposes of business analysis, segmentation and anonymous profile development.
- to improve the performance of our Services and to personalize the content we direct to you.  For example - with the help of location data we display a list of ads that are around you to improve your buying experience.  For this purpose, the Google Maps service is integrated into our Platform.  Google Maps is provided by us and Google acts as an independent controller.  This means that Google and we are responsible for processing your location data in the context of Google Maps.  However, we will not process your location data for any other purposes than those described in this Privacy Statement.  However, Google may process such location data for their own purposes as described in the Google Privacy Policy which can be reviewed here.  Use of Google Maps through our Platform is subject to the Google Maps Terms of Service.
- to measure and monitor your interactions with third-party banner ads that we place on our Platform

iii. With the help of your login information which includes your email id and phone number, we map the different devices (such as desktop, mobile, tablet) used by you to access our Platform. This allows us to associate your activity on our Platform across devices and helps us provide a good experience no matter what device you are using.

We process the above information based on our legitimate interests to improve your experience on our Platform and for the adequate performance of our contract with you.

2.3 To provide you with a safe and secure Platform
- We use your mobile number, log data and unique device identifier to manage and protect our Platform (including troubleshooting, data analysis, testing, fraud prevention, system maintenance, support, reporting and data hosting).
- We analyze your communications made through our chat feature for fraud prevention and to improve security by blocking spam or abusive and unkind messages that may have been sent to you by other users.
- We process the above information for adequate performance of our contract with you, to improve our services and in our legitimate interest to prevent fraud.

### How will we notify you of changes to our privacy statement?

We may change this privacy statement from time to time.  We will post the changes on this page and will notify you by e-mail or through our Platform.  If you do not agree with the changes, you can close your account by going to account settings and selecting delete account.

3.1 Your rights
- In certain circumstances, you have rights under data protection laws relating to your personal data.
- If you wish to exercise any of the rights set out below, please go to your account/privacy settings or contact us using the Contact Form/privacy email id: tech@sukai.io
- The right to request access to your personal data (commonly known as a "data subject access request").  This allows you to receive a copy of the personal data we hold about you and to check that we are lawfully processing it.
- The right to request correction of any data we hold about you.  This allows you to have any incomplete or inaccurate data we hold about you correct, although we may need to verify the accuracy of the new data you provide us.
- The right to request restrictions on the processing of your personal data.  This allows you to request that we suspend the processing of your personal data in the following scenarios: (a) if you want us to establish the accuracy of the data;  (b) if the use of our data is unauthorized;  (c) where you need us to store data even if we no longer need it when you need it to establish, enforce or defend legal claims;  or (d) you object to our use of your data, but we need to verify whether we have ruled out a valid reason for using it.
- The right to request the deletion of your personal data.  This allows you to request that we delete or delete personal data for which there is no compelling reason for us to continue processing it.  You also have the right to ask us to delete or transfer your personal data where you have successfully exercised your right to object to processing (see below), where we may have processed your information unlawfully or where we have been asked to delete your personal data.  to comply with local laws.  Please note that for certain purposes we may be legally obligated to retain your data.
- The right to object to processing of your personal data where we rely on a legitimate interest (or that of a third party) and there is something about your particular situation that makes you want to object to processing on this basis because you feel it impacts your fundamental rights and freedoms.  You also have the right to object to where we process your personal data for direct marketing purposes.  In some cases, we may demonstrate that we have imposed a valid reason to process your information that overrides your rights and freedoms.
- The right to request the transfer of your personal data to you or to a third party.  We will provide you, or a third party you choose, your personal data in a structured, commonly used, machine-readable format.  Note that this right only applies to automated information that you initially consent to us using or where we use that information to perform a contract with you.
- The right to withdraw your consent to the processing of your personal data at any time.  This does not affect the legality of any processing we have carried out based on the prior consent.
- No fees are usually required: you don't have to pay a fee to access your personal data (or exercise any other rights).  However, we may charge a reasonable fee if your request is clearly unfounded, repetitive or excessive.  Alternatively, we may refuse to comply with your request in these circumstances.
- Deadline for responding: We try to respond to all valid requests within one month.  Sometimes it may take us more than a month if your request is very complex or you have made several requests.  In this case, we will notify you and inform you of the progress of this matter.
- In addition, you have the right to lodge a complaint at any time with the data protection authority responsible for you.
- However, before you file a complaint with the data protection authority, we would appreciate the opportunity to address your concern in the first instance, please contact our email at: tech@sukai.io and the Telegram chat service SUKAI at https://t.me/SkyX_Sukai


4.1 Communication and marketing
We will communicate with you via email, SMS or application notifications in connection with our Services/Platform to confirm your registration, to notify you if your listing has become live/expired and for other transactional messages in connection with our Services.  Because it is very important for us to provide you with such transactional messages, you may not be able to opt out of these messages.

However, you can ask us to stop sending you marketing communications at any time by clicking on the opt-out link in the email or SMS sent to you or by changing the communication settings in your account.  If there is a problem changing these settings, please contact us via the Connection Form.

You may receive marketing communications from us if you:
- has requested such information from us;
- use our Platform or Services;
- provide us with your details when you enter a competition; or
- registered for promotion.

Who do we share your data with?
We may have to share your personal data with the parties specified below for the purposes set out in above section.

Corporate affiliates - we may share your data with other SUKAI group companies located within and outside the EEA and assist us in providing business operations services such as product improvement, customer support and fraud detection mechanisms.  Any sharing of personal data within the SUKAI group of companies located outside the European Economic Area (“EEA”) will always be subject to protection and data transfer agreement which clearly defines the obligations of the parties and ensures appropriate technical and measures  organizational steps to protect your data.

Third Party Service Providers: We use third party service providers to help us deliver certain aspects of our Services, for example, cloud storage facilities such as Amazon Web Services, Google Cloud Platform and Microsoft Azure.  Service providers can be located within or outside the “EEA”.

We conduct checks on our third party service providers and require them to respect the security of your personal data and treat it in accordance with the law.  We do not allow them to use your personal data for their own purposes and only allow them to process your personal data for certain purposes and in accordance with our instructions.

Advertising and analytics providers: To improve our Services, we will sometimes share your information with analytics providers who can help us analyze how people use our Platforms/Services.  For the most part, we provide them with your information in a non-identifiable form to monitor and report on the effectiveness of campaign delivery to our business partners and for internal business analysis.  This information will typically include the cookie ID, IP address (short), the referral URL and browser and device information.  We partner with advertising providers regulated under the Interactive Advertising Bureau (IAB) and listed here: https://advertisingconsent.eu/vendor-list/.  To control and manage the settings for ads, you can access the settings via the link in the footer of our website. For more information about our advertisers and analytics providers.

Law enforcement officials, regulators and others: We may disclose your personal data to law enforcement, regulators, government or public bodies and other relevant third parties to comply with any legal or regulatory requirements.

We may choose to sell, transfer, or combine part of our business or our assets. Alternatively, we may seek to acquire other businesses or join them. If changes occur to our business, the new owner may use your personal data in the same way as set out in this privacy statement.

Publicly available information: When you post items for sale using our Services, you can choose to make certain personal information visible to other SUKAI  users. This may include your first name, last name, your email address, your location and your contact number. Please note, any information you provide to other users may always be shared with them with others, so please be tactful in this regard.

### International transfer

Whenever we transfer your personal data from the EEA, we ensure the same level of protection is provided to it by ensuring at least one of the following safeguards is in place:

- We will only transfer your personal data to countries that have been deemed to provide an adequate level of protection for personal data by the European Commission.  For more information, see European Commission: Adequacy of the protection of personal data in non-EU countries.
- Where we use certain service providers, we may use certain contracts approved by the European Commission that provide personal data with the same protections as those in Europe. For more information, see European Commission: Adequacy of the protection of personal data in non-EU countries.
- Where we use service providers based in the United States, we may transfer data to them if they are part of a Privacy Shield which requires them to provide the same protection for personal data shared between Europe and the United States.  For more information, see European Commission: Adequacy of the protection of personal data in non-EU countries.

You can receive a copy of the relevant and applied safeguards by contacting us via the Connection Form.

5.1 Where do we store your data and for how long?
The data we collect about you will be stored and processed within and outside the EEA on secure servers to provide the best possible user experience.  For example - to build a website or a fast mobile app.
We will only retain your personal data for as long as necessary to fulfill the purposes for which we collected it, including for the purpose of meeting any legal, accounting or reporting requirements.

To determine an appropriate retention period for personal data, we consider the amount, nature and sensitivity of the personal data, the potential risk of harm from unauthorized use or disclosure of your personal data, the purposes for which we process your personal data and whether we can achieve those goals.  through other means, and applicable legal requirements.

If there is no activity on your account for a period longer than 24 months, we will delete your account including all personal data stored in your account which means that you will no longer be able to access and use it.

If you have any questions regarding your data retention period, please contact us at tech@sukai.io


6.1 Technical and organizational actions & security processing
All information we receive about you is stored on secure servers and we have implemented appropriate and necessary technical and organizational measures to protect your personal data.  SUKAI continuously evaluates the security of its network and the adequacy of its internal information security program designed to (a) help secure your data from accidental or unlawful loss, access or disclosure, (b) identify reasonably foreseeable risks to the security of SUKAI's network, and  (c) minimize security risks, including through risk assessment and routine testing.  In addition, we ensure that all payment data is encrypted using SSL technology.

Please note, although we have taken the steps we put in place to protect your data, data transfer over the Internet or other open networks is never completely secure and there is a risk that your personal data may be accessed by unauthorized third parties.

7.1 Links to third party websites
Our platform may contain links to third party websites or applications. If you click on any of these links, please note that each will have its own privacy policy.  We do not control these websites/apps and are not responsible for those policies. When you leave our Platform, we encourage you to read the privacy notices of each website you visit.

Latest Updates on July 01, 2021.
